package com.example.ololoproxdd.sqlliteforest.XmlModels.Mds;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Доп_макет23")
public class Maket23 {
    public void setDefault() {
        osobennost1 = "1";
        osobennost2 = "1";
        osobennost3 = "1";
        osobennost4 = "1";
        osobennost5 = "1";
        osobennost6 = "1";
        osobennost7 = "1";
        osobennost8 = "1";
    }

    public Maket23(){
        setDefault();
    }

    //region поля
    @Element(required = false, name = "Особенность_1_код")
    public String osobennost1;

    @Element(required = false, name = "Особенность_2_код")
    public String osobennost2;

    @Element(required = false, name = "Особенность_3_код")
    public String osobennost3;

    @Element(required = false, name = "Особенность_4_код")
    public String osobennost4;

    @Element(required = false, name = "Особенность_5_код")
    public String osobennost5;

    @Element(required = false, name = "Особенность_6_код")
    public String osobennost6;

    @Element(required = false, name = "Особенность_7_код")
    public String osobennost7;

    @Element(required = false, name = "Особенность_8_код")
    public String osobennost8;
    //endregion

}
