package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket21;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;

public class RekreacHarakterActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_tip_landshaft = null;
    Spinner spinner_estet_ocenka = null;
    Spinner spinner_san_ocenka = null;
    Spinner spinner_ustoichivost = null;
    Spinner spinner_prohodimost = null;
    Spinner spinner_prosmatr = null;
    Spinner spinner_stadia_degresii = null;
    Spinner spinner_maf = null;

    ArrayList<String> tip_landshafta = null;
    ArrayList<String> estet_ocenka = null;
    ArrayList<String> san_ocenka = null;
    ArrayList<String> ustoichivost = null;
    ArrayList<String> prohodimost = null;
    ArrayList<String> prosmatr = null;
    ArrayList<String> stadia_degressii = null;
    ArrayList<String> maf = null;

    Videl selectedVidel = State.selectedVidel;
    Maket21 maket21 = selectedVidel.maket21;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekreac_harakter);
        getSupportActionBar().setTitle("Рекреационная характеристика");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        if (validation){
            String errorMessage = "";
            //обязатаельные поля для ввода
            if (spinner_tip_landshaft.getSelectedItem().toString().equals("0") ||
                    spinner_estet_ocenka.getSelectedItem().toString().equals("0") ||
                    spinner_san_ocenka.getSelectedItem().toString().equals("0") ||
                    spinner_ustoichivost.getSelectedItem().toString().equals("0") ||
                    spinner_prohodimost.getSelectedItem().toString().equals("0") ||
                    spinner_prosmatr.getSelectedItem().toString().equals("0") ||
                    spinner_stadia_degresii.getSelectedItem().toString().equals("0"))
                errorMessage += "Заполните обязательное поле ввода!\n";

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Рекреационная характеристика");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save(){


        //guid
        maket21.tipLand  = DatabaseProcedures.GetPKCode("тип_ландшафта", spinner_tip_landshaft.getSelectedItem().toString());
        maket21.estetPoint  = DatabaseProcedures.GetPKCode("эстет_оценка", spinner_estet_ocenka.getSelectedItem().toString());
        maket21.sanitarPoint  = DatabaseProcedures.GetPKCode("сан_оценка", spinner_san_ocenka.getSelectedItem().toString());
        maket21.ustojchuvost  = DatabaseProcedures.GetPKCode("устойчивость", spinner_ustoichivost.getSelectedItem().toString());
        maket21.prohodimost  = DatabaseProcedures.GetPKCode("проходимость", spinner_prohodimost.getSelectedItem().toString());
        maket21.prosmatrivaemost  = DatabaseProcedures.GetPKCode("просматриваемость", spinner_prosmatr.getSelectedItem().toString());
        maket21.stadiaDigresija  = DatabaseProcedures.GetPKCode("стадия_дегрессии", spinner_stadia_degresii.getSelectedItem().toString());
        maket21.MAF  = DatabaseProcedures.GetPKCode("МАФ", spinner_maf.getSelectedItem().toString());

        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    public void init() {
        //region начальная инициализация
        spinner_tip_landshaft = findViewById(R.id.spinner_tip_landshaft);
        ArrayAdapter<String> adapter_spinner_tip_landshaft = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tip_landshafta);
        adapter_spinner_tip_landshaft.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tip_landshaft.setAdapter(adapter_spinner_tip_landshaft);

        spinner_estet_ocenka = findViewById(R.id.spinner_estet_ocenka);
        ArrayAdapter<String> adapter_spinner_estet_ocenka = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, estet_ocenka);
        adapter_spinner_estet_ocenka.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_estet_ocenka.setAdapter(adapter_spinner_estet_ocenka);

        spinner_san_ocenka = findViewById(R.id.spinner_san_ocenka);
        ArrayAdapter<String> adapter_spinner_san_ocenka = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, san_ocenka);
        adapter_spinner_san_ocenka.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_san_ocenka.setAdapter(adapter_spinner_san_ocenka);

        spinner_ustoichivost = findViewById(R.id.spinner_ustoichivost);
        ArrayAdapter<String> adapter_spinner_ustoichivost = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ustoichivost);
        adapter_spinner_ustoichivost.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_ustoichivost.setAdapter(adapter_spinner_ustoichivost);

        spinner_prohodimost = findViewById(R.id.spinner_prohodimost);
        ArrayAdapter<String> adapter_spinner_prohodimost = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, prohodimost);
        adapter_spinner_prohodimost.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_prohodimost.setAdapter(adapter_spinner_prohodimost);

        spinner_prosmatr = findViewById(R.id.spinner_prosmatr);
        ArrayAdapter<String> adapter_spinner_prosmatr = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, prosmatr);
        adapter_spinner_prosmatr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_prosmatr.setAdapter(adapter_spinner_prosmatr);

        spinner_stadia_degresii = findViewById(R.id.spinner_stadia_degresii);
        ArrayAdapter<String> adapter_spinner_stadia_degresii = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stadia_degressii);
        adapter_spinner_stadia_degresii.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_stadia_degresii.setAdapter(adapter_spinner_stadia_degresii);

        spinner_maf = findViewById(R.id.spinner_maf);
        ArrayAdapter<String> adapter_spinner_maf = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, maf);
        adapter_spinner_maf.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_maf.setAdapter(adapter_spinner_maf);
        //endregion

        //region события
        spinner_tip_landshaft.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_estet_ocenka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_san_ocenka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_ustoichivost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_prohodimost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_prosmatr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_stadia_degresii.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_maf.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        //endregion

        //region загружаем информацию из xml
        if (selectedVidel != null && maket21 != null) {
            //тип ландшафта
            if (maket21.tipLand != null) {
                String tip_land = DatabaseProcedures.GetDataFromPK("тип_ландшафта", "code", "name", maket21.tipLand);
                spinner_tip_landshaft.setSelection(adapter_spinner_tip_landshaft.getPosition(tip_land));
            }

            //эстетическая оценка
            if (maket21.estetPoint != null) {
                String estet = DatabaseProcedures.GetDataFromPK("эстет_оценка", "code", "name", maket21.estetPoint);
                spinner_estet_ocenka.setSelection(adapter_spinner_estet_ocenka.getPosition(estet));
            }

            //санитарная оценка
            if (maket21.sanitarPoint != null) {
                String san = DatabaseProcedures.GetDataFromPK("сан_оценка", "code", "name", maket21.sanitarPoint);
                spinner_san_ocenka.setSelection(adapter_spinner_san_ocenka.getPosition(san));
            }

            //устойчивость
            if (maket21.ustojchuvost != null) {
                String ust = DatabaseProcedures.GetDataFromPK("устойчивость", "code", "name", maket21.ustojchuvost);
                spinner_ustoichivost.setSelection(adapter_spinner_ustoichivost.getPosition(ust));
            }

            //проходиомсть
            if (maket21.prohodimost != null) {
                String prohod = DatabaseProcedures.GetDataFromPK("проходимость", "code", "name", maket21.prohodimost);
                spinner_prohodimost.setSelection(adapter_spinner_prohodimost.getPosition(prohod));
            }

            //просматриваемость
            if (maket21.prosmatrivaemost != null) {
                String prosmatr = DatabaseProcedures.GetDataFromPK("просматриваемость", "code", "name", maket21.prosmatrivaemost);
                spinner_prosmatr.setSelection(adapter_spinner_prosmatr.getPosition(prosmatr));
            }

            //стадия дегрессии
            if (maket21.stadiaDigresija != null) {
                String digres = DatabaseProcedures.GetDataFromPK("стадия_дегрессии", "code", "name", maket21.stadiaDigresija);
                spinner_stadia_degresii.setSelection(adapter_spinner_stadia_degresii.getPosition(digres));
            }

            //МАФ
            if (maket21.MAF != null) {
                String maf = DatabaseProcedures.GetDataFromPK("МАФ", "code", "name", maket21.MAF);
                spinner_maf.setSelection(adapter_spinner_maf.getPosition(maf));
            }
        }
        //endregion
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        tip_landshafta = DatabaseProcedures.getData("тип_ландшафта", "code", "name");
        estet_ocenka = DatabaseProcedures.getData("эстет_оценка", "code", "name");
        san_ocenka = DatabaseProcedures.getData("сан_оценка", "code", "name");
        ustoichivost = DatabaseProcedures.getData("устойчивость", "code", "name");
        prohodimost = DatabaseProcedures.getData("проходимость", "code", "name");
        prosmatr = DatabaseProcedures.getData("просматриваемость", "code", "name");
        stadia_degressii = DatabaseProcedures.getData("стадия_дегрессии", "code", "name");
        maf = DatabaseProcedures.getData("МАФ", "code", "name");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
