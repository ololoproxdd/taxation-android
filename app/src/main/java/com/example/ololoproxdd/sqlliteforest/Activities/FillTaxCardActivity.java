package com.example.ololoproxdd.sqlliteforest.Activities;

import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.CategoriesActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.EventActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.GpsActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.NoteActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.PodrostActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.VirubkaActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.YarusActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.R;

import java.io.IOException;

public class FillTaxCardActivity extends AppCompatActivity {

    //region UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_tax_card);
        getSupportActionBar().setTitle("Карточка таксации");
    }

    @Override
    protected void onStart(){
        super.onStart();
        getActiveFormsMDS();
        setEnableActiveForms();
    }

    // Категории
    public void categories(View view) {
        Intent intent = new Intent(this, CategoriesActivity.class);
        startActivity(intent);
    }

    // События
    public void events(View view) {
        Intent intent = new Intent(this, EventActivity.class);
        startActivity(intent);
    }

    // Вырубка
    public void felling(View view) {
        Intent intent = new Intent(this, VirubkaActivity.class);
        startActivity(intent);
    }

    // Ярус
    public void tier(View view) {
        Intent intent = new Intent(this, YarusActivity.class);
        startActivity(intent);
    }

    // Подрост / подлесок
    public void undergrowth(View view) {
        Intent intent = new Intent(this, PodrostActivity.class);
        startActivity(intent);
    }

    // МДС
    public void additional_inf(View view) {
        Intent intent = new Intent(this, MdsActivity.class);
        startActivity(intent);
    }

    // GPS
    public void gps(View view) {
        Intent intent = new Intent(this, GpsActivity.class);
        startActivity(intent);
    }

    // Примечания
    public void notes(View view) {
        Intent intent = new Intent(this, NoteActivity.class);
        startActivity(intent);
    }
    //endregion

    //region Активные формы и мдс
    void getActiveFormsMDS(){
        String cat_zemel_pk = State.selectedVidel.katZemel != null ? State.selectedVidel.katZemel : "1";
        String cat_zemel_code = DatabaseProcedures.GetDataFromPK("категория_земель_ТЗ", "code", cat_zemel_pk);
        State.activeForms = DatabaseProcedures.runScript(
                "select форма_ввода_МДС from категория_форма_ввода_мдс where категория_земель_code = " + cat_zemel_code).get(0);

        State.activeRequiredForms = DatabaseProcedures.runScript(
                "select обязат_заполн from категория_форма_ввода_мдс where категория_земель_code = " + cat_zemel_code).get(0);

    }

    void setDisabledForms(){
        int disable = Color.LTGRAY;
        Button cat = findViewById(R.id.btn_categories);
        cat.setEnabled(false);
        cat.setTextColor(disable);

        Button yarus = findViewById(R.id.btn_yarus);
        yarus.setEnabled(false);
        yarus.setTextColor(disable);

        Button virub = findViewById(R.id.btn_virubka);
        virub.setEnabled(false);
        virub.setTextColor(disable);

        Button events = findViewById(R.id.btn_events);
        events.setEnabled(false);
        events.setTextColor(disable);

        Button podrost = findViewById(R.id.btn_podrost);
        podrost.setEnabled(false);
        podrost.setTextColor(disable);

        Button mds = findViewById(R.id.btn_mds);
        mds.setEnabled(false);
        mds.setTextColor(disable);

        Button gps = findViewById(R.id.btn_gps);
        gps.setEnabled(false);
        gps.setTextColor(disable);

        Button notes = findViewById(R.id.btn_notes);
        notes.setEnabled(false);
        notes.setTextColor(disable);
    }

    void setEnableActiveForms(){
        if (State.selectedVidel.selectedMDS == null)
            State.selectedVidel.selectedMDS = "";
        //устанавливаем стандартные значения
        State.setDefaultMdsMap();
        setDisabledForms();
        //устанавливаем значения для данной категории
        for (String form : State.activeForms.split(", ")) {
            int int_form = Integer.parseInt(form.trim());
            Button btn = null;
            switch(int_form){
                case 1 :
                    btn = findViewById(R.id.btn_categories);
                    //btn.setBackgroundResource(R.drawable.draw_category);
                    break;
                case 2 :
                    btn = findViewById(R.id.btn_yarus);
                    //btn.setBackgroundResource(R.drawable.draw_yarus);
                    break;
                case 3 :
                    btn = findViewById(R.id.btn_virubka);
                    //btn.setBackgroundResource(R.drawable.draw_virubka);
                    break;
                case 4 :
                    btn = findViewById(R.id.btn_events);
                    //btn.setBackgroundResource(R.drawable.draw_event);
                    break;
                case 5 :
                    btn = findViewById(R.id.btn_podrost);
                    //btn.setBackgroundResource(R.drawable.draw_podrost);
                    break;
                case 6 :
                    btn = findViewById(R.id.btn_mds);
                    //btn.setBackgroundResource(R.drawable.draw_mds);
                    break;
                case 7 :
                    btn = findViewById(R.id.btn_gps);
                    //btn.setBackgroundResource(R.drawable.draw_gps);
                    break;
                case 8 :
                    btn = findViewById(R.id.btn_notes);
                    //btn.setBackgroundResource(R.drawable.draw_note);
                    break;
                default :
                    State.mdsEnabled.put(int_form, true);
                    break;
            }
            if (btn != null) {
                btn.setEnabled(true);
                //btn.setBackgroundColor(Color.WHITE);
                btn.setTextColor(Color.BLACK);
            }
        }
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_observe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
