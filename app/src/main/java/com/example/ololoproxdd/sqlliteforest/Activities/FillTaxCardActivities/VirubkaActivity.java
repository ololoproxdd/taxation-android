package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class VirubkaActivity extends AppCompatActivity {

    //region поля
    Videl selectedVidel = State.selectedVidel;
    NoteTaxation selectedNote = State.selectedNote;

    ArrayList<String> god_virubki = null;
    ArrayList<String> type_virubki = null;
    ArrayList<String> pnei = null;
    ArrayList<String> diametr = null;

    Spinner spinner_god_virubki = null;
    Spinner spinner_pnei_vsego = null;
    Spinner spinner_pnei_sosni = null;
    Spinner spinner_diametr = null;
    Spinner spinner_tip_virubki = null;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virubka);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Вырубка");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        if (validation){
            String errorMessage = "";
            //Год вырубки не соответствует десятилетнему диапазону от текущего года
            if (!spinner_god_virubki.getSelectedItem().toString().isEmpty()) {
                Integer god_vir = Integer.parseInt(spinner_god_virubki.getSelectedItem().toString());
                if (Math.abs(god_vir - Calendar.getInstance().get(Calendar.YEAR)) > 10)
                    errorMessage += "Проверь год вырубки!\n";
            }
            //обязательное поле
            else
                errorMessage += "Заполните обязательное поле ввода!\n";

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Вырубка");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save(){
        //int
        selectedVidel.godVirubki = spinner_god_virubki.getSelectedItem().toString();
        selectedVidel.pneyVsego = spinner_pnei_vsego.getSelectedItem().toString();
        selectedVidel.pneySosny = spinner_pnei_sosni.getSelectedItem().toString();
        selectedVidel.diameter = spinner_diametr.getSelectedItem().toString();

        //guid
        selectedVidel.tipVirubki = DatabaseProcedures.GetPK("тип_вырубки", "short_title", spinner_tip_virubki.getSelectedItem().toString());

        if (selectedNote != null)
            XmlHelper.writeXmlFile(selectedNote);

    }

    public void init(){
        //region начальная инициализация
        spinner_god_virubki = findViewById(R.id.spinner_god_virubki);
        ArrayAdapter<String> adapter_spinner_god_virubki = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, god_virubki);
        adapter_spinner_god_virubki.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_god_virubki.setAdapter(adapter_spinner_god_virubki);

        spinner_pnei_vsego = findViewById(R.id.spinner_pnei_vsego);
        ArrayAdapter<String> adapter_spinner_pnei_vsego = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pnei);
        adapter_spinner_pnei_vsego.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pnei_vsego.setAdapter(adapter_spinner_pnei_vsego);

        spinner_pnei_sosni = findViewById(R.id.spinner_pnei_sosni);
        ArrayAdapter<String> adapter_spinner_pnei_sosni = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pnei);
        adapter_spinner_pnei_sosni.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pnei_sosni.setAdapter(adapter_spinner_pnei_sosni);

        spinner_diametr = findViewById(R.id.spinner_diametr);
        ArrayAdapter<String> adapter_spinner_diametr = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, diametr);
        adapter_spinner_diametr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_diametr.setAdapter(adapter_spinner_diametr);

        spinner_tip_virubki = findViewById(R.id.spinner_tip_virubki);
        ArrayAdapter<String> adapter_spinner_tip_virubki = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, type_virubki);
        adapter_spinner_tip_virubki.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tip_virubki.setAdapter(adapter_spinner_tip_virubki);
        //endregion

        //region события
        spinner_god_virubki.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_pnei_vsego.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_pnei_sosni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_diametr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_tip_virubki.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null) {
            //год вырубки
            if (selectedVidel.godVirubki != null)
                spinner_god_virubki.setSelection(adapter_spinner_god_virubki.getPosition(selectedVidel.godVirubki));

            //пней всего
            if (selectedVidel.pneyVsego != null)
                spinner_pnei_vsego.setSelection(adapter_spinner_pnei_vsego.getPosition(selectedVidel.pneyVsego));

            //пней сосны
            if (selectedVidel.pneySosny != null)
                spinner_pnei_sosni.setSelection(adapter_spinner_pnei_sosni.getPosition(selectedVidel.pneySosny));

            //диаметр
            if (selectedVidel.diameter != null)
                spinner_diametr.setSelection(adapter_spinner_diametr.getPosition(selectedVidel.diameter));

            //тип вырубки
            if (selectedVidel.tipVirubki != null) {
                String sel_tip_vir = DatabaseProcedures.GetDataFromPK("тип_вырубки", "short_title", selectedVidel.tipVirubki);
                spinner_tip_virubki.setSelection(adapter_spinner_tip_virubki.getPosition(sel_tip_vir));
            }
        }
        //endregion
    }

    //region БД
    public void getDataFromDB(){
        god_virubki = DatabaseProcedures.getData("год_вырубки");
        type_virubki = DatabaseProcedures.getData("тип_вырубки", "short_title");
        pnei = DatabaseProcedures.getData("пней");
        diametr = DatabaseProcedures.getData("диаметр");
    }
    //endregion

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
