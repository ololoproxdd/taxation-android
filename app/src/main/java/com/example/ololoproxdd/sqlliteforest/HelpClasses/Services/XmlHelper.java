package com.example.ololoproxdd.sqlliteforest.HelpClasses.Services;

import android.os.Environment;
import android.util.Log;

import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Kvartal;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Lesnichestvo;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.XmlModels.UchastkovoeLesvo;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;

public class XmlHelper {

    //region поля
    public static ArrayList<NoteTaxation> noteTaxationList = new ArrayList<>();
    public static File directory;
    //endregion

    //region методы
    public static void initXmlHelper(){
        createDirectory();
    }

    public static void createDirectory() {
        directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "Taxation");
        if (!directory.exists())
            directory.mkdirs();
    }

    public static void readXmlFiles() {
        try {
            noteTaxationList.clear();
            File[] noteFiles = directory.listFiles();
            for (File noteFile : noteFiles) {
                String res = "";
                try {
                    FileInputStream inputStream = new FileInputStream(noteFile);
                    try {
                        res = convertStreamToString(inputStream);
                        Log.w("log", "onCreate: string from xml * " + res);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    Reader reader = new StringReader(res);
                    Persister serializer = new Persister();
                    NoteTaxation note = serializer.read(NoteTaxation.class, reader, false);
                    noteTaxationList.add(note);
                } catch (Exception e) {
                    Log.e("readXmlFiles", "error read");
                }
            }
        }
        catch (Exception e){
            Log.e("readXmlFiles", "error read");
        }
    }

    public static NoteTaxation readXmlFile(String filepath) {
        try {
            String res = "";
            File file = new File(filepath);
            FileInputStream inputStream = new FileInputStream(file);
            res = convertStreamToString(inputStream);
            inputStream.close();
            Reader reader = new StringReader(res);
            Persister serializer = new Persister();
            return serializer.read(NoteTaxation.class, reader, false);
        } catch (Exception e) {
            Log.e("readXmlFiles", "error read");
            return null;
        }
    }

    public static NoteTaxation writeXmlFile(NoteTaxation note){
        try
        {
            if (directory != null) {
                Writer writer = new StringWriter();
                Serializer serializer = new Persister();
                serializer.write(note, writer);
                String xml = writer.toString();

                if (note.Id == null || note.Id.isEmpty())
                    note.Id = java.util.UUID.randomUUID().toString();

                FileOutputStream outputStream = new FileOutputStream(new File(directory.getPath() + "/" + note.Id + ".xml"));
                outputStream.write(xml.getBytes());
                outputStream.close();
                noteTaxationList.add(note);
            }
            return note;
        }
        catch (Exception e)
        {
            Log.e("writeXmlFile", "error write");
            return null;
        }
    }

    public static NoteTaxation exportNote (NoteTaxation note){
        try {
            File exportDirectory = new File(Environment.getExternalStorageDirectory(), "export");
            if (!exportDirectory.exists())
                exportDirectory.mkdirs();
            Writer writer = new StringWriter();
            Serializer serializer = new Persister();
            NoteTaxation exportedNote = getActualData(note);
            serializer.write(exportedNote, writer);
            String xml = writer.toString();

            if (exportedNote.Id == null || exportedNote.Id.isEmpty())
                exportedNote.Id = java.util.UUID.randomUUID().toString();

            FileOutputStream outputStream = new FileOutputStream(new File(exportDirectory.getPath() + "/" + note.Name + "." + note.DateTime + ".xml"));
            outputStream.write(xml.getBytes());
            outputStream.close();
            return exportedNote;
        }
        catch (Exception e)
        {
            Log.e("exportNote", "error write");
            return null;
        }
    }

    public static NoteTaxation getActualData(NoteTaxation note) {
        NoteTaxation exportedNote = new NoteTaxation();

        exportedNote.Id = note.Id;
        exportedNote.DateTime = note.DateTime;
        exportedNote.ispolnitel = note.ispolnitel;
        exportedNote.Name = note.Name;
        for (Lesnichestvo lesnichestvo : note.lesnichestvos) {
            if (exportedNote.lesnichestvos == null)
                exportedNote.lesnichestvos = new ArrayList<>();
            Lesnichestvo exportedLesnichestvo = new Lesnichestvo();
            exportedLesnichestvo.lesnichestvoName = lesnichestvo.lesnichestvoName;

            for (UchastkovoeLesvo uchastkovoeLesvo : lesnichestvo.uchastkovoeLesvos) {
                if (exportedLesnichestvo.uchastkovoeLesvos == null)
                    exportedLesnichestvo.uchastkovoeLesvos = new ArrayList<>();
                UchastkovoeLesvo exportedUchlesvo = new UchastkovoeLesvo();
                exportedUchlesvo.uchLesvoName = uchastkovoeLesvo.uchLesvoName;

                for (Kvartal kvartal : uchastkovoeLesvo.kvartals) {
                    if (exportedUchlesvo.kvartals == null)
                        exportedUchlesvo.kvartals = new ArrayList<>();
                    Kvartal exportedKvartal = new Kvartal();
                    exportedKvartal.kvartalNomer = kvartal.kvartalNomer;
                    exportedKvartal.kvartalPloshad = kvartal.kvartalPloshad;

                    for (Videl videl : kvartal.videls) {
                        if (exportedKvartal.videls == null)
                            exportedKvartal.videls = new ArrayList<>();
                        Videl exportedVidel = getActualDataVidel(videl);

                        exportedKvartal.videls.add(exportedVidel);
                    }

                    exportedUchlesvo.kvartals.add(exportedKvartal);
                }

                exportedLesnichestvo.uchastkovoeLesvos.add(exportedUchlesvo);
            }

            exportedNote.lesnichestvos.add(exportedLesnichestvo);
        }
        return exportedNote;
    }

    public static Videl getActualDataVidel(Videl videl) {
        String cat_zemel_pk = videl.katZemel != null ? videl.katZemel : "1";
        String cat_zemel_code = DatabaseProcedures.GetDataFromPK("категория_земель_ТЗ", "code", cat_zemel_pk);
        String activeForms= DatabaseProcedures.runScript(
                "select форма_ввода_МДС from категория_форма_ввода_мдс where категория_земель_code = " + cat_zemel_code).get(0);

        Videl exportedVidel = new Videl();
        for (String form : activeForms.split(", ")) {
            int int_form = Integer.parseInt(form.trim());
            switch (int_form) {
                case 1:
                    //region Категория
                    exportedVidel.nomerVidela = videl.nomerVidela;
                    exportedVidel.ploshadVidela = videl.ploshadVidela;
                    exportedVidel.celevoeNaznachenie = videl.celevoeNaznachenie;
                    exportedVidel.katZashit = videl.katZashit;
                    exportedVidel.katZemel = videl.katZemel;
                    exportedVidel.OZU = videl.OZU;
                    exportedVidel.exposSklona = videl.exposSklona;
                    exportedVidel.krutSklona = videl.krutSklona;
                    exportedVidel.visotaNadMorem = videl.visotaNadMorem;
                    exportedVidel.erosiaVid = videl.erosiaVid;
                    exportedVidel.erosiaStepen = videl.erosiaStepen;
                    //endregion
                    break;
                case 2:
                    //region Ярус
                    exportedVidel.tipLesa = videl.tipLesa;
                    exportedVidel.TLU = videl.TLU;
                    exportedVidel.preoblPoroda = videl.preoblPoroda;
                    exportedVidel.bonitet = videl.bonitet;
                    exportedVidel.groupVozrasta = videl.groupVozrasta;
                    exportedVidel.zahlam = videl.zahlam;
                    exportedVidel.syhostoj = videl.syhostoj;
                    exportedVidel.likvidnost = videl.likvidnost;
                    exportedVidel.yarus1 = videl.yarus1;
                    exportedVidel.yarus2 = videl.yarus2;
                    exportedVidel.yarus3 = videl.yarus3;
                    //endregion
                    break;
                case 3:
                    //region Вырубка
                    exportedVidel.tipVirubki = videl.tipVirubki;
                    exportedVidel.godVirubki = videl.godVirubki;
                    exportedVidel.pneyVsego = videl.pneyVsego;
                    exportedVidel.pneySosny = videl.pneySosny;
                    exportedVidel.diameter = videl.diameter;
                    //endregion
                    break;
                case 4:
                    //region Мероприятия
                    exportedVidel.celPoroda = videl.celPoroda;
                    exportedVidel.meropr1Vid = videl.meropr1Vid;
                    exportedVidel.meropr1Proc = videl.meropr1Proc;
                    exportedVidel.meropr1RTK = videl.meropr1RTK;
                    exportedVidel.meropr2Vid = videl.meropr2Vid;
                    exportedVidel.meropr2RTK = videl.meropr2RTK;
                    exportedVidel.meropr3Vid = videl.meropr3Vid;
                    exportedVidel.meropr3RTK = videl.meropr3RTK;
                    //endregion
                    break;
                case 5:
                    //region Подрост подлесок
                    exportedVidel.podrostGustota = videl.podrostGustota;
                    exportedVidel.podrostVozrast = videl.podrostVozrast;
                    exportedVidel.podrostVisota = videl.podrostVisota;
                    exportedVidel.podrostPoroda1 = videl.podrostPoroda1;
                    exportedVidel.podrostPoroda2 = videl.podrostPoroda2;
                    exportedVidel.podrostPoroda3 = videl.podrostPoroda3;
                    exportedVidel.podrostCoef1 = videl.podrostCoef1;
                    exportedVidel.podrostCoef2 = videl.podrostCoef2;
                    exportedVidel.podrostCoef3 = videl.podrostCoef3;
                    exportedVidel.podlesokGustota = videl.podlesokGustota;
                    exportedVidel.podlesokPoroda1 = videl.podlesokPoroda1;
                    exportedVidel.podlesokPoroda2 = videl.podlesokPoroda2;
                    exportedVidel.podlesokPoroda3 = videl.podlesokPoroda3;
                    //endregion
                    break;
                case 7:
                    //region GPS
                    exportedVidel.GPS = videl.GPS;
                    //endregion
                    break;
                case 8:
                    //region Примечания
                    exportedVidel.note = videl.note;
                    //endregion
                    break;

                case 11:
                    exportedVidel.maket11 = videl.maket11;
                    break;
                case 12:
                    exportedVidel.maket12 = videl.maket12;
                    break;
                case 13:
                    exportedVidel.maket13 = videl.maket13;
                    break;
                case 17:
                    exportedVidel.maket17 = videl.maket17;
                    break;
                case 19:
                    exportedVidel.maket19 = videl.maket19;
                    break;
                case 21:
                    exportedVidel.maket21 = videl.maket21;
                    break;
                case 23:
                    exportedVidel.maket23 = videl.maket23;
                    break;
            }
        }
        return exportedVidel;
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static void deleteXmlFile(NoteTaxation noteTaxation){
        try {
            noteTaxationList.remove(noteTaxation);
            File[] noteFiles = directory.listFiles();
            for (File noteFile : noteFiles) {
                if (noteFile.getName().equals(noteTaxation.Id + ".xml")) {
                    if (!noteFile.delete())
                        Log.d("deleteXmlFile", "error delete");
                    return;
                }
            }
        }
        catch (Exception e){
            Log.e("deleteXmlFile", "error delete");
        }
    }
    //endregion
}
