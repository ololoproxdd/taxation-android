package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket13;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;

public class LineinieObjectActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_sost = null;
    Spinner spinner_naznach = null;
    Spinner spinner_tip_pokryt = null;
    Spinner spinner_sezonnost = null;
    EditText shirina = null;
    EditText protyaj = null;
    EditText shirina_proezj = null;
    EditText dlina = null;

    ArrayList<String> sostoyanie = null;
    ArrayList<String> naznach = null;
    ArrayList<String> tip_pokryt = null;
    ArrayList<String> sezonnost = null;

    Videl selectedVidel = State.selectedVidel;
    Maket13 maket13 = selectedVidel.maket13;

    boolean close = false;
//endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lineinie_object);
        getSupportActionBar().setTitle("Линейный объект");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        setDefaultValues();
        if (validation){
            String errorMessage = "";
            //обязатаельные поля для ввода
            if (shirina.getText().toString().isEmpty() ||
                    protyaj.getText().toString().isEmpty() ||
                    selectedVidel.katZemel.equals("74") && spinner_sost.getSelectedItem().toString().equals("0") ||
                    ((selectedVidel.katZemel.equals("15") ||
                    selectedVidel.katZemel.equals("25") ||
                    selectedVidel.katZemel.equals("26") ||
                    selectedVidel.katZemel.equals("27") ||
                    selectedVidel.katZemel.equals("29") ||
                    selectedVidel.katZemel.equals("74")) &&
                    (spinner_naznach.getSelectedItem().toString().equals("0") ||
                    spinner_tip_pokryt.getSelectedItem().toString().equals("0") ||
                    shirina_proezj.getText().toString().isEmpty() ||
                    spinner_sezonnost.getSelectedItem().toString().equals("0"))))
                errorMessage += "Заполните обязательное поле ввода!\n";

            //Длина требуемого мероприятия не должна превышать значение длина объекта (2 поле)
            Integer protyaj_v_metrah = Integer.parseInt(protyaj.getText().toString()) * 1000;
            Integer dl = Integer.parseInt(dlina.getText().toString());
            if (dl > protyaj_v_metrah)
                errorMessage += "Длина требуемого мероприятия не должна превышать значение ширина!\n";

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Линейные объекты");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save(){

        setDefaultValues();
        //int
        maket13.shirina = shirina.getText().toString();
        maket13.protyaj = protyaj.getText().toString();
        maket13.shirinaDorogi = shirina_proezj.getText().toString();
        maket13.dlinaMeropr = dlina.getText().toString();

        //guid

        maket13.sostojan = DatabaseProcedures.GetPKCode("состояние_линейн_объект", spinner_sost.getSelectedItem().toString());
        maket13.naznachDorogi = DatabaseProcedures.GetPKCode("назначение_дороги", spinner_naznach.getSelectedItem().toString());
        maket13.tipPokrutija = DatabaseProcedures.GetPKCode("тип_покрытия", spinner_tip_pokryt.getSelectedItem().toString());
        maket13.sezonnost = DatabaseProcedures.GetPKCode("сезонность_действия", spinner_sezonnost.getSelectedItem().toString());


        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues(){
        if (shirina.getText().toString().isEmpty())
            shirina.setText("0");

        if (protyaj.getText().toString().isEmpty())
            protyaj.setText("0");

        if (shirina_proezj.getText().toString().isEmpty())
            shirina_proezj.setText("0");

        if (dlina.getText().toString().isEmpty())
            dlina.setText("0");
    }

    public void init() {
        //region начальная инициализация
        spinner_sost = findViewById(R.id.spinner_sost);
        ArrayAdapter<String> adapter_spinner_sost = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sostoyanie);
        adapter_spinner_sost.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sost.setAdapter(adapter_spinner_sost);

        spinner_naznach = findViewById(R.id.spinner_naznach);
        ArrayAdapter<String> adapter_spinner_naznach = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, naznach);
        adapter_spinner_naznach.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_naznach.setAdapter(adapter_spinner_naznach);

        spinner_tip_pokryt = findViewById(R.id.spinner_tip_pokryt);
        ArrayAdapter<String> adapter_spinner_tip_pokryt = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tip_pokryt);
        adapter_spinner_tip_pokryt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tip_pokryt.setAdapter(adapter_spinner_tip_pokryt);

        spinner_sezonnost = findViewById(R.id.spinner_sezonnost);
        ArrayAdapter<String> adapter_spinner_sezonnost = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sezonnost);
        adapter_spinner_sezonnost.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sezonnost.setAdapter(adapter_spinner_sezonnost);

        shirina = findViewById(R.id.edit_shirina);
        shirina.setText("0");

        protyaj = findViewById(R.id.edit_protyaj);
        protyaj.setText("0");

        shirina_proezj = findViewById(R.id.edit_shirina_proezj_chasti);
        shirina_proezj.setText("0");

        dlina = findViewById(R.id.edit_dlina_treb);
        dlina.setText("0");

        //Для просек, границ, противопож.разрывов обязательно состояние (3 поле).
        if (selectedVidel.katZemel.equals("74"))
            spinner_sost.setBackgroundResource(R.drawable.draw_et_req);
        else
            spinner_sost.setBackgroundResource(R.drawable.draw_et_not_req);

        //Для дорог обязательны к заполнению 4,5,6,7 поля.
        if (selectedVidel.katZemel.equals("15") ||
                selectedVidel.katZemel.equals("25") ||
                selectedVidel.katZemel.equals("26") ||
                selectedVidel.katZemel.equals("27") ||
                selectedVidel.katZemel.equals("29") ||
                selectedVidel.katZemel.equals("74")) {
            spinner_naznach.setBackgroundResource(R.drawable.draw_et_req);
            spinner_tip_pokryt.setBackgroundResource(R.drawable.draw_et_req);
            shirina_proezj.setBackgroundResource(R.drawable.draw_et_req);
            spinner_sezonnost.setBackgroundResource(R.drawable.draw_et_req);
        }
        else{
            spinner_naznach.setBackgroundResource(R.drawable.draw_et_not_req);
            spinner_tip_pokryt.setBackgroundResource(R.drawable.draw_et_not_req);
            shirina_proezj.setBackgroundResource(R.drawable.draw_et_not_req);
            spinner_sezonnost.setBackgroundResource(R.drawable.draw_et_not_req);
        }

        //endregion

        //region события
        shirina.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        shirina.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        protyaj.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        protyaj.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        shirina_proezj.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        shirina_proezj.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        dlina.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        dlina.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });


        spinner_sost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_naznach.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_tip_pokryt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_sezonnost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null && maket13 != null) {
            //ширина
            if (maket13.shirina != null)
                shirina.setText(maket13.shirina);

            //протяжённость
            if (maket13.protyaj != null)
                protyaj.setText(maket13.protyaj);

            //ширина проезжей части
            if (maket13.shirinaDorogi != null)
                shirina_proezj.setText(maket13.shirinaDorogi);

            //длина треб меропр
            if (maket13.dlinaMeropr != null)
                dlina.setText(maket13.dlinaMeropr);


            //состояние
            if (maket13.sostojan != null){
                String sost = DatabaseProcedures.GetDataFromPK("состояние_линейн_объект", "code", "name", maket13.sostojan);
                spinner_sost.setSelection(adapter_spinner_sost.getPosition(sost));
            }

            //назначение дороги
            if (maket13.naznachDorogi != null){
                String naznach = DatabaseProcedures.GetDataFromPK("назначение_дороги", "code", "name", maket13.naznachDorogi);
                spinner_naznach.setSelection(adapter_spinner_naznach.getPosition(naznach));
            }

            //тип покрытия
            if (maket13.tipPokrutija != null){
                String pokryt = DatabaseProcedures.GetDataFromPK("тип_покрытия", "code", "name", maket13.tipPokrutija);
                spinner_tip_pokryt.setSelection(adapter_spinner_tip_pokryt.getPosition(pokryt));
            }

            //сезонность действия
            if (maket13.sezonnost != null){
                String sezon = DatabaseProcedures.GetDataFromPK("сезонность_действия", "code", "name", maket13.sezonnost);
                spinner_sezonnost.setSelection(adapter_spinner_sezonnost.getPosition(sezon));
            }


        }

    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        sostoyanie = DatabaseProcedures.getData("состояние_линейн_объект", "code", "name");
        naznach = DatabaseProcedures.getData("назначение_дороги", "code", "name");
        tip_pokryt = DatabaseProcedures.getData("тип_покрытия", "code", "name");
        sezonnost = DatabaseProcedures.getData("сезонность_действия", "code", "name");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
