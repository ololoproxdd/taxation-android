package com.example.ololoproxdd.sqlliteforest.XmlModels.Mds;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Доп_макет13")
public class Maket13 {

    public void setDefault() {
        shirina = "0";
        protyaj = "0";
        sostojan = "5a8d30b4-cb8c-4687-9649-0e59f3574318";
        naznachDorogi = "affd0a43-cb36-41e3-b409-02ca5b69be26";
        tipPokrutija = "e2f02b7a-3794-4817-b619-02e718e11b04";
        shirinaDorogi = "0";
        sezonnost = "3fc374b7-fbd5-4b1a-a1fa-89cb06aa87ff";
        dlinaMeropr = "0";
    }

    //region поля
    @Element(required = false, name = "Ширина")
    public String shirina;

    @Element(required = false, name = "Протяженность")
    public String protyaj;

    @Element(required = false, name = "Состояние")
    public String sostojan;

    @Element(required = false, name = "Назначение_дороги")
    public String naznachDorogi;

    @Element(required = false, name = "Тип_покрытия_дороги")
    public String tipPokrutija;

    @Element(required = false, name = "Ширина_проезжей_части")
    public String shirinaDorogi;

    @Element(required = false, name = "Сезонность")
    public String sezonnost;

    @Element(required = false, name = "Длина_треб_меропр")
    public String dlinaMeropr;
    //endregion
}
