package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;

public class EventActivity extends AppCompatActivity {

    //region поля
    EditText event1_proc = null;
    EditText event1_rtk = null;
    EditText event2_rtk = null;
    EditText event3_rtk = null;
    Spinner spinner_type_event1 = null;
    Spinner spinner_type_event2 = null;
    Spinner spinner_type_event3 = null;
    Spinner spinner_cel_poroda = null;


    ArrayList<String> type_event_list = null;
    ArrayList<String> cel_poroda_list = null;

    Videl selectedVidel = State.selectedVidel;
    NoteTaxation selectedNote = State.selectedNote;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Мероприятия");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        setDefaultValues();
        if (validation){
            String errorMessage = "";
            //Процент выборки по мероприятию более 100%
            Integer proc = Integer.parseInt(event1_proc.getText().toString());
            if (proc > 100)
                errorMessage += "Проверь процент вырубки!\n";

            //После сплошной рубки не назначено второе лесовосстановительное мероприятие
            if (spinner_type_event1.getSelectedItem().toString().equals("Сплошная рубка")
                    && spinner_type_event2.getSelectedItem().toString().equals("-")
                    && spinner_type_event3.getSelectedItem().toString().equals("-"))
                errorMessage += "Проверь мероприятие!\n";

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Мероприятие");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save(){
        setDefaultValues();
        //int
        selectedVidel.meropr1Proc = event1_proc.getText().toString();
        selectedVidel.meropr1RTK = event1_rtk.getText().toString();
        selectedVidel.meropr2RTK = event2_rtk.getText().toString();
        selectedVidel.meropr3RTK = event3_rtk.getText().toString();

        //guid
        selectedVidel.meropr1Vid = DatabaseProcedures.GetPK("мероприятия_ТЗ", "name", spinner_type_event1.getSelectedItem().toString());
        selectedVidel.meropr2Vid = DatabaseProcedures.GetPK("мероприятия_ТЗ", "name", spinner_type_event2.getSelectedItem().toString());
        selectedVidel.meropr3Vid = DatabaseProcedures.GetPK("мероприятия_ТЗ", "name", spinner_type_event3.getSelectedItem().toString());
        selectedVidel.celPoroda = DatabaseProcedures.GetPK("порода_ярус", "name", spinner_cel_poroda.getSelectedItem().toString());

        if (selectedNote != null)
            XmlHelper.writeXmlFile(selectedNote);
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues(){
        if (event1_proc.getText().toString().isEmpty())
            event1_proc.setText("0");

        if (event1_rtk.getText().toString().isEmpty())
            event1_rtk.setText("0");

        if (event2_rtk.getText().toString().isEmpty())
            event2_rtk.setText("0");

        if (event3_rtk.getText().toString().isEmpty())
            event3_rtk.setText("0");
    }

    public void init(){
        //region начальная инициализация
        //edit text
        event1_proc = findViewById(R.id.event1_proc);
        event1_rtk = findViewById(R.id.event1_rtk);
        event2_rtk = findViewById(R.id.event2_rtk);
        event3_rtk = findViewById(R.id.event3_rtk);

        //spinners
        spinner_cel_poroda = findViewById(R.id.cel_poroda);
        ArrayAdapter<String> adapter_spinner_cel_poroda = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cel_poroda_list);
        adapter_spinner_cel_poroda.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_cel_poroda.setAdapter(adapter_spinner_cel_poroda);

        spinner_type_event1 = findViewById(R.id.type_event1);
        ArrayAdapter<String> adapter_spinner_type_event1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, type_event_list);
        adapter_spinner_type_event1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type_event1.setAdapter(adapter_spinner_type_event1);

        spinner_type_event2 = findViewById(R.id.type_event2);
        ArrayAdapter<String> adapter_spinner_type_event2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, type_event_list);
        adapter_spinner_type_event2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type_event2.setAdapter(adapter_spinner_type_event2);

        spinner_type_event3 = findViewById(R.id.type_event3);
        ArrayAdapter<String> adapter_spinner_type_event3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, type_event_list);
        adapter_spinner_type_event3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type_event3.setAdapter(adapter_spinner_type_event3);
        //endregion

        //region события
        event1_proc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        event1_proc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        event1_rtk.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        event1_rtk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        event2_rtk.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        event2_rtk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        event3_rtk.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        event3_rtk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        spinner_cel_poroda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        spinner_type_event1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        spinner_type_event2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        spinner_type_event3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null) {
            //меропр1. процент выборки
            if (selectedVidel.meropr1Proc != null)
                event1_proc.setText(selectedVidel.meropr1Proc);

            //меропр1. ртк
            if (selectedVidel.meropr1RTK != null)
                event1_rtk.setText(selectedVidel.meropr1RTK);

            //меропр2. ртк
            if (selectedVidel.meropr1RTK != null)
                event2_rtk.setText(selectedVidel.meropr2RTK);

            //меропр3. ртк
            if (selectedVidel.meropr1RTK != null)
                event3_rtk.setText(selectedVidel.meropr3RTK);

            // целевая порода
            if (selectedVidel.celPoroda != null) {
                String cel_por = DatabaseProcedures.GetDataFromPK("порода_ярус", "name", selectedVidel.celPoroda);
                spinner_cel_poroda.setSelection(adapter_spinner_cel_poroda.getPosition(cel_por));
            }

            //меропр1. вид меропр
            if (selectedVidel.meropr1Vid != null) {
                String sel_mer1_vid = DatabaseProcedures.GetDataFromPK("мероприятия_ТЗ", "name", selectedVidel.meropr1Vid);
                spinner_type_event1.setSelection(adapter_spinner_type_event1.getPosition(sel_mer1_vid));
            }

            //меропр2. вид меропр
            if (selectedVidel.meropr2Vid != null) {
                String sel_mer2_vid = DatabaseProcedures.GetDataFromPK("мероприятия_ТЗ", "name", selectedVidel.meropr2Vid);
                spinner_type_event2.setSelection(adapter_spinner_type_event2.getPosition(sel_mer2_vid));
            }

            //меропр3. вид меропр
            if (selectedVidel.meropr3Vid != null) {
                String sel_mer3_vid = DatabaseProcedures.GetDataFromPK("мероприятия_ТЗ", "name", selectedVidel.meropr3Vid);
                spinner_type_event3.setSelection(adapter_spinner_type_event3.getPosition(sel_mer3_vid));
            }

        }
        //endregion
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        type_event_list = DatabaseProcedures.getDataActual("мероприятия_ТЗ");
        cel_poroda_list = DatabaseProcedures.getData("порода_ярус");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
