package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket12;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class PovrejdNasajdActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_tip_povrejd = null;
    Spinner spinner_povrejd_poroda = null;
    Spinner spinner_bolezn1 = null;
    Spinner spinner_stepen1 = null;
    Spinner spinner_bolezn2 = null;
    Spinner spinner_stepen2 = null;
    EditText god_povrejd = null;
    EditText istochnik = null;

    ArrayList<String> tip_povrejd = null;
    ArrayList<String> povrejd_poroda = null;
    ArrayList<String> bolezn = null;
    ArrayList<String> stepen = null;

    Videl selectedVidel = State.selectedVidel;
    Maket12 maket12 = selectedVidel.maket12;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_povrejd_nasajd);
        getSupportActionBar().setTitle("Повреждение насаждений");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        setDefaultValues();
        if (validation){
            String errorMessage = "";
            //обязатаельные поля для ввода
            if (spinner_tip_povrejd.getSelectedItem().toString().isEmpty() ||
                    god_povrejd.getText().toString().isEmpty())
                errorMessage += "Заполните обязательное поле ввода!\n";

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Повреждение насаждений");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();

        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();

        return true;
    }

    public void save(){

        setDefaultValues();
        //int
        maket12.godPovrejd = god_povrejd.getText().toString();
        maket12.istochnik = istochnik.getText().toString();

        //guid
        maket12.tipPovrejd = DatabaseProcedures.GetPKCode("причина_неуд_сост", spinner_tip_povrejd.getSelectedItem().toString());
        maket12.povrejdPoroda = DatabaseProcedures.GetPKCode("порода_ярус", spinner_povrejd_poroda.getSelectedItem().toString());
        maket12.bolezn1 = DatabaseProcedures.GetPKCode("болезнь", spinner_bolezn1.getSelectedItem().toString());
        maket12.bolezn2 = DatabaseProcedures.GetPKCode("болезнь", spinner_bolezn2.getSelectedItem().toString());
        maket12.stepen1 = DatabaseProcedures.GetPKCode("степень_поврежд", spinner_stepen1.getSelectedItem().toString());
        maket12.stepen2 = DatabaseProcedures.GetPKCode("степень_поврежд", spinner_stepen2.getSelectedItem().toString());

        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues(){
        if (god_povrejd.getText().toString().isEmpty())
            god_povrejd.setText(Calendar.getInstance().get(Calendar.YEAR));

        if (istochnik.getText().toString().isEmpty())
            istochnik.setText("0");
    }

    public void init() {
        //region начальная инициализация
        spinner_tip_povrejd = findViewById(R.id.spinner_tip_povrejd);
        ArrayAdapter<String> adapter_spinner_tip_povrejd = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tip_povrejd);
        adapter_spinner_tip_povrejd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tip_povrejd.setAdapter(adapter_spinner_tip_povrejd);

        spinner_povrejd_poroda = findViewById(R.id.spinner_povrejd_poroda);
        ArrayAdapter<String> adapter_spinner_povrejd_poroda = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, povrejd_poroda);
        adapter_spinner_povrejd_poroda.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_povrejd_poroda.setAdapter(adapter_spinner_povrejd_poroda);

        spinner_bolezn1 = findViewById(R.id.spinner_bolezn1);
        ArrayAdapter<String> adapter_spinner_bolezn1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bolezn);
        adapter_spinner_bolezn1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_bolezn1.setAdapter(adapter_spinner_bolezn1);

        spinner_stepen1 = findViewById(R.id.spinner_stepen1);
        ArrayAdapter<String> adapter_spinner_stepen1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stepen);
        adapter_spinner_stepen1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_stepen1.setAdapter(adapter_spinner_stepen1);

        spinner_bolezn2 = findViewById(R.id.spinner_bolezn2);
        ArrayAdapter<String> adapter_spinner_bolezn2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bolezn);
        adapter_spinner_bolezn2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_bolezn2.setAdapter(adapter_spinner_bolezn2);

        spinner_stepen2 = findViewById(R.id.spinner_stepen2);
        ArrayAdapter<String> adapter_spinner_stepen2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stepen);
        adapter_spinner_stepen2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_stepen2.setAdapter(adapter_spinner_stepen2);

        istochnik = findViewById(R.id.edit_istochnik);
        istochnik.setText("0");

        god_povrejd = findViewById(R.id.edit_god_povrejd);
        god_povrejd.setText("2018");
        //endregion

        //region события
        spinner_tip_povrejd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Integer code = Integer.parseInt(spinner_tip_povrejd.getSelectedItem().toString().split("-")[0]);
                boolean enable = true;
                if (code >= 0 && code <= 6) {
                    enable = false;
                    spinner_bolezn1.setSelection(0);
                    spinner_bolezn2.setSelection(0);
                    spinner_stepen1.setSelection(0);
                    spinner_stepen2.setSelection(0);
                }

                spinner_bolezn1.setEnabled(enable);
                spinner_bolezn2.setEnabled(enable);
                spinner_stepen1.setEnabled(enable);
                spinner_stepen2.setEnabled(enable);

                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_povrejd_poroda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_bolezn1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_stepen1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_bolezn2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_stepen2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        god_povrejd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        istochnik.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        istochnik.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null && maket12 != null) {
            //год повреждения
            if (maket12.godPovrejd != null)
                god_povrejd.setText(maket12.godPovrejd);

            //источник вред возд
            if (maket12.istochnik != null)
                istochnik.setText(maket12.istochnik);

            //тип повреждения
            if (maket12.tipPovrejd!= null){
                String tip_povr = DatabaseProcedures.GetDataFromPK("причина_неуд_сост", "code", "name", maket12.tipPovrejd);
                spinner_tip_povrejd.setSelection(adapter_spinner_tip_povrejd.getPosition(tip_povr));
            }

            //поврежденная порода
            if (maket12.povrejdPoroda!= null){
                String povr_poroda = DatabaseProcedures.GetDataFromPK("порода_ярус", "code", maket12.povrejdPoroda);
                spinner_povrejd_poroda.setSelection(adapter_spinner_povrejd_poroda.getPosition(povr_poroda));
            }

            //болезнь1
            if (maket12.bolezn1!= null){
                String bolezn1 = DatabaseProcedures.GetDataFromPK("болезнь", "code", "name", maket12.bolezn1);
                spinner_bolezn1.setSelection(adapter_spinner_bolezn1.getPosition(bolezn1));
            }

            //болезнь2
            if (maket12.bolezn2!= null){
                String bolezn2 = DatabaseProcedures.GetDataFromPK("болезнь", "code", "name", maket12.bolezn2);
                spinner_bolezn2.setSelection(adapter_spinner_bolezn2.getPosition(bolezn2));
            }

            //степень1
            if (maket12.stepen1!= null){
                String stepen1 = DatabaseProcedures.GetDataFromPK("степень_поврежд", "code", "name", maket12.stepen1);
                spinner_stepen1.setSelection(adapter_spinner_stepen1.getPosition(stepen1));
            }

            //степень2
            if (maket12.stepen2!= null){
                String stepen2 = DatabaseProcedures.GetDataFromPK("степень_поврежд", "code", "name", maket12.stepen2);
                spinner_stepen2.setSelection(adapter_spinner_stepen2.getPosition(stepen2));
            }

        }
        //endregion
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        tip_povrejd = DatabaseProcedures.getData("причина_неуд_сост", "code", "name");
        povrejd_poroda = DatabaseProcedures.getDataActual("порода_ярус", "code");
        bolezn = DatabaseProcedures.getData("болезнь", "code", "name");
        stepen = DatabaseProcedures.getData("степень_поврежд", "code", "name");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
