package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities;

import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket23;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;

public class OsobenVidelaActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_osobennost1 = null;
    Spinner spinner_osobennost2 = null;
    Spinner spinner_osobennost3 = null;
    Spinner spinner_osobennost4 = null;
    Spinner spinner_osobennost5 = null;
    Spinner spinner_osobennost6 = null;
    Spinner spinner_osobennost7 = null;
    Spinner spinner_osobennost8 = null;

    ArrayList<String> osobennosti = null;

    Videl selectedVidel = State.selectedVidel;
    Maket23 maket23 = selectedVidel.maket23;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (selectedVidel.maket23 == null)
            selectedVidel.maket23 = new Maket23();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_osoben_videla);
        getSupportActionBar().setTitle("Особенность выдела");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    protected void onPause(){
        save();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();

        super.onBackPressed();  // optional depending on your needs
    }
    public void save(){
        //guid

        maket23.osobennost1 = DatabaseProcedures.GetPKCode("особенности_ТЗ", spinner_osobennost1.getSelectedItem().toString());
        maket23.osobennost2 = DatabaseProcedures.GetPKCode("особенности_ТЗ", spinner_osobennost2.getSelectedItem().toString());
        maket23.osobennost3 = DatabaseProcedures.GetPKCode("особенности_ТЗ", spinner_osobennost3.getSelectedItem().toString());
        maket23.osobennost4 = DatabaseProcedures.GetPKCode("особенности_ТЗ", spinner_osobennost4.getSelectedItem().toString());
        maket23.osobennost5 = DatabaseProcedures.GetPKCode("особенности_ТЗ", spinner_osobennost5.getSelectedItem().toString());
        maket23.osobennost6 = DatabaseProcedures.GetPKCode("особенности_ТЗ", spinner_osobennost6.getSelectedItem().toString());
        maket23.osobennost7 = DatabaseProcedures.GetPKCode("особенности_ТЗ", spinner_osobennost7.getSelectedItem().toString());
        maket23.osobennost8 = DatabaseProcedures.GetPKCode("особенности_ТЗ", spinner_osobennost8.getSelectedItem().toString());



        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    public void init() {
        //region начальная инициализация
        spinner_osobennost1 = findViewById(R.id.spinner_osobennost1);
        ArrayAdapter<String> adapter_spinner_osobennost1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, osobennosti);
        adapter_spinner_osobennost1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_osobennost1.setAdapter(adapter_spinner_osobennost1);

        spinner_osobennost2 = findViewById(R.id.spinner_osobennost2);
        ArrayAdapter<String> adapter_spinner_osobennost2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, osobennosti);
        adapter_spinner_osobennost2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_osobennost2.setAdapter(adapter_spinner_osobennost2);

        spinner_osobennost3 = findViewById(R.id.spinner_osobennost3);
        ArrayAdapter<String> adapter_spinner_osobennost3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, osobennosti);
        adapter_spinner_osobennost3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_osobennost3.setAdapter(adapter_spinner_osobennost3);

        spinner_osobennost4 = findViewById(R.id.spinner_osobennost4);
        ArrayAdapter<String> adapter_spinner_osobennost4 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, osobennosti);
        adapter_spinner_osobennost4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_osobennost4.setAdapter(adapter_spinner_osobennost4);

        spinner_osobennost5 = findViewById(R.id.spinner_osobennost5);
        ArrayAdapter<String> adapter_spinner_osobennost5 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, osobennosti);
        adapter_spinner_osobennost5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_osobennost5.setAdapter(adapter_spinner_osobennost5);

        spinner_osobennost6 = findViewById(R.id.spinner_osobennost6);
        ArrayAdapter<String> adapter_spinner_osobennost6 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, osobennosti);
        adapter_spinner_osobennost6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_osobennost6.setAdapter(adapter_spinner_osobennost6);

        spinner_osobennost7 = findViewById(R.id.spinner_osobennost7);
        ArrayAdapter<String> adapter_spinner_osobennost7 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, osobennosti);
        adapter_spinner_osobennost7.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_osobennost7.setAdapter(adapter_spinner_osobennost7);

        spinner_osobennost8 = findViewById(R.id.spinner_osobennost8);
        ArrayAdapter<String> adapter_spinner_osobennost8 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, osobennosti);
        adapter_spinner_osobennost8.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_osobennost8.setAdapter(adapter_spinner_osobennost8);
        //endregion

        //region события
        spinner_osobennost1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_osobennost2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_osobennost3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_osobennost4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_osobennost5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_osobennost6.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_osobennost7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_osobennost8.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null && maket23 != null) {

            if (maket23.osobennost1 != null) {
                String osob1 = DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", "name", maket23.osobennost1);
                spinner_osobennost1.setSelection(adapter_spinner_osobennost1.getPosition(osob1));
            }

            if (maket23.osobennost2 != null) {
                String osob2 = DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", "name", maket23.osobennost2);
                spinner_osobennost2.setSelection(adapter_spinner_osobennost2.getPosition(osob2));
            }

            if (maket23.osobennost3 != null) {
                String osob3 = DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", "name", maket23.osobennost3);
                spinner_osobennost3.setSelection(adapter_spinner_osobennost3.getPosition(osob3));
            }

            if (maket23.osobennost4 != null) {
                String osob4 = DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", "name", maket23.osobennost4);
                spinner_osobennost4.setSelection(adapter_spinner_osobennost4.getPosition(osob4));
            }

            if (maket23.osobennost5 != null) {
                String osob5 = DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", "name", maket23.osobennost5);
                spinner_osobennost5.setSelection(adapter_spinner_osobennost5.getPosition(osob5));
            }

            if (maket23.osobennost6 != null) {
                String osob6 = DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", "name", maket23.osobennost6);
                spinner_osobennost6.setSelection(adapter_spinner_osobennost6.getPosition(osob6));
            }

            if (maket23.osobennost7 != null) {
                String osob7 = DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", "name", maket23.osobennost7);
                spinner_osobennost7.setSelection(adapter_spinner_osobennost7.getPosition(osob7));
            }

            if (maket23.osobennost8 != null) {
                String osob8 = DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", "name", maket23.osobennost8);
                spinner_osobennost8.setSelection(adapter_spinner_osobennost8.getPosition(osob8));
            }

        }
        //endregion
        
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        osobennosti = DatabaseProcedures.getDataActual("особенности_ТЗ", "code", "name");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
                toast.show();

                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
