package com.example.ololoproxdd.sqlliteforest.XmlModels;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;

@Root(name="Участ_Лесничество")
public class UchastkovoeLesvo  implements Serializable {

    //region методы
    public UchastkovoeLesvo(){
        kvartals = new ArrayList<>();
    }

    public ArrayList<String> kvartalsNames() {
        ArrayList<String> kvrtalsNames = new ArrayList<>();
        if (kvartals != null) {
            for (int i = 0; i < kvartals.size(); i++) {
                String name = kvartals.get(i).kvartalNomer;
                if (name != null)
                    kvrtalsNames.add(name);
            }
        }
        return kvrtalsNames;
    }

    public void setDefault() {
        uchLesvoName = "";
        if (kvartals != null) {
            for (int i = 0; i < kvartals.size(); i++)
                kvartals.get(i).setDefault();
        }
    }
    //endregion

    //region поля
    @Element(required = false, name = "Участ_Лесничество_название")
    public String uchLesvoName;

    @ElementList(inline=true, required = false, name = "Квартал")
    public ArrayList<Kvartal> kvartals;
    //endregion
}
