package com.example.ololoproxdd.sqlliteforest.XmlModels.Mds;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Доп_макет11")
public class Maket11 {
    //region методы
    public void setDefault() {
        godSozd = "2005";
        sposobObrabotki = "0";
        sposobSozd = "dcf63b37-91d8-4e0a-82a6-a96065dd7a4e";
        mejduRyad = "0";
        vRyad = "0";
        kolvo = "0";
        sostoyanie = "8d2ecc44-8837-45a0-84ac-906d122829fb";
        prichinaNeudSost = "df867025-a5f3-44d9-8d8b-46d730eed74c";
    }
    //endregion

    //region поля
    @Element(required = false, name = "Год")
    public String godSozd;

    @Element(required = false, name = "Способ_обработки")
    public String sposobObrabotki;

    @Element(required = false, name = "Способ_создания")
    public String sposobSozd;

    @Element(required = false, name = "Расст_между_ряд")
    public String mejduRyad;

    @Element(required = false, name = "Расст_в_ряд")
    public String vRyad;

    @Element(required = false, name = "Количество")
    public String kolvo;

    @Element(required = false, name = "Cостояние")
    public String sostoyanie;

    @Element(required = false, name = "Причина_гибели")
    public String prichinaNeudSost;
    //endregion
}
