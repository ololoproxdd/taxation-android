package com.example.ololoproxdd.sqlliteforest.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.R;

import java.util.List;

public class NoteAdapter extends ArrayAdapter<NoteTaxation> {

    public NoteAdapter(Context context, int resource, List<NoteTaxation> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.view_note_item, null);
        }

        NoteTaxation note = getItem(position);

        if(note != null) {
            TextView content = convertView.findViewById(R.id.list_note_contet);
            TextView date = convertView.findViewById(R.id.list_note_date);

            content.setText(note.Name);
            date.setText(note.DateTime);
        }

        return convertView;
    }
}
