package com.example.ololoproxdd.sqlliteforest.XmlModels.Mds;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Доп_макет21")
public class Maket21 {

    public void setDefault() {
        tipLand = "0";
        estetPoint = "0";
        sanitarPoint = "0";
        ustojchuvost = "0";
        prohodimost = "0";
        prosmatrivaemost = "0";
        stadiaDigresija = "0";
        MAF = "0";
    }

    //region поля
    @Element(required = false, name = "Тип_ландшафта")
    public String tipLand;

    @Element(required = false, name = "Эстетическая_оценка")
    public String estetPoint;

    @Element(required = false, name = "Санитарная_оценка")
    public String sanitarPoint;

    @Element(required = false, name = "Оценка_устойчивости")
    public String ustojchuvost;

    @Element(required = false, name = "Оценка_проходимости")
    public String prohodimost;

    @Element(required = false, name = "Оценка_просматриваемости")
    public String prosmatrivaemost;

    @Element(required = false, name = "Стадия_дегрессии")
    public String stadiaDigresija;

    @Element(required = false, name = "Малый_архитектурные_формы")
    public String MAF;
    //endregion
}
