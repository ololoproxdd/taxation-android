package com.example.ololoproxdd.sqlliteforest.Adapters;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.EditItem;
import com.example.ololoproxdd.sqlliteforest.R;

import java.io.IOException;
import java.util.List;

public class EditAdapter extends ArrayAdapter<EditItem>
{
    //region поля
    Context _context;
    String _table;
    //endregion

    //region методы
    public EditAdapter(Context context, int resource, List<EditItem> objects, String table) {
        super(context, resource, objects);
        _context = context;
        _table = table;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.edit_item, null);
        }

        EditItem item = getItem(position);

        if(item != null) {
            CheckBox cb = convertView.findViewById(R.id.checkbox_item);

            cb.setText(item.name);
            cb.setChecked(item.checked);

            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    DatabaseProcedures DatabaseProcedures = new DatabaseProcedures();
                    DatabaseProcedures.setActual(_table, isChecked ? 1 : 0, "name", "'" + buttonView.getText().toString() + "'");
                }
            });
        }

        return convertView;
    }
    //endregion
}
