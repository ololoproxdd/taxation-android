package com.example.ololoproxdd.sqlliteforest.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.XmlModels.YarusPoroda;

import java.util.List;

public class YarusPorodaAdapter extends ArrayAdapter<YarusPoroda> {
    public YarusPorodaAdapter(Context context, int resource, List<YarusPoroda> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.yarus_poroda_item, null);
        }

        YarusPoroda poroda = getItem(position);

        if(poroda != null) {
            TextView item_coef = convertView.findViewById(R.id.item_coef);
            TextView item_poroda = convertView.findViewById(R.id.item_poroda);
            TextView item_vozrast = convertView.findViewById(R.id.item_vozrast);
            TextView item_visota = convertView.findViewById(R.id.item_visota);
            TextView item_diametr = convertView.findViewById(R.id.item_diametr);
            TextView item_tovarnost = convertView.findViewById(R.id.item_tovarnost);

            item_coef.setText(poroda.coeff);
            item_poroda.setText(DatabaseProcedures.GetDataFromPK("порода_ярус", "code", poroda.poroda));
            item_vozrast.setText(poroda.vozrast);
            item_visota.setText(poroda.visota);
            item_diametr.setText(poroda.diameter);
            item_tovarnost.setText(poroda.tovarnost);

            LinearLayout layout = (LinearLayout) convertView;
            int color;
            switch (DatabaseProcedures.GetDataFromPK("происхождение_породы", "code", poroda.proishogden)){
                case "2":
                    color = Color.WHITE;
                    break;
                case "3":
                    color = Color.GREEN;
                    break;
                case "5":
                    color = Color.BLUE;
                    break;
                case "6":
                    color = Color.YELLOW;
                    break;
                 default:
                     color = Color.WHITE;
            }
            layout.setBackgroundColor(color);
        }

        return convertView;
    }
}
