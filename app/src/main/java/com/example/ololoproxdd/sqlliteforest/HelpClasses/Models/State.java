package com.example.ololoproxdd.sqlliteforest.HelpClasses.Models;

import android.app.Activity;
import android.widget.RadioButton;

import com.example.ololoproxdd.sqlliteforest.HelpClasses.ModelPoroda;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Kvartal;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Lesnichestvo;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.XmlModels.UchastkovoeLesvo;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Yarus;
import com.example.ololoproxdd.sqlliteforest.XmlModels.YarusPoroda;

import java.util.ArrayList;
import java.util.HashMap;

public class State {
    //region поля
    public static NoteTaxation selectedNote = null;
    public static Lesnichestvo selectedLesvo = null;
    public static UchastkovoeLesvo selectedUchLesvo = null;
    public static Kvartal selectedKvartal = null;
    public static Videl selectedVidel = null;
    public static Yarus selectedYarus = null;

    //region ярус
    public static int SelectedYarusNum = 0;
    public static ArrayList<ModelPoroda> selectedSostavPorod = null;
    public static ArrayList<ModelPoroda> sostavPorod1 = new ArrayList<>();
    public static ArrayList<ModelPoroda> sostavPorod2 = new ArrayList<>();
    public static ArrayList<ModelPoroda> sostavPorod3 = new ArrayList<>();

    public static YarusPoroda selectedPoroda = null;
    public static RadioButton selectedYarusRadioButton = null;
    public static ModelPoroda preobladPoroda = null;
    //endregion

    //region импорт экспорт
    public static boolean exportFlag = false;
    public static Activity ImportExportActivity = null;
    //endregion

    public static String activeRequiredForms = null;
    public static String activeForms = null;

    //endregion

    //region методы
    public static HashMap<Integer, Boolean> mdsEnabled = new HashMap<Integer, Boolean>()
    {{
        put(11,false);
        put(12,false);
        put(13,false);
        put(17,false);
        put(19,false);
        put(21,false);
        put(23,false);
    }};

    public static void setDefaultMdsMap(){
        mdsEnabled.clear();
        mdsEnabled.put(11,false);
        mdsEnabled.put(12,false);
        mdsEnabled.put(13,false);
        mdsEnabled.put(17,false);
        mdsEnabled.put(19,false);
        mdsEnabled.put(21,false);
        mdsEnabled.put(23,false);
    }

//    public static HashMap<Integer, Boolean> mdsShow = new HashMap<Integer, Boolean>()
//    {{
//        put(11,false);
//        put(12,false);
//        put(13,false);
//        put(17,false);
//        put(19,false);
//        put(21,false);
//        put(23,false);
//    }};
//
//    public static void setHideMdsMap(){
//        mdsShow.clear();
//        mdsShow.put(11,false);
//        mdsShow.put(12,false);
//        mdsShow.put(13,false);
//        mdsShow.put(17,false);
//        mdsShow.put(19,false);
//        mdsShow.put(21,false);
//        mdsShow.put(23,false);
//    }
    //endregion
}
