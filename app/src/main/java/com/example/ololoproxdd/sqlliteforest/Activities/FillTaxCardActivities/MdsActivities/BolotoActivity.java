package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket19;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;

public class BolotoActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_tip_bolota = null;
    Spinner spinner_tip_rast = null;
    Spinner spinner_poroda_zarast = null;
    Spinner spinner_stepen_zarast = null;
    EditText moshnost = null;

    ArrayList<String> tip_bolota = null;
    ArrayList<String> tip_rastit = null;
    ArrayList<String> poroda_zarast = null;
    ArrayList<String> stepen_zarast = null;

    Videl selectedVidel = State.selectedVidel;
    Maket19 boloto = selectedVidel.maket19;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boloto);
        getSupportActionBar().setTitle("Болото");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        setDefaultValues();
        if (validation){
            String errorMessage = "";
            //обязатаельные поля для ввода
            if (spinner_tip_bolota.getSelectedItem().toString().equals("0") ||
                    spinner_tip_bolota.getSelectedItem().toString().equals("0") ||
                    moshnost.getText().toString().isEmpty())
                errorMessage += "Заполните обязательное поле ввода!\n";

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Болото");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save(){
        setDefaultValues();
        //int
        boloto.moshTorfSloj = moshnost.getText().toString();
        boloto.stepenZarast = spinner_stepen_zarast.getSelectedItem().toString();

        //guid
        boloto.porodaZarast = DatabaseProcedures.GetPKCode("порода_ярус", spinner_poroda_zarast.getSelectedItem().toString());
        boloto.tipRast = DatabaseProcedures.GetPKCode("тип_растительности", spinner_tip_rast.getSelectedItem().toString());
        boloto.tipBolota = DatabaseProcedures.GetPKCode("тип_болота", spinner_tip_bolota.getSelectedItem().toString());


        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues(){
        if (moshnost.getText().toString().isEmpty())
            moshnost.setText("0");
    }

    public void init() {
        //region начальная инициализация
        spinner_tip_bolota = findViewById(R.id.spinner_tip_bolota);
        ArrayAdapter<String> adapter_spinner_tip_bolota = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tip_bolota);
        adapter_spinner_tip_bolota.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tip_bolota.setAdapter(adapter_spinner_tip_bolota);

        spinner_tip_rast = findViewById(R.id.spinner_tip_rast);
        ArrayAdapter<String> adapter_spinner_tip_rast = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tip_rastit);
        adapter_spinner_tip_rast.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tip_rast.setAdapter(adapter_spinner_tip_rast);

        spinner_poroda_zarast = findViewById(R.id.spinner_poroda_zarast);
        ArrayAdapter<String> adapter_spinner_poroda_zarast = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_zarast);
        adapter_spinner_poroda_zarast.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_poroda_zarast.setAdapter(adapter_spinner_poroda_zarast);

        spinner_stepen_zarast = findViewById(R.id.spinner_stepen_zarast);
        ArrayAdapter<String> adapter_spinner_stepen_zarast = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stepen_zarast);
        adapter_spinner_stepen_zarast.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_stepen_zarast.setAdapter(adapter_spinner_stepen_zarast);

        moshnost = findViewById(R.id.edit_moshnost);
        moshnost.setText("0");
        //endregion

        //region события
        moshnost.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        moshnost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        spinner_stepen_zarast.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_poroda_zarast.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_tip_bolota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_tip_rast.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null && boloto != null) {
            //болото. мощность торф слоя
            if (boloto.moshTorfSloj != null)
                moshnost.setText(boloto.moshTorfSloj);

            //болото. тип болота
            if (boloto.tipBolota!= null){
                String tip_bol = DatabaseProcedures.GetDataFromPK("тип_болота", "code", "name", boloto.tipBolota);
                spinner_tip_bolota.setSelection(adapter_spinner_tip_bolota.getPosition(tip_bol));
            }

            //болото. тип растительности
            if (boloto.tipRast!= null){
                String tip_rast = DatabaseProcedures.GetDataFromPK("тип_растительности", "code", "name", boloto.tipRast);
                spinner_tip_rast.setSelection(adapter_spinner_tip_rast.getPosition(tip_rast));
            }

            //болото. порода зарастания
            if (boloto.porodaZarast != null){
                String poroda_zar = DatabaseProcedures.GetDataFromPK("порода_ярус", "code", boloto.porodaZarast);
                spinner_poroda_zarast.setSelection(adapter_spinner_poroda_zarast.getPosition(poroda_zar));
            }

            //болото. степень зарастания
            if (boloto.stepenZarast != null)
                spinner_stepen_zarast.setSelection(adapter_spinner_stepen_zarast.getPosition(boloto.stepenZarast));

        }
        //endregion
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        tip_bolota = DatabaseProcedures.getData("тип_болота", "code", "name");
        tip_rastit = DatabaseProcedures.getData("тип_растительности", "code", "name");
        poroda_zarast = DatabaseProcedures.getDataActual("порода_ярус", "code");
        stepen_zarast = DatabaseProcedures.getData("процент_зарастания");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
