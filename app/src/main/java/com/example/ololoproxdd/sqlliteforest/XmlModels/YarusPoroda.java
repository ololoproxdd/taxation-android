package com.example.ololoproxdd.sqlliteforest.XmlModels;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "Порода")
public class YarusPoroda {

    //region методы
    public void setDefault() {
        poroda = "0";
        coeff = "1";
        vozrast = "0";
        visota = "0";
        diameter = "0";
        tovarnost = "0";
        proishogden = "914195df-b40d-4d8a-bfca-f8c71da00933";
    }
    //endregion

    //region поля
    @Element(required=false, name="Вид_породы")
    public String poroda;

    @Element(required=false, name="Коэффициент_породы")
    public String coeff;

    @Element(required=false, name="Возраст")
    public String vozrast;

    @Element(required=false, name="Высота")
    public String visota;

    @Element(required=false, name="Диаметр")
    public String diameter;

    @Element(required=false, name="Товарность")
    public String tovarnost;

    @Element(required=false, name="Происхождение")
    public String proishogden;

    //endregion
}
