package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

public class GpsActivity extends AppCompatActivity {

    EditText edit_gps = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("GPS");
        init();
        initHeader();
    }

    @Override
    protected void onPause(){
        save();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();

        super.onBackPressed();  // optional depending on your needs
    }

    public void init(){
        edit_gps = findViewById(R.id.edit_gps);

        edit_gps.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        edit_gps.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        edit_gps.setText(State.selectedVidel.GPS);
    }

    public void save(){
        //int
        State.selectedVidel.GPS = edit_gps.getText().toString();

        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();

                Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
                toast.show();
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
