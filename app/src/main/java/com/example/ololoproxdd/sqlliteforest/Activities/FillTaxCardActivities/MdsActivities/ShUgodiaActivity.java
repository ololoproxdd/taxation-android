package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket17;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;

public class ShUgodiaActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_polzovatel = null;
    Spinner spinner_kach_ugod = null;
    Spinner spinner_tip_senokos = null;
    Spinner spinner_sh_ugod_sostayanie = null;
    Spinner spinner_poroda_zarast  = null;
    Spinner spinner_procent_zarast = null;
    EditText urojai = null;

    ArrayList<String> polzovatel = null;
    ArrayList<String> kach_ugod = null;
    ArrayList<String> tip_senokos = null;
    ArrayList<String> sostoyanie = null;
    ArrayList<String> poroda_zarast = null;
    ArrayList<String> procent_zarast = null;

    Videl selectedVidel = State.selectedVidel;
    Maket17 maket17 = selectedVidel.maket17;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sh_ugodia);
        getSupportActionBar().setTitle("С/Х угодья");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        setDefaultValues();
        if (validation){
            String errorMessage = "";
            //обязатаельные поля для ввода
            if (spinner_polzovatel.getSelectedItem().toString().equals("0") ||
                    spinner_kach_ugod.getSelectedItem().toString().equals("0") ||
                    spinner_tip_senokos.getSelectedItem().toString().equals("0"))
                errorMessage += "Заполните обязательное поле ввода!\n";

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "С/Х угодья");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();

        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_SHORT);
        toast.show();

        return true;
    }

    public void save(){

        setDefaultValues();
        //int
        maket17.urogainost = urojai.getText().toString();
        maket17.procentZarast = spinner_procent_zarast.getSelectedItem().toString();

        //guid
        maket17.polzovatel = DatabaseProcedures.GetPKCode("тип_пользователя", spinner_polzovatel.getSelectedItem().toString());
        maket17.kachestvo = DatabaseProcedures.GetPKCode("качество_угодья", spinner_kach_ugod.getSelectedItem().toString());
        maket17.tipSenokosa = DatabaseProcedures.GetPKCode("тип_сенокоса", spinner_tip_senokos.getSelectedItem().toString());
        maket17.sostoyanie = DatabaseProcedures.GetPKCode("состояние_сх", spinner_sh_ugod_sostayanie.getSelectedItem().toString());
        maket17.porodaZarast = DatabaseProcedures.GetPKCode("порода_ярус", spinner_poroda_zarast.getSelectedItem().toString());

        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues(){
        if (urojai.getText().toString().isEmpty())
            urojai.setText("0");
    }

    public void init() {
        //region начальная инициализация
        spinner_polzovatel = findViewById(R.id.spinner_polzovatel);
        ArrayAdapter<String> adapter_spinner_polzovatel = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, polzovatel);
        adapter_spinner_polzovatel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_polzovatel.setAdapter(adapter_spinner_polzovatel);

        spinner_kach_ugod = findViewById(R.id.spinner_kach_ugod);
        ArrayAdapter<String> adapter_spinner_kach_ugod = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, kach_ugod);
        adapter_spinner_kach_ugod.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_kach_ugod.setAdapter(adapter_spinner_kach_ugod);

        spinner_tip_senokos = findViewById(R.id.spinner_tip_senokos);
        ArrayAdapter<String> adapter_spinner_tip_senokos = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tip_senokos);
        adapter_spinner_tip_senokos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tip_senokos.setAdapter(adapter_spinner_tip_senokos);

        spinner_sh_ugod_sostayanie = findViewById(R.id.spinner_sh_ugod_sostayanie);
        ArrayAdapter<String> adapter_spinner_sh_ugod_sostayanie = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sostoyanie);
        adapter_spinner_sh_ugod_sostayanie.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sh_ugod_sostayanie.setAdapter(adapter_spinner_sh_ugod_sostayanie);

        spinner_poroda_zarast = findViewById(R.id.spinner_poroda_zarast);
        ArrayAdapter<String> adapter_spinner_poroda_zarast = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_zarast);
        adapter_spinner_poroda_zarast.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_poroda_zarast.setAdapter(adapter_spinner_poroda_zarast);

        spinner_procent_zarast = findViewById(R.id.spinner_procent_zarast);
        ArrayAdapter<String> adapter_spinner_procent_zarast = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, procent_zarast);
        adapter_spinner_procent_zarast.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_procent_zarast.setAdapter(adapter_spinner_procent_zarast);

        urojai = findViewById(R.id.edit_urozhainost);
        urojai.setText("0");
        //endregion

        //region события
        spinner_polzovatel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_kach_ugod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_tip_senokos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_sh_ugod_sostayanie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_poroda_zarast.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_procent_zarast.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        urojai.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        urojai.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null && maket17 != null) {
            // урожайность
            if (maket17.urogainost != null)
                urojai.setText(maket17.urogainost);

            // процент зарастания
            if (maket17.procentZarast != null)
                spinner_procent_zarast.setSelection(adapter_spinner_procent_zarast.getPosition(maket17.procentZarast));

            //тип пользователя
            if (maket17.polzovatel!= null){
                String tip_pol = DatabaseProcedures.GetDataFromPK("тип_пользователя", "code", "name", maket17.polzovatel);
                spinner_polzovatel.setSelection(adapter_spinner_polzovatel.getPosition(tip_pol));
            }

            //качество_угодья
            if (maket17.kachestvo!= null){
                String kach = DatabaseProcedures.GetDataFromPK("качество_угодья", "code", "name", maket17.kachestvo);
                spinner_kach_ugod.setSelection(adapter_spinner_kach_ugod.getPosition(kach));
            }

            //тип_сенокоса
            if (maket17.tipSenokosa!= null){
                String seno = DatabaseProcedures.GetDataFromPK("тип_сенокоса", "code", "name", maket17.tipSenokosa);
                spinner_tip_senokos.setSelection(adapter_spinner_tip_senokos.getPosition(seno));
            }

            //состояние_сх
            if (maket17.sostoyanie!= null){
                String sost = DatabaseProcedures.GetDataFromPK("состояние_сх", "code", "name", maket17.sostoyanie);
                spinner_sh_ugod_sostayanie.setSelection(adapter_spinner_sh_ugod_sostayanie.getPosition(sost));
            }

            //порода зарастания
            if (maket17.porodaZarast!= null){
                String zarast = DatabaseProcedures.GetDataFromPK("порода_ярус", "code", maket17.porodaZarast);
                spinner_poroda_zarast.setSelection(adapter_spinner_poroda_zarast.getPosition(zarast));
            }
        }
        //endregion
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        polzovatel = DatabaseProcedures.getData("тип_пользователя", "code", "name");
        kach_ugod = DatabaseProcedures.getData("качество_угодья", "code", "name");
        tip_senokos = DatabaseProcedures.getData("тип_сенокоса", "code", "name");
        sostoyanie =  DatabaseProcedures.getData("состояние_сх", "code", "name");
        poroda_zarast = DatabaseProcedures.getDataActual("порода_ярус", "code");
        procent_zarast = DatabaseProcedures.getData("процент_зарастания");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
