package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;

import java.io.IOException;
import java.util.ArrayList;

public class CategoriesActivity extends AppCompatActivity {

    //region поля
    public Videl selectedVidel = null;
    public NoteTaxation selectedNote = null;

    EditText nomer_videla = null;
    EditText ploshad_videla = null;
    EditText krutizna = null;
    EditText visota = null;
    Spinner spinner_special_purpose = null;
    Spinner spinner_category_protect = null;
    Spinner spinner_land_category = null;
    Spinner spinner_OZU = null;
    Spinner spinner_exposure_slope = null;
    Spinner spinner_erosion_form = null;
    Spinner spinner_erosion_pow = null;

    ArrayList<String> special_purpose = null;
    ArrayList<String> category_protect = null;
    ArrayList<String> category_zemel = null;
    ArrayList<String> OZU = null;
    ArrayList<String> expozicia_sklona = null;
    //ArrayList<String> krutizna_sklona = null;
    ArrayList<String> erozia_vid = null;
    ArrayList<String> erozia_stepen = null;

    boolean close = false;
    //endregion
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Категории");
        initNote();
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation) {
        setDefaultValues();
        if (validation) {
            String errorMessage = "";
            //При выбранном целевом назначении «Эксплутационные» заполнено поле категория защитных
            //При выбранном целевом назначении «Защитные» не заполнено поле категория защитных
            String cel = spinner_special_purpose.getSelectedItem().toString();
            String cat = spinner_category_protect.getSelectedItem().toString();
            if (cel.equals("Защитные") && cat.equals("-") ||
                    cel.equals("Эксплуатационные") && !cat.equals("-"))
                errorMessage += "Проверь категорию защитности!\n";

            if (!errorMessage.isEmpty()) {
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Категории");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    void initNote() {
        selectedVidel = State.selectedVidel;
        selectedNote = State.selectedNote;
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues() {
        if (nomer_videla.getText().toString().isEmpty())
            nomer_videla.setText("0");

        if (ploshad_videla.getText().toString().isEmpty())
            ploshad_videla.setText("0");

        if (krutizna.getText().toString().isEmpty())
            krutizna.setText("0");

        if (visota.getText().toString().isEmpty())
            visota.setText("0");
    }

    void getActiveFormsMDS() {
        DatabaseProcedures DatabaseProcedures = new DatabaseProcedures();
        String cat_zemel_pk = State.selectedVidel.katZemel != null ? State.selectedVidel.katZemel : "1";
        String cat_zemel_code = DatabaseProcedures.GetDataFromPK("категория_земель_ТЗ", "code", cat_zemel_pk);
        State.activeForms = DatabaseProcedures.runScript(
                "select форма_ввода_МДС from категория_форма_ввода_мдс where категория_земель_code = " + cat_zemel_code).get(0);

        State.activeRequiredForms = DatabaseProcedures.runScript(
                "select обязат_заполн from категория_форма_ввода_мдс where категория_земель_code = " + cat_zemel_code).get(0);
    }

    void clearFocus() {
       /* View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.clearFocus();
        }
        findViewById(R.id.category_layout).requestFocus();*/
    }

    public void init() {
        //region начальная инициализация
        spinner_special_purpose = findViewById(R.id.spinner_special_purpose);
        ArrayAdapter<String> adapter_special_purpose = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, special_purpose);
        adapter_special_purpose.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_special_purpose.setAdapter(adapter_special_purpose);

        spinner_category_protect = findViewById(R.id.spinner_category_protect);
        ArrayAdapter<String> adapter_spinner_category_protect = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, category_protect);
        adapter_spinner_category_protect.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_category_protect.setAdapter(adapter_spinner_category_protect);

        spinner_land_category = findViewById(R.id.spinner_land_category);
        ArrayAdapter<String> adapter_spinner_land_category = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, category_zemel);
        adapter_spinner_land_category.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_land_category.setAdapter(adapter_spinner_land_category);

        spinner_OZU = findViewById(R.id.spinner_OZU);
        ArrayAdapter<String> adapter_spinner_OZU = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, OZU);
        adapter_spinner_OZU.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_OZU.setAdapter(adapter_spinner_OZU);

        spinner_exposure_slope = findViewById(R.id.spinner_exposure_slope);
        ArrayAdapter<String> adapter_spinner_exposure_slope = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, expozicia_sklona);
        adapter_spinner_exposure_slope.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_exposure_slope.setAdapter(adapter_spinner_exposure_slope);

        spinner_erosion_form = findViewById(R.id.spinner_erosion_form);
        ArrayAdapter<String> adapter_spinner_erosion_form = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, erozia_vid);
        adapter_spinner_erosion_form.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_erosion_form.setAdapter(adapter_spinner_erosion_form);

        spinner_erosion_pow = findViewById(R.id.spinner_erosion_pow);
        ArrayAdapter<String> adapter_spinner_erosion_pow = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, erozia_stepen);
        adapter_spinner_erosion_pow.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_erosion_pow.setAdapter(adapter_spinner_erosion_pow);

        krutizna = findViewById(R.id.edit_steepness_slope);
        krutizna.setText("0");

        visota = findViewById(R.id.edit_height_sea_level);
        visota.setText("0");

        ploshad_videla = findViewById(R.id.edit_ploshad_videla);
        ploshad_videla.setText("0");
        //endregion

        //region события
        nomer_videla = findViewById(R.id.edit_part_number);
        nomer_videla.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        nomer_videla.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        ploshad_videla.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        ploshad_videla.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        krutizna.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });

        visota.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });

        spinner_special_purpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearFocus();
                String item = (String) parentView.getItemAtPosition(position);
                if (item.equals("Защитные"))
                    spinner_category_protect.setEnabled(true);
                else {
                    spinner_category_protect.setSelection(0);
                    spinner_category_protect.setEnabled(false);
                }
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                clearFocus();
            }
        });

        spinner_category_protect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearFocus();
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                clearFocus();
            }
        });

        spinner_land_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearFocus();
                save();
                getActiveFormsMDS();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                clearFocus();
            }
        });

        spinner_OZU.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearFocus();
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                clearFocus();
            }
        });

        spinner_exposure_slope.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearFocus();
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                clearFocus();
            }
        });

        spinner_erosion_form.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearFocus();
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                clearFocus();
            }
        });

        spinner_erosion_pow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearFocus();
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                clearFocus();
            }
        });

        krutizna.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        visota.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });
        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null) {
            //номер выдела
            if (selectedVidel.nomerVidela != null)
                nomer_videla.setText(selectedVidel.nomerVidela);

            //площадь выдела
            if (selectedVidel.ploshadVidela!= null)
                ploshad_videla.setText(selectedVidel.ploshadVidela);

            DatabaseProcedures DatabaseProcedures = new DatabaseProcedures();

            //целевое назначение
            if (selectedVidel.celevoeNaznachenie != null) {
                String sel_cel_nazn = DatabaseProcedures.GetDataFromPK("целевое_назнач", "name", selectedVidel.celevoeNaznachenie);
                spinner_special_purpose.setSelection(adapter_special_purpose.getPosition(sel_cel_nazn));
            }

            //категория защитности
            if (selectedVidel.katZashit != null) {
                String sel_cat_prot = DatabaseProcedures.GetDataFromPK("категория_защитности_ТЗ", "name", selectedVidel.katZashit);
                spinner_category_protect.setSelection(adapter_spinner_category_protect.getPosition(sel_cat_prot));
            }

            //категория земель
            if (selectedVidel.katZemel != null) {
                String sel_cat_zemel = DatabaseProcedures.GetDataFromPK("категория_земель_ТЗ", "code", "name", selectedVidel.katZemel);
                spinner_land_category.setSelection(adapter_spinner_land_category.getPosition(sel_cat_zemel));
            }

            //ОЗУ
            if (selectedVidel.OZU != null) {
                String sel_ozu = DatabaseProcedures.GetDataFromPK("ОЗУ_ТЗ", "code", "name", selectedVidel.OZU);
                spinner_OZU.setSelection(adapter_spinner_OZU.getPosition(sel_ozu));
            }

            //Экспозиция склона
            if (selectedVidel.exposSklona != null) {
                String sel_exp_skl = DatabaseProcedures.GetDataFromPK("экспозиция_склона", "code", "name", selectedVidel.exposSklona);
                spinner_exposure_slope.setSelection(adapter_spinner_exposure_slope.getPosition(sel_exp_skl));
            }

            //Крутизна склона
            if (selectedVidel.krutSklona != null)
                krutizna.setText(selectedVidel.krutSklona);

            //Высота над уровнем моря
            if (selectedVidel.visotaNadMorem != null)
                visota.setText(selectedVidel.visotaNadMorem);

            //Эрозия вид
            if (selectedVidel.erosiaVid != null) {
                String sel_eros_vid = DatabaseProcedures.GetDataFromPK("вид_эрозии", "code", "name", selectedVidel.erosiaVid);
                spinner_erosion_form.setSelection(adapter_spinner_erosion_form.getPosition(sel_eros_vid));
            }

            //Эрозия вид
            if (selectedVidel.erosiaStepen != null) {
                String sel_eros_step = DatabaseProcedures.GetDataFromPK("степень_эрозии", "code", "name", selectedVidel.erosiaStepen);
                spinner_erosion_pow.setSelection(adapter_spinner_erosion_pow.getPosition(sel_eros_step));
            }
        }
        //endregion
    }

    public void save(){
        setDefaultValues();
        //int
        selectedVidel.nomerVidela = nomer_videla.getText().toString();
        selectedVidel.ploshadVidela = ploshad_videla.getText().toString();
        selectedVidel.krutSklona = krutizna.getText().toString();
        selectedVidel.visotaNadMorem = visota.getText().toString();

        //guid
        DatabaseProcedures DatabaseProcedures = new DatabaseProcedures();
        selectedVidel.celevoeNaznachenie = DatabaseProcedures.GetPK("целевое_назнач", "name", spinner_special_purpose.getSelectedItem().toString());
        selectedVidel.katZashit = DatabaseProcedures.GetPK("категория_защитности_ТЗ", "name", spinner_category_protect.getSelectedItem().toString());
        selectedVidel.katZemel = DatabaseProcedures.GetPKCode("категория_земель_ТЗ", spinner_land_category.getSelectedItem().toString());
        selectedVidel.OZU = DatabaseProcedures.GetPKCode("ОЗУ_ТЗ", spinner_OZU.getSelectedItem().toString());
        selectedVidel.exposSklona = DatabaseProcedures.GetPKCode("экспозиция_склона", spinner_exposure_slope.getSelectedItem().toString());
        selectedVidel.erosiaVid = DatabaseProcedures.GetPKCode("вид_эрозии", spinner_erosion_form.getSelectedItem().toString());
        selectedVidel.erosiaStepen = DatabaseProcedures.GetPKCode("степень_эрозии", spinner_erosion_pow.getSelectedItem().toString());

        if (selectedNote != null)
            XmlHelper.writeXmlFile(selectedNote);
    }

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region работа с бд
    public void getDataFromDB(){
        special_purpose = DatabaseProcedures.getDataOrderDesc("целевое_назнач", "name");
        category_protect = DatabaseProcedures.getData("категория_защитности_ТЗ");
        category_zemel = DatabaseProcedures.getDataActual("категория_земель_ТЗ", "code", "name");
        OZU = DatabaseProcedures.getDataActual("ОЗУ_ТЗ", "code", "name");
        expozicia_sklona = DatabaseProcedures.getData("экспозиция_склона", "code", "name");
        erozia_vid = DatabaseProcedures.getData("вид_эрозии", "code", "name");
        erozia_stepen = DatabaseProcedures.getData("степень_эрозии", "code", "name");
    }
    //endregion

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }
}
