package com.example.ololoproxdd.sqlliteforest.XmlModels;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.ArrayList;

public class Yarus {
    //region методы
    public Yarus(){
        yarusPorodaList = new ArrayList<>();
    }

    public void setDefault() {
        typeYarus = "cab413b1-36d4-447d-b360-1147c39d618c";
        polnota = "0";
        SZN = "0";
        if (yarusPorodaList != null) {
            for (int i = 0; i < yarusPorodaList.size(); i++)
                yarusPorodaList.get(i).setDefault();
        }
    }

    public Integer getSummaCoef(){
        Integer summCoef = 0;
        if (yarusPorodaList != null){
            for (int i = 0; i < yarusPorodaList.size(); i++)
                summCoef += Integer.parseInt(yarusPorodaList.get(i).coeff);
        }
        return  summCoef;
    }
    //endregion

    //region поля
    @Element(required=false, name="Тип_яруса")
    public String typeYarus;

    @Element(required=false, name="Полнота")
    public String polnota;

    @Element(required=false, name="Объём")
    public String SZN;

    @ElementList(inline=true, required = false, name = "Порода")
    public ArrayList<YarusPoroda> yarusPorodaList;
    //endregion
}
