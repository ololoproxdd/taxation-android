package com.example.ololoproxdd.sqlliteforest.XmlModels.Mds;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Доп_макет17")
public class Maket17 {
    public void setDefault() {
        polzovatel = "eed6fc81-f58c-47f9-8065-fce2f0bdc190";
        kachestvo = "a028519e-da16-41c1-899c-c2f79d28bad0";
        tipSenokosa = "e874e533-b734-4af8-a767-9201f7e4edd0";
        sostoyanie = "ec5fb64b-a2eb-44ed-b839-0acda073de10";
        porodaZarast = "0";
        procentZarast = "0";
        urogainost = "0";
    }

    //region поля
    @Element(required = false, name = "Пользователь")
    public String polzovatel;

    @Element(required = false, name = "Качество_угодья")
    public String kachestvo;

    @Element(required = false, name = "Тип_сенокоса")
    public String tipSenokosa;

    @Element(required = false, name = "Состояние")
    public String sostoyanie;

    @Element(required = false, name = "Порода_зарастания")
    public String porodaZarast;

    @Element(required = false, name = "Процент_зарастания")
    public String procentZarast;

    @Element(required = false, name = "Урожайность")
    public String urogainost;
    //endregion
}
