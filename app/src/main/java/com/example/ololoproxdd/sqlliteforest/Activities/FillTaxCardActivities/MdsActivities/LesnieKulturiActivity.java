package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket11;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.XmlModels.YarusPoroda;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class LesnieKulturiActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_spos_obrab_poch = null;
    Spinner spinner_spos_sozd_lk = null;
    Spinner spinner_sostoyanie = null;
    Spinner spinner_prichina = null;
    EditText god_sozd = null;
    EditText mej_ryad = null;
    EditText v_ryadu  = null;
    EditText kol_vo = null;

    ArrayList<String> sposob_obrab_pochv = null;
    ArrayList<String> sposob_sozd_lk = null;
    ArrayList<String> sost_lk = null;
    ArrayList<String> pricihina_neud = null;

    Videl selectedVidel = State.selectedVidel;
    Maket11 maket11 = selectedVidel.maket11;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesnie_kulturi);
        getSupportActionBar().setTitle("Лесные культуры");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        setDefaultValues();
        if (validation){
            String errorMessage = "";

            //обязатаельные поля для ввода
            if (god_sozd.getText().toString().isEmpty() ||
                    spinner_sostoyanie.getSelectedItem().toString().equals("-"))
                errorMessage += "Заполните обязательное поле ввода!\n";

            if (errorMessage.isEmpty()) {
                //Год создания лесных культур в 11 макете не соотносится с возрастом породы
                // искусственного происхождения в первом ярусе.
                ArrayList<YarusPoroda> yarusPorodas = State.selectedVidel.yarus1.yarusPorodaList;
                if (yarusPorodas != null && yarusPorodas.size() > 0) {
                    for (int i = 0; i < yarusPorodas.size(); i++) {
                        if (yarusPorodas.get(i).proishogden.equals("af33ed5e-5558-4a45-abf0-8a80e39f6fed")) {
                            Integer god_soz = Integer.parseInt(god_sozd.getText().toString().trim());
                            Integer vozr = Integer.parseInt(yarusPorodas.get(i).vozrast.trim());
                            Integer god_cur = Calendar.getInstance().get(Calendar.YEAR);
                            if (god_soz + vozr - 1 != god_cur) {
                                errorMessage += "Проверь возраст культур!\n";
                                break;
                            }
                        }
                    }
                }
                //для категории земель 8 (насаждения с породами искусственного происхождения)
                // в 11 макете состояние культур отличное, хорошее или удовлетворительное
                String sost = spinner_sostoyanie.getSelectedItem().toString();
                if (State.selectedVidel.katZemel.equals("4") &&
                        (sost.equals("удовлетворительные") || sost.equals("хорошие")))
                    errorMessage += "Проверь 11 макет!\n";
            }

            if (!errorMessage.isEmpty()) {
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Лесные культуры");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save(){

        setDefaultValues();
        //int
        maket11.godSozd = god_sozd.getText().toString();
        maket11.mejduRyad = mej_ryad.getText().toString();
        maket11.vRyad = v_ryadu.getText().toString();
        maket11.kolvo = kol_vo.getText().toString();

        //guid

        maket11.sposobObrabotki = DatabaseProcedures.GetPKCode("способ_обр_почвы", spinner_spos_obrab_poch.getSelectedItem().toString());
        maket11.sposobSozd = DatabaseProcedures.GetPKCode("способ_созд_лк", spinner_spos_sozd_lk.getSelectedItem().toString());
        maket11.sostoyanie= DatabaseProcedures.GetPK("состояние_лес_культ", "name", spinner_sostoyanie.getSelectedItem().toString());
        maket11.prichinaNeudSost = DatabaseProcedures.GetPKCode("причина_неуд_сост", spinner_prichina.getSelectedItem().toString());


        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues(){
        if (god_sozd.getText().toString().isEmpty())
            god_sozd.setText(Calendar.getInstance().get(Calendar.YEAR));

        if (mej_ryad.getText().toString().isEmpty())
            mej_ryad.setText("0");

        if (v_ryadu.getText().toString().isEmpty())
            v_ryadu.setText("0");

        if (kol_vo.getText().toString().isEmpty())
            kol_vo.setText("0");
    }

    public void init() {
        //region начальная инициализация
        spinner_spos_obrab_poch = findViewById(R.id.spinner_spos_obrab_poch);
        ArrayAdapter<String> adapter_spinner_spos_obrab_poch = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sposob_obrab_pochv);
        adapter_spinner_spos_obrab_poch.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_spos_obrab_poch.setAdapter(adapter_spinner_spos_obrab_poch);

        spinner_spos_sozd_lk = findViewById(R.id.spinner_spos_sozd_lk);
        ArrayAdapter<String> adapter_spinner_spos_sozd_lk = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sposob_sozd_lk);
        adapter_spinner_spos_sozd_lk.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_spos_sozd_lk.setAdapter(adapter_spinner_spos_sozd_lk);

        spinner_sostoyanie = findViewById(R.id.spinner_sostoyanie);
        ArrayAdapter<String> adapter_spinner_sostoyanie = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sost_lk);
        adapter_spinner_sostoyanie.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sostoyanie.setAdapter(adapter_spinner_sostoyanie);

        spinner_prichina = findViewById(R.id.spinner_prichina);
        ArrayAdapter<String> adapter_spinner_prichina = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pricihina_neud);
        adapter_spinner_prichina.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_prichina.setAdapter(adapter_spinner_prichina);

        god_sozd = findViewById(R.id.edit_god_sozdania);
        // TODO исправить по ТЗ
        god_sozd.setText("2018");

        mej_ryad = findViewById(R.id.edit_rast_mej_ryad);
        mej_ryad.setText("0");

        v_ryadu = findViewById(R.id.edit_rast_v_ryadu);
        v_ryadu.setText("0");

        kol_vo = findViewById(R.id.edit_kolvo);
        kol_vo.setText("0");
        //endregion

        //region события
        spinner_spos_obrab_poch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_spos_sozd_lk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_sostoyanie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        if (spinner_sostoyanie.getSelectedItem().toString().equals("погибшие") ||
                                spinner_sostoyanie.getSelectedItem().toString().equals("неудовлетворительные"))
                            spinner_prichina.setEnabled(true);
                        else {
                            spinner_prichina.setSelection(0);
                            spinner_prichina.setEnabled(false);
                        }
                        save();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // your code here
                    }
                });
        spinner_prichina.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        god_sozd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        god_sozd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        mej_ryad.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        mej_ryad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        v_ryadu.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        v_ryadu.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        kol_vo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        kol_vo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        //endregion

        // region загружаем информацию из xml
        if (selectedVidel != null && maket11 != null) {

            //год создания
            if (maket11.godSozd != null)
                god_sozd.setText(maket11.godSozd);

            //способ обработки
            if (maket11.sposobObrabotki != null){
                String sp_obr = DatabaseProcedures.GetDataFromPK("способ_обр_почвы", "code", "name", maket11.sposobObrabotki);
                spinner_spos_obrab_poch.setSelection(adapter_spinner_spos_obrab_poch.getPosition(sp_obr));
            }

            //способ создания
            if (maket11.sposobSozd != null){
                String sp_sozd = DatabaseProcedures.GetDataFromPK("способ_созд_лк", "code", "name", maket11.sposobSozd);
                spinner_spos_sozd_lk.setSelection(adapter_spinner_spos_sozd_lk.getPosition(sp_sozd));
            }

            //междурядье
            if (maket11.mejduRyad != null)
                mej_ryad.setText(maket11.mejduRyad);

            //в ряд
            if (maket11.vRyad != null)
                v_ryadu.setText(maket11.vRyad);

            //кол-во
            if (maket11.kolvo != null)
                kol_vo.setText(maket11.kolvo);

            //состояние
            if (maket11.sostoyanie != null){
                String sost = DatabaseProcedures.GetDataFromPK("состояние_лес_культ", "name", maket11.sostoyanie);
                spinner_sostoyanie.setSelection(adapter_spinner_sostoyanie.getPosition(sost));
            }

            //причина неуд сост
            if (maket11.prichinaNeudSost != null){
                String prichina = DatabaseProcedures.GetDataFromPK("причина_неуд_сост", "code","name", maket11.prichinaNeudSost);
                spinner_prichina.setSelection(adapter_spinner_prichina.getPosition(prichina));
            }

        }
        //endregion
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        sposob_obrab_pochv = DatabaseProcedures.getData("способ_обр_почвы", "code", "name");
        sposob_sozd_lk = DatabaseProcedures.getData("способ_созд_лк", "code", "name");
        sost_lk = DatabaseProcedures.getData("состояние_лес_культ");
        pricihina_neud = DatabaseProcedures.getData("причина_неуд_сост", "code", "name");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
