package com.example.ololoproxdd.sqlliteforest.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

public class MainActivity extends AppCompatActivity {

    void initApp(){
        DatabaseProcedures._context = this;
        XmlHelper.initXmlHelper();
        XmlHelper.readXmlFiles();
    }

    //region UI
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Электронная карточка таксации");
        initApp();
    }

    // Заполнение карточки таксации
    public void fillTaxCard(View view) {
        Intent intent = new Intent(this, AddZapisActivity.class);
        startActivity(intent);
    }

    // Редактирование справочников
    public void editDir(View view) {
        Intent intent = new Intent(this, EditDirActivity.class);
        startActivity(intent);
    }

    // импорт / экспорт
    public void importExport(View view) {
        Intent intent = new Intent(this, ImportExportActivity.class);
        startActivity(intent);
    }

    public void settings(View view) {
    }
    //endregion

    //region permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
        }
    }
    //endregion
}
