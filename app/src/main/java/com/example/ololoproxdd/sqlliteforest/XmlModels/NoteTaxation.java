package com.example.ololoproxdd.sqlliteforest.XmlModels;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

@Root(name="Запись")
public class NoteTaxation implements Serializable {

    //region методы
    public void setDefault(){
        Id = "";
        Name = "";
        ispolnitel = "";
        DateTime = "";
        if (lesnichestvos != null) {
            for (int i = 0; i < lesnichestvos.size(); i++)
            lesnichestvos.get(i).setDefault();
        }
    }

    public ArrayList<String> lesnichestvosNames() {
        ArrayList<String> lesnichestvosNames = new ArrayList<>();
        if (lesnichestvos != null) {
            for (int i = 0; i < lesnichestvos.size(); i++) {
                String name = lesnichestvos.get(i).lesnichestvoName;
                if (name != null)
                    lesnichestvosNames.add(name);
            }
        }
        return lesnichestvosNames;
    }

    public void setId(){
        Id = java.util.UUID.randomUUID().toString();
    }

    public void setDate(){
        DateTime = df.format(Calendar.getInstance().getTime());
    }
    //endregion

    //region поля
    DateFormat df = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");

    @Element(required = false, name = "id")
    public String Id;

    @Element(required = false, name = "Название_записи")
    public String Name;

    @Element(required = false, name = "Исполнитель")
    public String ispolnitel;

    @Element(required = false, name = "Дата_и_время_создания")
    public String DateTime;

    @ElementList(inline=true, required = false, name = "Лесничество")
    public ArrayList<Lesnichestvo> lesnichestvos;
    //endregion
}
