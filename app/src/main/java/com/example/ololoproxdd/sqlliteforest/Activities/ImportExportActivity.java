package com.example.ololoproxdd.sqlliteforest.Activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.util.regex.Pattern;

public class ImportExportActivity extends AppCompatActivity {

    Activity _this = this;
    NoteTaxation note = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        State.ImportExportActivity = this;
        setContentView(R.layout.activity_import_export);
        getSupportActionBar().setTitle("Импорт / Экспорт");
    }

    //region Импорт
    public void importData(View view) {
        DialogInterface.OnClickListener confirm = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new MaterialFilePicker()
                        .withActivity(_this)
                        .withRequestCode(1)
                        .withFilter(Pattern.compile(".*\\.xml$")) // Filtering files and directories by file name using regexp
                        .withFilterDirectories(false) // Set directories filterable (false by default)
                        .withHiddenFiles(true) // Show hidden files and folders
                        .start();
            }
        };

        MyDialog dialog = new MyDialog();
        Bundle args = new Bundle();
        args.putString("title", "Импорт");
        args.putString("message", "Выберите файл для импорта");
        args.putInt("icon", android.R.drawable.ic_menu_search);
        args.putString("positiveMessage", "Ок");
        dialog.setArguments(args);
        dialog.positive = confirm;
        dialog.show(getSupportFragmentManager(), "custom");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            // Загрузка файла из xml
            note = XmlHelper.readXmlFile(filePath);
            if (note != null){
                note = XmlHelper.writeXmlFile(note);
                // Если загрузка успешна, то перейти к activity Lesnichestvo
                DialogInterface.OnClickListener confirm = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        State.selectedNote = note;
                        Intent intent = new Intent(_this, LesnichestvoActivity.class);
                        startActivity(intent);
                    }
                };

                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Импорт");
                args.putString("message", "Импорт выполнен успешно\nПерейти к добавленной записи?");
                args.putInt("icon", android.R.drawable.ic_dialog_info);
                args.putString("positiveMessage", "Да");
                args.putString("negativeMessage", "Нет");
                dialog.setArguments(args);
                dialog.positive = confirm;
                dialog.show(getSupportFragmentManager(), "custom");
            }
            else {
                // иначе вывести ошибку о том, что файл не корректен
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Импорт");
                args.putString("message", "Ошибка импорта");
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Ок");
                dialog.setArguments(args);
                dialog.show(getSupportFragmentManager(), "custom");
            }
        }
    }
    //endregion

    //region Экспорт
    public void exportData(View view) {
        DialogInterface.OnClickListener confirm = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                State.exportFlag = true;
                _this.finish();
                Intent intent = new Intent(_this, AddZapisActivity.class);
                startActivity(intent);
            }
        };

        MyDialog dialog = new MyDialog();
        Bundle args = new Bundle();
        args.putString("title", "Экспорт");
        args.putString("message", "Выберите запись для экспорта");
        args.putInt("icon", android.R.drawable.ic_menu_search);
        args.putString("positiveMessage", "Ок");
        dialog.setArguments(args);
        dialog.positive = confirm;
        dialog.show(getSupportFragmentManager(), "custom");
    }
    //endregion
}
