package com.example.ololoproxdd.sqlliteforest.XmlModels.Mds;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Доп_макет19")
public class Maket19 {

    public void setDefault() {
        tipBolota = "4d871d7e-c2f4-4076-8e05-2e26fae6cdaa";
        tipRast = "13b59d87-1add-4859-a5f7-63a5c274c733";
        moshTorfSloj = "0";
        porodaZarast = "0";
        stepenZarast = "0";
    }

    //region поля
    @Element(required = false, name = "Тип_болота")
    public String tipBolota;

    @Element(required = false, name = "Тип_растительности")
    public String tipRast;

    @Element(required = false, name = "Мощность_торф_слоя")
    public String moshTorfSloj;

    @Element(required = false, name = "Порода_зарастания")
    public String porodaZarast;

    @Element(required = false, name = "Процент_зарастания")
    public String stepenZarast;
    //endregion
}
