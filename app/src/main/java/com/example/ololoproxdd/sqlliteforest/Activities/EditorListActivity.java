package com.example.ololoproxdd.sqlliteforest.Activities;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.ololoproxdd.sqlliteforest.Adapters.EditAdapter;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.EditItem;
import com.example.ololoproxdd.sqlliteforest.R;

import java.io.IOException;
import java.util.ArrayList;

public class EditorListActivity extends AppCompatActivity {

    //region поля
    ArrayList<EditItem> edit_items = null;
    ListView editList = null;
    String edtior_type = null;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_list);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Bundle arguments = getIntent().getExtras();
        edtior_type = arguments.get("editor_type").toString();
        getSupportActionBar().setTitle(arguments.get("title").toString());
        String table = getDataFromDB();
        editList = findViewById(R.id.edit_list);
        EditAdapter adapter = new EditAdapter(this, R.layout.edit_item, edit_items, table);
        editList.setAdapter(adapter);
    }

    //region БД
    public String getDataFromDB(){
        String table = "";
        if (edtior_type.equals("kat_zemel"))
            table = "категория_земель_ТЗ";
        else if (edtior_type.equals("poroda_yarus"))
            table = "порода_ярус";
        else if (edtior_type.equals("tip_lesa"))
            table = "тип_леса";
        else if (edtior_type.equals("ozu"))
            table = "ОЗУ_ТЗ";
        else if (edtior_type.equals("osoben"))
            table = "особенности_ТЗ";
        else if (edtior_type.equals("events"))
            table = "мероприятия_ТЗ";

        edit_items = DatabaseProcedures.getDataEditor(table, "name", "actual");
        return table;
    }
    //endregion
}
