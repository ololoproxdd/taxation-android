package com.example.ololoproxdd.sqlliteforest.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class MyDialog extends DialogFragment {

    public DialogInterface.OnClickListener positive;
    public DialogInterface.OnClickListener negative;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        int icon = getArguments().getInt("icon");
        String positiveMessage = getArguments().getString("positiveMessage");
        String negativeMessage = getArguments().getString("negativeMessage");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if (positiveMessage != null && !positiveMessage.isEmpty())
            builder.setPositiveButton(positiveMessage, positive);

        if (negativeMessage != null && !negativeMessage.isEmpty())
            builder.setNegativeButton(negativeMessage, negative);

        return builder
                .setTitle(title)
                .setIcon(icon)
                .setMessage(message)
                //.setPositiveButton("Ок", positive)
                //.setNegativeButton("Нет", negative)
                .create();
    }
}