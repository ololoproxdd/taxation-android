package com.example.ololoproxdd.sqlliteforest.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Kvartal;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Lesnichestvo;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.UchastkovoeLesvo;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.util.ArrayList;

public class LesnichestvoActivity extends AppCompatActivity {

    //region поля
    EditText nazv_zapisi = null;
    EditText ispolnitel = null;
    EditText ploshad = null;
    Button addVidelBtn = null;
    AutoCompleteTextView ac_text_view_forestery = null;
    AutoCompleteTextView ac_text_view_uchLesvo = null;
    AutoCompleteTextView ac_text_view_quarter = null;
    ListView listVidel = null;

    public NoteTaxation selectedNote = State.selectedNote;
    public Lesnichestvo selectedLesnichestvo = new Lesnichestvo();
    public UchastkovoeLesvo selectedUchLesvo = new UchastkovoeLesvo();
    public Kvartal selectedKvartal = new Kvartal();
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesnichestvo);
    }

    @Override
    protected void onStart(){
        super.onStart();
        init();
        getSupportActionBar().setTitle("Лесничество");
    }

    @Override
    public void onBackPressed()
    {
        if (save(false))
            super.onBackPressed();  // optional depending on your needs
    }

    public boolean save(boolean validation){
        if (validation){
            String errorMessage = "";
            //обязатаельные поля для ввода
            if (nazv_zapisi.getText().toString().isEmpty() ||
                    ispolnitel.getText().toString().isEmpty() ||
                    ac_text_view_forestery.getText().toString().isEmpty() ||
                    ac_text_view_uchLesvo.getText().toString().isEmpty() ||
                    ac_text_view_quarter.getText().toString().isEmpty() ||
                    ploshad.getText().toString().isEmpty())
                errorMessage += "Заполните обязательное поле ввода!\n";

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Детали записи");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Ок");
                //args.putString("negativeMessage", "Нет");
                dialog.setArguments(args);
                //dialog.positive = null;
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        return true;
    }

    public void save() {
        selectedNote.Name = nazv_zapisi.getText().toString();
        selectedNote.ispolnitel = ispolnitel.getText().toString();
        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    //region изменеие адаптеров при изменении родительского элемента
    AutoCompleteTextView changeLesvoUchLesvoAdapter() {
        final AutoCompleteTextView ac_text_view_uchLesvo = findViewById(R.id.ac_text_view_uchLesvo);
        ArrayAdapter<String> ac_text_view_uchLesvo_adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, selectedLesnichestvo.uchastkovoeLesvosNames());
        ac_text_view_uchLesvo.setAdapter(ac_text_view_uchLesvo_adapter);
        return ac_text_view_uchLesvo;
    }

    AutoCompleteTextView changeKvartalAdapter() {
        final AutoCompleteTextView ac_text_view_quarter = findViewById(R.id.ac_text_view_quarter);
        ArrayAdapter<String> ac_text_view_quarter_adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, selectedUchLesvo.kvartalsNames());
        ac_text_view_quarter.setAdapter(ac_text_view_quarter_adapter);


        return ac_text_view_quarter;
    }

    ListView changeVidelAdapter() {
        if (selectedKvartal != null && selectedKvartal.kvartalPloshad != null) {
            final EditText ploshad = findViewById(R.id.edit_ploshad);
            ploshad.setText(selectedKvartal.kvartalPloshad);
        }

        ListView listVidel = findViewById(R.id.listVidel);
        ArrayAdapter<String> listVidel_adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, selectedKvartal != null ? selectedKvartal.videlsNames() : new ArrayList<String>());
        listVidel.setAdapter(listVidel_adapter);
        return listVidel;
    }
    //endregion

    //region изменение выбранных элементов
     void tryChangeSelectedLesvo(String selectedName) {
         if (selectedName != null) {
             boolean bHasLesvo = false;
             if (selectedNote.lesnichestvos != null) {
                 for (int i = 0; i < selectedNote.lesnichestvos.size(); i++) {
                     String name = selectedNote.lesnichestvos.get(i).lesnichestvoName;
                     if (name != null && name.toLowerCase().equals(selectedName.toLowerCase())) {
                         selectedLesnichestvo = selectedNote.lesnichestvos.get(i);
                         bHasLesvo = true;
                         break;
                     }
                 }
             }
             if (!bHasLesvo) {
                 selectedLesnichestvo = new Lesnichestvo();
                 selectedLesnichestvo.lesnichestvoName = selectedName;
             }
             changeLesvoUchLesvoAdapter();
             tryChangeSelectedUchLesvo(selectedUchLesvo.uchLesvoName);
         }
     }

    void tryChangeSelectedUchLesvo(String selectedName){
        if (selectedName!= null) {
            boolean bHasUchLesvo = false;
            for (int i = 0; i < selectedLesnichestvo.uchastkovoeLesvos.size(); i++) {
                String name = selectedLesnichestvo.uchastkovoeLesvos.get(i).uchLesvoName;
                if (name != null && name.toLowerCase().equals(selectedName.toLowerCase())) {
                    selectedUchLesvo = selectedLesnichestvo.uchastkovoeLesvos.get(i);
                    bHasUchLesvo = true;
                    break;
                }
            }
            if (!bHasUchLesvo)
            {
                selectedUchLesvo = new UchastkovoeLesvo();
                selectedUchLesvo.uchLesvoName = selectedName;
            }
            changeKvartalAdapter();
            tryChangeSelectedKvartal(selectedKvartal.kvartalNomer);
        }
    }

    void tryChangeSelectedKvartal(String selectedNomer){
        if (selectedNomer!= null) {
            for (int i = 0; i < selectedUchLesvo.kvartals.size(); i++) {
                String nomer = selectedUchLesvo.kvartals.get(i).kvartalNomer;
                if (nomer != null && nomer.toLowerCase().equals(selectedNomer.toLowerCase())) {
                    selectedKvartal = selectedUchLesvo.kvartals.get(i);
                    changeVidelAdapter();
                    return;
                }
            }
            selectedKvartal = new Kvartal();
            selectedKvartal.kvartalNomer = selectedNomer;
            changeVidelAdapter();
        }
    }
    //endregion

    void init(){
        if (selectedNote != null) {
            //region начальная инициализация
            addVidelBtn = findViewById(R.id.addVidel);
            nazv_zapisi = findViewById(R.id.edit_nazv_zapisi);
            ispolnitel = findViewById(R.id.edit_ispolnitel);
            ploshad = findViewById(R.id.edit_ploshad);

            //Лесничество
            ac_text_view_forestery = findViewById(R.id.ac_text_view_forestery);
            ArrayAdapter<String> ac_text_view_forestery_adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, selectedNote.lesnichestvosNames());
            ac_text_view_forestery.setAdapter(ac_text_view_forestery_adapter);

            //Участковое лесничество
            ac_text_view_uchLesvo = changeLesvoUchLesvoAdapter();

            //Квартал
            ac_text_view_quarter = changeKvartalAdapter();

            //Выдел
            listVidel = changeVidelAdapter();
            //endregion

            //region события
            addVidelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addVidel(v);
                }
            });
            nazv_zapisi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus)
                        save();
                }
            });
            ispolnitel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus)
                        save();
                }
            });
            //лес-во
            ac_text_view_forestery.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tryChangeSelectedLesvo(ac_text_view_forestery.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            ac_text_view_forestery.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        ac_text_view_forestery.showDropDown();
                    } else {
                        //tryChangeSelectedLesvo(ac_text_view_forestery.getText().toString());
                    }
                }
            });
            ac_text_view_forestery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tryChangeSelectedLesvo(selectedNote.lesnichestvos.get(position).lesnichestvoName);
                }
            });
            //уч лес-во
            ac_text_view_uchLesvo.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tryChangeSelectedUchLesvo(ac_text_view_uchLesvo.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            ac_text_view_uchLesvo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        ac_text_view_uchLesvo.showDropDown();
                    } else {
                        //tryChangeSelectedUchLesvo(ac_text_view_uchLesvo.getText().toString());
                    }
                }
            });
            ac_text_view_uchLesvo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tryChangeSelectedUchLesvo(selectedLesnichestvo.uchastkovoeLesvos.get(position).uchLesvoName);
                }
            });
            //квартал
            ac_text_view_quarter.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tryChangeSelectedKvartal(ac_text_view_quarter.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                        s.delete(0,1);
                    }
                }
            });
            ac_text_view_quarter.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        ac_text_view_quarter.showDropDown();
                    } else {
                        //tryChangeSelectedKvartal(ac_text_view_quarter.getText().toString());
                    }
                }
            });
            ac_text_view_quarter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tryChangeSelectedKvartal(selectedUchLesvo.kvartals.get(position).kvartalNomer);
                }
            });
            //площадь
            ploshad.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                        s.delete(0,1);
                    }
                }
            });

            //выдел

            listVidel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), FillTaxCardActivity.class);
                    State.selectedVidel = selectedKvartal.videls.get(position);
                    State.selectedKvartal = selectedKvartal;
                    State.selectedUchLesvo = selectedUchLesvo;
                    State.selectedLesvo = selectedLesnichestvo;
                    State.selectedNote = selectedNote;
                    startActivity(intent);
                }
            });
            listVidel.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    DialogInterface.OnClickListener confirmDelete = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Videl deletedVidel = selectedKvartal.videls.get(position);
                            deletedVidel = null;
                            selectedKvartal.videls.remove(position);

                            if (selectedKvartal.videls.size() < 1) {
                                selectedUchLesvo.kvartals.remove(selectedKvartal);
                                selectedKvartal = null;
                            }

                            if (selectedUchLesvo.kvartals.size() < 1) {
                                selectedLesnichestvo.uchastkovoeLesvos.remove(selectedUchLesvo);
                                selectedUchLesvo = null;
                            }

                            if (selectedLesnichestvo.uchastkovoeLesvos.size() < 1) {
                                selectedNote.lesnichestvos.remove(selectedLesnichestvo);
                                selectedLesnichestvo = null;
                            }

                            changeVidelAdapter();
                            XmlHelper.writeXmlFile(selectedNote);
                        }
                    };

                    MyDialog dialog = new MyDialog();
                    Bundle args = new Bundle();
                    args.putString("title", "Подтверждение удаления выдела");
                    args.putString("message", "Удалить выбранный выдел?");
                    args.putInt("icon", android.R.drawable.ic_menu_delete);
                    args.putString("positiveMessage", "Ок");
                    dialog.setArguments(args);
                    dialog.positive = confirmDelete;
                    dialog.show(getSupportFragmentManager(), "custom");

                    return true;
                }
            });
            //endregion

            //region загрузка информации из xml
            ispolnitel.setText(selectedNote.ispolnitel);

            nazv_zapisi.setText(selectedNote.Name);

            /*if (ac_text_view_forestery.getText().toString().isEmpty() ||
                    ac_text_view_uchLesvo.getText().toString().isEmpty() ||
                    ac_text_view_quarter.getText().toString().isEmpty()) {
                if (State.selectedLesvo != null && State.selectedUchLesvo != null && State.selectedKvartal != null) {
                    ac_text_view_forestery.setText(State.selectedLesvo.lesnichestvoName);
                    ac_text_view_uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);
                    ac_text_view_quarter.setText(State.selectedKvartal.kvartalNomer);
                    ploshad.setText(State.selectedKvartal.kvartalPloshad);
                } else {*/
            if (ac_text_view_forestery.getText().toString().isEmpty() ||
                    ac_text_view_uchLesvo.getText().toString().isEmpty() ||
                    ac_text_view_quarter.getText().toString().isEmpty()) {
                if (selectedNote.lesnichestvos != null && selectedNote.lesnichestvos.size() > 0) {
                    String lesvoName = selectedNote.lesnichestvos.get(selectedNote.lesnichestvos.size() - 1).lesnichestvoName;
                    ac_text_view_forestery.setText(lesvoName);
                    tryChangeSelectedLesvo(lesvoName);

                    if (selectedLesnichestvo != null && selectedLesnichestvo.uchastkovoeLesvos.size() > 0) {
                        String uchLesvoName = selectedLesnichestvo.uchastkovoeLesvos.get(selectedLesnichestvo.uchastkovoeLesvos.size() - 1).uchLesvoName;
                        ac_text_view_uchLesvo.setText(uchLesvoName);
                        tryChangeSelectedUchLesvo(uchLesvoName);

                        if (selectedUchLesvo != null && selectedUchLesvo.kvartals.size() > 0) {
                            String kvartalName = selectedUchLesvo.kvartals.get(selectedUchLesvo.kvartals.size() - 1).kvartalNomer;
                            ac_text_view_quarter.setText(kvartalName);
                            tryChangeSelectedKvartal(kvartalName);

                            final EditText ploshad = findViewById(R.id.edit_ploshad);
                            ploshad.setText(selectedKvartal.kvartalPloshad);
                        }
                    }
                }
            }
            //endregion
        }
    }

    //добавление выдела
    public void addVidel(View view) {
        if (save(false)) {
            Button addVidel = (Button) findViewById(R.id.addVidel);
            addVidel.setFocusableInTouchMode(true);
            addVidel.requestFocus();
            //Собирем из выбранных элементов структуру
            if (selectedNote != null) {
                if (selectedNote.lesnichestvos == null)
                    selectedNote.lesnichestvos = new ArrayList<>();

                if (selectedLesnichestvo != null) {
                    if (!selectedNote.lesnichestvos.contains(selectedLesnichestvo))
                        selectedNote.lesnichestvos.add(selectedLesnichestvo);

                    if (selectedLesnichestvo.uchastkovoeLesvos == null)
                        selectedLesnichestvo.uchastkovoeLesvos = new ArrayList<>();

                    if (selectedUchLesvo != null) {
                        if (!selectedLesnichestvo.uchastkovoeLesvos.contains(selectedUchLesvo))
                            selectedLesnichestvo.uchastkovoeLesvos.add(selectedUchLesvo);

                        if (selectedUchLesvo.kvartals == null)
                            selectedUchLesvo.kvartals = new ArrayList<>();

                        if (selectedKvartal != null) {
                            if (!selectedUchLesvo.kvartals.contains(selectedKvartal))
                                selectedUchLesvo.kvartals.add(selectedKvartal);

                            if (selectedKvartal.videls == null)
                                selectedKvartal.videls = new ArrayList<>();

                            selectedKvartal.kvartalPloshad = ploshad.getText().toString();

                            Videl videl = new Videl();
                            videl.setDefault();
                            if (selectedKvartal.videls.size() > 0) {
                                Integer nomerVidela = Integer.parseInt(selectedKvartal.videls.get(selectedKvartal.videls.size() - 1).nomerVidela) + 1;
                                videl.nomerVidela = nomerVidela.toString();
                            } else
                                videl.nomerVidela = "1";
                            selectedKvartal.videls.add(videl);
                            State.selectedVidel = videl;
                            State.selectedKvartal = selectedKvartal;
                            State.selectedUchLesvo = selectedUchLesvo;
                            State.selectedLesvo = selectedLesnichestvo;
                            State.selectedNote = selectedNote;
                            XmlHelper.writeXmlFile(selectedNote);
                            Intent intent = new Intent(this, FillTaxCardActivity.class);
                            startActivity(intent);
                        }
                    }
                }
            }
        }
    }
}
