package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;

public class PodrostActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_gystotaPodrost = null;
    Spinner spinner_age = null;
    Spinner spinner_height = null;
    Spinner spinner_type_tree1 = null;
    Spinner spinner_coef1 = null;
    Spinner spinner_type_tree2 = null;
    Spinner spinner_coef2 = null;
    Spinner spinner_type_tree3 = null;
    Spinner spinner_coef3 = null;
    Spinner spinner_gustota_podlesok = null;
    Spinner spinner_podlesok_poroda1 = null;
    Spinner spinner_podlesok_poroda2 = null;
    Spinner spinner_podlesok_poroda3 = null;

    ArrayList<String> kolvoPodrost = null;
    ArrayList<String> age = null;
    ArrayList<String> height = null;
    ArrayList<String> poroda_podrsot = null;
    ArrayList<String> coef = null;
    ArrayList<String> gustota_podleska = null;
    ArrayList<String> poroda_podlesok = null;

    Videl selectedVidel = State.selectedVidel;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podrost);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Подрост / Подлесок");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation){
        if (validation){
            String errorMessage = "";
            //сумма коэф должна быть = 10
            Double kolvo_podrost = Double.parseDouble(spinner_gystotaPodrost.getSelectedItem().toString());
            Integer coef1 = Integer.parseInt(spinner_coef1.getSelectedItem().toString().trim());
            Integer coef2 = Integer.parseInt(spinner_coef2.getSelectedItem().toString().trim());
            Integer coef3 = Integer.parseInt(spinner_coef3.getSelectedItem().toString().trim());
            if (kolvo_podrost != 0 && coef1 + coef2 + coef3 != 10)
                errorMessage +="Сумма коэффициентов подроста должна равной 10!\n";

            //Возраст подроста больше 40 лет
            Integer age = Integer.parseInt(spinner_age.getSelectedItem().toString());
            if (age > 40)
                errorMessage += "Проверь возраст подроста!\n";

            //Высота подроста больше 1/3 от высоты яруса или больше 10 м
            Double height = Double.parseDouble(spinner_height.getSelectedItem().toString());
            if (height > 10)
                errorMessage += "Проверь высоту подроста!\n";

            //Не могут быть одинаковые породы в подросте и подлеске
            String podrost1 = spinner_type_tree1.getSelectedItem().toString();
            String podrost2 = spinner_type_tree2.getSelectedItem().toString();
            String podrost3 = spinner_type_tree3.getSelectedItem().toString();
            if (podrost1.equals(podrost2) && !podrost1.equals("-") && !podrost2.equals("-") ||
                    podrost2.equals(podrost3) && !podrost2.equals("-") && !podrost3.equals("-") ||
                    podrost1.equals(podrost3) && !podrost1.equals("-") && !podrost3.equals("-"))
                errorMessage += "Проверь породы в подросте!\n";

            String podlesok1 = spinner_podlesok_poroda1.getSelectedItem().toString();
            String podlesok2 = spinner_podlesok_poroda2.getSelectedItem().toString();
            String podlesok3 = spinner_podlesok_poroda3.getSelectedItem().toString();
            if (podlesok1.equals(podlesok2) && !podlesok1.equals("-") && !podlesok2.equals("-") ||
                    podlesok2.equals(podlesok3) && !podlesok2.equals("-") && !podlesok3.equals("-") ||
                    podlesok1.equals(podlesok3) && !podlesok1.equals("-") && !podlesok3.equals("-"))
                errorMessage += "Проверь породы в подлеске!\n";

            if (!errorMessage.isEmpty()) {
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Подрост");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save(){
        //int
        selectedVidel.podrostGustota = spinner_gystotaPodrost.getSelectedItem().toString();
        selectedVidel.podrostVozrast = spinner_age.getSelectedItem().toString();
        selectedVidel.podrostVisota = spinner_height.getSelectedItem().toString();
        selectedVidel.podrostCoef1 = spinner_coef1.getSelectedItem().toString();
        selectedVidel.podrostCoef2 = spinner_coef2.getSelectedItem().toString();
        selectedVidel.podrostCoef3 = spinner_coef3.getSelectedItem().toString();

        //guid
        selectedVidel.podrostPoroda1 = DatabaseProcedures.GetPK("порода_подрост", "code", spinner_type_tree1.getSelectedItem().toString());
        selectedVidel.podrostPoroda2 = DatabaseProcedures.GetPK("порода_подрост", "code", spinner_type_tree2.getSelectedItem().toString());
        selectedVidel.podrostPoroda3 = DatabaseProcedures.GetPK("порода_подрост", "code", spinner_type_tree3.getSelectedItem().toString());

        selectedVidel.podlesokGustota = DatabaseProcedures.GetPK("густота_подлеска", "name", spinner_gustota_podlesok.getSelectedItem().toString());
        selectedVidel.podlesokPoroda1 = DatabaseProcedures.GetPK("порода_подлесок", "code", spinner_podlesok_poroda1.getSelectedItem().toString());
        selectedVidel.podlesokPoroda2 = DatabaseProcedures.GetPK("порода_подлесок", "code", spinner_podlesok_poroda2.getSelectedItem().toString());
        selectedVidel.podlesokPoroda3 = DatabaseProcedures.GetPK("порода_подлесок", "code", spinner_podlesok_poroda3.getSelectedItem().toString());

        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    public void init(){
        //region начальная инициализация
        spinner_gystotaPodrost = findViewById(R.id.gystotaPodrost);
        final ArrayAdapter<String> adapter_spinner_gystotaPodrost = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, kolvoPodrost);
        adapter_spinner_gystotaPodrost.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_gystotaPodrost.setAdapter(adapter_spinner_gystotaPodrost);

        spinner_age = findViewById(R.id.spinner_age);
        ArrayAdapter<String> adapter_spinner_age = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, age);
        adapter_spinner_age.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_age.setAdapter(adapter_spinner_age);

        spinner_height = findViewById(R.id.spinner_height);
        ArrayAdapter<String> adapter_spinner_height = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, height);
        adapter_spinner_height.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_height.setAdapter(adapter_spinner_height);

        spinner_type_tree1 = findViewById(R.id.spinner_type_tree1);
        ArrayAdapter<String> adapter_spinner_type_tree1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_podrsot);
        adapter_spinner_type_tree1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type_tree1.setAdapter(adapter_spinner_type_tree1);

        spinner_coef1 = findViewById(R.id.spinner_coef1);
        ArrayAdapter<String> adapter_spinner_coef1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, coef);
        adapter_spinner_coef1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_coef1.setAdapter(adapter_spinner_coef1);

        spinner_type_tree2 = findViewById(R.id.spinner_type_tree2);
        ArrayAdapter<String> adapter_spinner_type_tree2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_podrsot);
        adapter_spinner_type_tree2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type_tree2.setAdapter(adapter_spinner_type_tree2);

        spinner_coef2 = findViewById(R.id.spinner_coef2);
        ArrayAdapter<String> adapter_spinner_coef2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, coef);
        adapter_spinner_coef2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_coef2.setAdapter(adapter_spinner_coef2);

        spinner_type_tree3 = findViewById(R.id.spinner_type_tree3);
        ArrayAdapter<String> adapter_spinner_type_tree3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_podrsot);
        adapter_spinner_type_tree3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type_tree3.setAdapter(adapter_spinner_type_tree3);

        spinner_coef3 = findViewById(R.id.spinner_coef3);
        ArrayAdapter<String> adapter_spinner_coef3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, coef);
        adapter_spinner_coef3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_coef3.setAdapter(adapter_spinner_coef1);

        spinner_gustota_podlesok = findViewById(R.id.spinnerGystotaPodlesok);
        ArrayAdapter<String> adapter_spinner_gustota_podlesok= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gustota_podleska);
        adapter_spinner_gustota_podlesok.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_gustota_podlesok.setAdapter(adapter_spinner_gustota_podlesok);

        spinner_podlesok_poroda1 = findViewById(R.id.spinnerPoroda1);
        ArrayAdapter<String> adapter_spinner_podlesok_poroda1= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_podlesok);
        adapter_spinner_podlesok_poroda1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_podlesok_poroda1.setAdapter(adapter_spinner_podlesok_poroda1);

        spinner_podlesok_poroda2 = findViewById(R.id.spinnerPoroda2);
        ArrayAdapter<String> adapter_spinner_podlesok_poroda2= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_podlesok);
        adapter_spinner_podlesok_poroda2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_podlesok_poroda2.setAdapter(adapter_spinner_podlesok_poroda2);

        spinner_podlesok_poroda3 = findViewById(R.id.spinnerPoroda3);
        ArrayAdapter<String> adapter_spinner_podlesok_poroda3= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_podlesok);
        adapter_spinner_podlesok_poroda3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_podlesok_poroda3.setAdapter(adapter_spinner_podlesok_poroda3);
        //endregion

        //region события
        spinner_gystotaPodrost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (adapter_spinner_gystotaPodrost.getItem(position).toString().equals("0")){
                    spinner_coef1.setSelection(0);
                    spinner_coef2.setSelection(0);
                    spinner_coef3.setSelection(0);
                    spinner_coef1.setEnabled(false);
                    spinner_coef2.setEnabled(false);
                    spinner_coef3.setEnabled(false);
                }
                else{
                    spinner_coef1.setEnabled(true);
                    spinner_coef2.setEnabled(true);
                    spinner_coef3.setEnabled(true);
                }

                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_height.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_type_tree1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_coef1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_type_tree2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_coef2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_type_tree3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_coef3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_gustota_podlesok.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_podlesok_poroda1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_podlesok_poroda2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_podlesok_poroda3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        //endregion

        //region загружаем информацию из xml
        if (selectedVidel != null) {
            //подрост кол-во (густота)
            if (selectedVidel.podrostGustota != null)
                spinner_gystotaPodrost.setSelection(adapter_spinner_gystotaPodrost.getPosition(selectedVidel.podrostGustota));

            //подрост возраст
            if (selectedVidel.podrostVozrast != null)
                spinner_age.setSelection(adapter_spinner_age.getPosition(selectedVidel.podrostVozrast));

            //подрост высота
            if (selectedVidel.podrostVisota != null)
                spinner_height.setSelection(adapter_spinner_height.getPosition(selectedVidel.podrostVisota));

            //подрост коэф1
            if (selectedVidel.podrostCoef1 != null)
                spinner_coef1.setSelection(adapter_spinner_coef1.getPosition(selectedVidel.podrostCoef1));

            //подрост коэф2
            if (selectedVidel.podrostCoef2 != null)
                spinner_coef2.setSelection(adapter_spinner_coef2.getPosition(selectedVidel.podrostCoef2));

            //подрост коэф3
            if (selectedVidel.podrostCoef3 != null)
                spinner_coef3.setSelection(adapter_spinner_coef3.getPosition(selectedVidel.podrostCoef3));

            //подрост порода1
            if (selectedVidel.podrostPoroda1 != null){
                String por1 = DatabaseProcedures.GetDataFromPK("порода_подрост", "code", selectedVidel.podrostPoroda1);
                spinner_type_tree1.setSelection(adapter_spinner_type_tree1.getPosition(por1));
            }

            //подрост порода2
            if (selectedVidel.podrostPoroda2 != null){
                String por2 = DatabaseProcedures.GetDataFromPK("порода_подрост", "code", selectedVidel.podrostPoroda2);
                spinner_type_tree2.setSelection(adapter_spinner_type_tree2.getPosition(por2));
            }

            //подрост порода3
            if (selectedVidel.podrostPoroda3 != null){
                String por3 = DatabaseProcedures.GetDataFromPK("порода_подрост", "code", selectedVidel.podrostPoroda3);
                spinner_type_tree3.setSelection(adapter_spinner_type_tree3.getPosition(por3));
            }

            //подлесок густота
            if (selectedVidel.podlesokGustota != null){
                String podl_gust = DatabaseProcedures.GetDataFromPK("густота_подлеска", "name", selectedVidel.podlesokGustota);
                spinner_gustota_podlesok.setSelection(adapter_spinner_gustota_podlesok.getPosition(podl_gust));
            }

            //подлесок порода1
            if (selectedVidel.podlesokPoroda1 != null){
                String podl_poroda1 = DatabaseProcedures.GetDataFromPK("порода_подлесок", "code", selectedVidel.podlesokPoroda1);
                spinner_podlesok_poroda1.setSelection(adapter_spinner_podlesok_poroda1.getPosition(podl_poroda1));
            }

            //подлесок порода2
            if (selectedVidel.podlesokPoroda2 != null){
                String podl_poroda2 = DatabaseProcedures.GetDataFromPK("порода_подлесок", "code", selectedVidel.podlesokPoroda2);
                spinner_podlesok_poroda2.setSelection(adapter_spinner_podlesok_poroda2.getPosition(podl_poroda2));
            }

            //подлесок порода3
            if (selectedVidel.podlesokPoroda3 != null){
                String podl_poroda3 = DatabaseProcedures.GetDataFromPK("порода_подлесок", "code", selectedVidel.podlesokPoroda3);
                spinner_podlesok_poroda3.setSelection(adapter_spinner_podlesok_poroda3.getPosition(podl_poroda3));
            }
        }
        //endregion
    }

    //region БД
    public void getDataFromDB(){
        kolvoPodrost = DatabaseProcedures.getData("колво_подроста");
        age = DatabaseProcedures.getData("возраст");
        height = DatabaseProcedures.getData("высота");
        poroda_podrsot = DatabaseProcedures.getData("порода_подрост", "code");
        coef = DatabaseProcedures.getData("коэф_подроста");
        gustota_podleska = DatabaseProcedures.getData("густота_подлеска");
        poroda_podlesok = DatabaseProcedures.getData("порода_подлесок", "code");
    }
    //endregion

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}

