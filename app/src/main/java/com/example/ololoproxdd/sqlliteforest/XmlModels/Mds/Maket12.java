package com.example.ololoproxdd.sqlliteforest.XmlModels.Mds;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Доп_макет12")
public class Maket12 {
    //region методы
    public void setDefault() {
        tipPovrejd = "df867025-a5f3-44d9-8d8b-46d730eed74c";
        godPovrejd = "2005";
        povrejdPoroda = "0";
        bolezn1 = "c7cd3fc0-9346-402b-a74c-80b1f630022b";
        bolezn2 = "c7cd3fc0-9346-402b-a74c-80b1f630022b";
        stepen1 = "128076f4-edd9-4e54-8256-cbeee88ac2dc";
        stepen2 = "128076f4-edd9-4e54-8256-cbeee88ac2dc";
        istochnik = "0";
    }
    //endregion

    //region поля
    @Element(required = false, name = "Тип_повреждения")
    public String tipPovrejd;

    @Element(required = false, name = "Год_повреждения")
    public String godPovrejd;

    @Element(required = false, name = "Повр_порода")
    public String povrejdPoroda;

    @Element(required = false, name = "Вид_повреждения1")
    public String bolezn1;

    @Element(required = false, name = "Вид_повреждения2")
    public String bolezn2;

    @Element(required = false, name = "Степень1_код")
    public String stepen1;

    @Element(required = false, name = "Степень2_код")
    public String stepen2;

    @Element(required = false, name = "Источник_болезни")
    public String istochnik;
    //endregion
}
