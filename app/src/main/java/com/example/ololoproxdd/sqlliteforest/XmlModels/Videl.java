package com.example.ololoproxdd.sqlliteforest.XmlModels;


import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket11;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket12;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket13;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket17;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket19;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket21;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket23;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;

@Root(name = "Выдел")
public class Videl  implements Serializable {

    //region методы
    public Videl(){
        yarus1 = new Yarus();
        yarus2 = new Yarus();
        yarus3 = new Yarus();
        maket11 = new Maket11();
        maket12 = new Maket12();
        maket13 = new Maket13();
        maket17 = new Maket17();
        maket19 = new Maket19();
        maket21 = new Maket21();
        maket23 = new Maket23();
    }

    public void setDefault() {
        //region Категория
        nomerVidela = "0";
        ploshadVidela = "0";
        celevoeNaznachenie = "587cdc18-8274-49f1-af02-183e6b0091a3";
        katZashit = "13e62ba4-f30f-4edf-99d6-2942a421edfd";
        katZemel = "1";
        OZU = "1";
        exposSklona = "0";
        krutSklona = "0";
        visotaNadMorem = "0";
        erosiaVid = "98b21b41-1276-413d-abba-21246ceaf9cc";
        erosiaStepen = "4974faa8-85f5-469a-87dc-1ef08e80605e";
        //endregion

        //region Мероприятие
        celPoroda = "0";
        meropr1Vid = "0";
        meropr1Proc = "0";
        meropr1RTK = "0";
        meropr2Vid = "0";
        meropr2RTK = "0";
        meropr3Vid = "0";
        meropr3RTK = "0";
        //endregion

        //region Вырубка
        tipVirubki = "5e253727-42b2-4efa-99e1-11a06b50e210";
        godVirubki = "2005";
        pneyVsego = "0";
        pneySosny = "0";
        diameter = "0";
        //endregion

        //region Ярус
        tipLesa = "0";
        TLU = "95787f19-5523-4529-a646-5a2fbc7e6617";
        preoblPoroda = "0";
        bonitet = "-";
        groupVozrasta = "1";
        zahlam = "0";
        syhostoj = "0";
        likvidnost = "0";
        yarus1.setDefault();
        yarus2.setDefault();
        yarus3.setDefault();
        //endregion

        //region Подрост / Подлесок
        podrostGustota = "0";
        podrostVozrast = "0";
        podrostVisota = "0";
        podrostPoroda1 = "0";
        podrostPoroda2 = "0";
        podrostPoroda3 = "0";
        podrostCoef1 = "1";
        podrostCoef2 = "1";
        podrostCoef3 = "1";

        podlesokGustota = "b8b5876c-684b-4edf-b45c-613c853eac44";
        podlesokPoroda1 = "0";
        podlesokPoroda2 = "0";
        podlesokPoroda3 = "0";
        //endregion

        GPS = "";

        note = "";

        selectedMDS = "";

        //region МДС
        maket11.setDefault();
        maket12.setDefault();
        maket13.setDefault();
        maket17.setDefault();
        maket19.setDefault();
        maket21.setDefault();
        maket23.setDefault();
        //endregion
    }

    public String getNameVidela(){
        String result = "Выдел № ";
        if (nomerVidela != null && !nomerVidela.isEmpty())
            result += nomerVidela;
        if (GPS != null && !GPS.isEmpty())
            result += " (GPS " + GPS + " )";
        return result;
    }
    //endregion

    //region поля
    // region Категории
    @Element(required=false, name = "Номер_выдела")
    public String nomerVidela;

    @Element(required=false, name = "Площадь_выдела")
    public String ploshadVidela;

    @Element(required = false, name = "Целевое_назначение")
    public String celevoeNaznachenie;

    @Element(required = false, name = "Категория_защитности")
    public String katZashit;

    @Element(required = false, name = "Категория_земель")
    public String katZemel;

    @Element(required = false, name = "ОЗУ")
    public String OZU;

    @Element(required = false, name = "Экспозиция_склона")
    public String exposSklona;

    @Element(required = false, name = "Крутизна_склона")
    public String krutSklona;

    @Element(required = false, name = "Высота_над_уровнем_моря")
    public String visotaNadMorem;

    @Element(required = false, name = "Эрозия_склона")
    public String erosiaVid;

    @Element(required = false, name = "Степень_эрозии")
    public String erosiaStepen;
    //endregion

    //region Мероприятие
    @Element(required=false, name = "Целевая_порода")
    public String celPoroda;

    @Element(required=false, name = "Мероприятие_1_Вид")
    public String meropr1Vid;

    @Element(required=false, name = "Мероприятие_1_Процент")
    public String meropr1Proc;

    @Element(required=false, name = "Мероприятие_1_РТК")
    public String meropr1RTK;

    @Element(required=false, name = "Мероприятие_2_Вид")
    public String meropr2Vid;

    @Element(required=false, name = "Мероприятие_2_РТК")
    public String meropr2RTK;

    @Element(required=false, name = "Мероприятие_3_Вид")
    public String meropr3Vid;

    @Element(required=false, name = "Мероприятие_3_РТК")
    public String meropr3RTK;
    //endregion

    //region Вырубка
    @Element(required = false, name = "Тип_вырубки")
    public String tipVirubki;

    @Element(required = false, name = "Год_вырубки")
    public String godVirubki;

    @Element(required = false, name = "Пней_всего")
    public String pneyVsego;

    @Element(required = false, name = "Пней_сосны")
    public String pneySosny;

    @Element(required = false, name = "Диаметр")
    public String diameter;

    //endregion

    //region Ярус
    @Element(required=false, name = "Тип_леса")
    public String tipLesa;

    @Element(required=false, name = "ТЛУ")
    public String TLU;

    @Element(required=false, name = "Преобладающая_порода")
    public  String preoblPoroda;

    @Element(required=false, name = "Класс_бонитета")
    public String bonitet;

    @Element(required=false, name = "Группа_возраста")
    public  String groupVozrasta;

    @Element(required=false, name = "Захламленность")
    public String zahlam;

    @Element(required=false, name = "Cухостой")
    public String syhostoj;

    @Element(required=false, name = "Ликвидность")
    public String likvidnost;

    @Element(required=false, name = "Ярус1")
    public Yarus yarus1;

    @Element(required=false, name = "Ярус2")
    public Yarus yarus2;

    @Element(required=false, name = "Ярус3")
    public Yarus yarus3;

    //endregion

    //region Подрост / Подлесок
    @Element(required=false, name="Подрост_Густота")
    public String podrostGustota;

    @Element(required=false, name="Подрост_Возраст")
    public String podrostVozrast;

    @Element(required=false, name="Подрост_Высота")
    public String podrostVisota;

    @Element(required=false, name="Подрост_Порода_1")
    public String podrostPoroda1;

    @Element(required=false, name="Подрост_Порода_2")
    public String podrostPoroda2;

    @Element(required=false, name="Подрост_Порода_3")
    public String podrostPoroda3;

    @Element(required=false, name="Подрост_Коеф_1")
    public String podrostCoef1;

    @Element(required=false, name="Подрост_Коеф_2")
    public String podrostCoef2;

    @Element(required=false, name="Подрост_Коеф_3")
    public String podrostCoef3;

    @Element(required=false, name="Подлесок_Густота")
    public String podlesokGustota;

    @Element(required=false, name="Подлесок_Порода_1")
    public String podlesokPoroda1;

    @Element(required=false, name="Подлесок_Порода_2")
    public String podlesokPoroda2;

    @Element(required=false, name="Подлесок_Порода_3")
    public String podlesokPoroda3;
    //endregion

    //GPS
    @Element(required=false, name = "Точка_навигатора")
    public String GPS;

    //Примечания
    @Element(required=false, name = "Примечание")
    public String note;

    //region МДС
    @Element(required=false, name = "Доп_макет11")
    public Maket11 maket11;

    @Element(required=false, name = "Доп_макет12")
    public Maket12 maket12;

    @Element(required=false, name = "Доп_макет13")
    public Maket13 maket13;

    @Element(required=false, name = "Доп_макет17")
    public Maket17 maket17;

    @Element(required=false, name = "Доп_макет19")
    public Maket19 maket19;

    @Element(required=false, name = "Доп_макет21")
    public Maket21 maket21;

    @Element(required=false, name = "Доп_макет23")
    public Maket23 maket23;

    @Element(required=false, name = "Выбранные_МДС")
    public String selectedMDS;// = new ArrayList<>();
    //endregion
    //endregion
}
