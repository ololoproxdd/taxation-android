package com.example.ololoproxdd.sqlliteforest.XmlModels;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;

@Root(name="Квартал")
public class Kvartal  implements Serializable {
    //region методы
    public Kvartal(){
        videls = new ArrayList<>();
    }

    public void setDefault() {
        kvartalNomer = "";
        kvartalPloshad = "";

        if (videls != null) {
            for (int i = 0; i < videls.size(); i++)
                videls.get(i).setDefault();
        }
    }
    public ArrayList<String> videlsNames() {
        ArrayList<String> videlsNames = new ArrayList<>();
        if (videls != null) {
            for (int i = 0; i < videls.size(); i++) {
                String name = videls.get(i).getNameVidela();
                if (name != null)
                    videlsNames.add(name);
            }
        }
        return videlsNames;
    }
    //endregion

    //region поля
    @Element(required = false, name = "Квартал_номер")
    public String kvartalNomer;

    @Element(required = false, name = "Квартал_площадь")
    public String kvartalPloshad;

    @ElementList(inline=true, required = false, name = "Выдел")
    public ArrayList<Videl> videls;
    //endregion
}
