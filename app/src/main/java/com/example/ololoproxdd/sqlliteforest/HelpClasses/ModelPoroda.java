package com.example.ololoproxdd.sqlliteforest.HelpClasses;

public class ModelPoroda {
    public ModelPoroda(String code, Integer coef, Integer priority){
        Code = code;
        Coef = coef;
        Priority = priority;
    }

    public ModelPoroda(String code, Integer coef, Integer priority, Integer visota, Integer vozrast, Integer proish, Integer yarusNum){
        Code = code;
        Coef = coef;
        Priority = priority;
        Visota = visota;
        Vozrast = vozrast;
        Proish = proish;
        YarusNum = yarusNum;
    }

    public String Code;
    public Integer Coef;
    public Integer Priority;
    public Integer Visota;
    public Integer Vozrast;
    public Integer Proish;
    public Integer YarusNum;
}
