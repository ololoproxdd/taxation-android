package com.example.ololoproxdd.sqlliteforest.Activities;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.XmlModels.YarusPoroda;
import com.example.ololoproxdd.sqlliteforest.R;

import java.io.IOException;
import java.util.ArrayList;

public class ObserveActivity extends AppCompatActivity {

    //region поля
    Videl selectedVidel = State.selectedVidel;

    //Категория
    TextView lesvo = null;
    TextView uch_lesvo = null;
    TextView kvartal = null;
    TextView videl = null;
    TextView kz = null;

    //Категория
    TextView nomer_videla = null;
    TextView ploshad_videla = null;
    TextView kat_zemel = null;
    TextView ozu = null;
    TextView num = null;
    TextView expozicia = null;
    TextView krutizna = null;
    TextView erosia_vid = null;
    TextView erosia_stepen = null;

    //Мероприятия
    TextView meropr1 = null;
    TextView meropr1_proc = null;
    TextView meropr1_rtk = null;
    TextView meropr2 = null;
    TextView meropr2_rtk = null;
    TextView meropr3 = null;
    TextView meropr3_rtk = null;
    TextView cel_por = null;

    //Вырубка
    TextView god_vir = null;
    TextView pne_vse = null;
    TextView pne_sos = null;
    TextView diam = null;
    TextView tip_vir = null;

    //Ярус
    TextView pre_por = null;
    TextView kl_bon = null;
    TextView tip_les = null;
    TextView tlu_obs = null;

    TextView zahl = null;
    TextView likvid = null;
    TextView suhost = null;

    //Подрост подлесок
    TextView podr_kolvo = null;
    TextView podr_visota = null;
    TextView podr_vozr = null;
    TextView coef1 = null;
    TextView por1 = null;
    TextView coef2 = null;
    TextView por2 = null;
    TextView coef3 = null;
    TextView por3 = null;
    TextView podl_gust = null;
    TextView podl_por1 = null;
    TextView podl_por2 = null;
    TextView podl_por3 = null;

    //GPS
    TextView gps = null;

    //Примечания
    TextView primech = null;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observe);
        getSupportActionBar().setTitle("Карточка таксации");
        init();
    }

    void setBackgroundResourceRecursive(View view, int res) {
        if (view instanceof LinearLayout) {
            LinearLayout layout = (LinearLayout) view;
            if (layout != null) {
                for (int i = 0; i < layout.getChildCount(); i++) {
                    setBackgroundResourceRecursive(layout.getChildAt(i), res);
                    layout.getChildAt(i).setBackgroundResource(res);
                }
            }
        }
    }

    void setTextColorRecursive(View view, int res) {
        if (view instanceof LinearLayout) {
            LinearLayout layout = (LinearLayout) view;
            if (layout != null) {
                for (int i = 0; i < layout.getChildCount(); i++) {
                    setTextColorRecursive(layout.getChildAt(i), res);
                    if (layout.getChildAt(i) instanceof TextView)
                        ((TextView) layout.getChildAt(i)).setTextColor(res);
                }
            }
        }
    }

    void setDisabledForms() {
        int disable = Color.LTGRAY;
        LinearLayout category = findViewById(R.id.l_category);
        //category.setBackgroundColor(disable);
        setTextColorRecursive(category, disable);
        setBackgroundResourceRecursive(category, R.drawable.draw_cell);

        LinearLayout event = findViewById(R.id.l_event);
        //event.setBackgroundColor(disable);
        setTextColorRecursive(event, disable);
        setBackgroundResourceRecursive(event, R.drawable.draw_cell);

        LinearLayout virubka = findViewById(R.id.l_virubka);
        //virubka.setBackgroundColor(disable);
        setTextColorRecursive(virubka, disable);
        setBackgroundResourceRecursive(virubka, R.drawable.draw_cell);

        LinearLayout yarus = findViewById(R.id.l_yarus);
        //yarus.setBackgroundColor(disable);
        setTextColorRecursive(yarus, disable);
        setBackgroundResourceRecursive(yarus, R.drawable.draw_cell);

        LinearLayout podrost = findViewById(R.id.l_pordrost);
        //podrost.setBackgroundColor(disable);
        setTextColorRecursive(podrost, disable);
        setBackgroundResourceRecursive(podrost, R.drawable.draw_cell);

        LinearLayout mds = findViewById(R.id.l_mds);
        //mds.setBackgroundColor(disable);
        setTextColorRecursive(mds, disable);
        setBackgroundResourceRecursive(mds, R.drawable.draw_cell);

        LinearLayout gps = findViewById(R.id.l_gps);
        //gps.setBackgroundColor(disable);
        setTextColorRecursive(gps, disable);
        setBackgroundResourceRecursive(gps, R.drawable.draw_cell);

        LinearLayout note = findViewById(R.id.l_note);
        //note.setBackgroundColor(disable);
        setTextColorRecursive(note, disable);
        setBackgroundResourceRecursive(note, R.drawable.draw_cell);
    }

    //region инициализация яруса
    void initYarus(ArrayList<YarusPoroda> yar, int color, String yarusNum, String polnota) {
        for (int i = 0; i < yar.size(); i++) {
            YarusPoroda poroda = yar.get(i);

            LinearLayout yarus = findViewById(R.id.yarus);
            TextView el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(yarusNum);
            el.setTextColor(color);
            yarus.addView(el, yarus.getChildCount());

            LinearLayout coef = findViewById(R.id.sost_coef);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(poroda.coeff);
            el.setTextColor(color);
            coef.addView(el, coef.getChildCount());

            LinearLayout por = findViewById(R.id.sost_por);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(DatabaseProcedures.GetDataFromPK("порода_ярус", "code", poroda.poroda));
            el.setTextColor(color);
            por.addView(el, por.getChildCount());

            LinearLayout vozr = findViewById(R.id.vozr);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(poroda.vozrast);
            el.setTextColor(color);
            vozr.addView(el, vozr.getChildCount());

            LinearLayout vis = findViewById(R.id.vis);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(poroda.visota);
            el.setTextColor(color);
            vis.addView(el, vis.getChildCount());

            LinearLayout dia = findViewById(R.id.dia);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(poroda.diameter);
            el.setTextColor(color);
            dia.addView(el, dia.getChildCount());

            LinearLayout tovar = findViewById(R.id.kl_tov);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(poroda.tovarnost);
            el.setTextColor(color);
            tovar.addView(el, tovar.getChildCount());

            LinearLayout proish = findViewById(R.id.proish);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(DatabaseProcedures.GetDataFromPK("происхождение_породы", "code", poroda.proishogden));
            el.setTextColor(color);
            proish.addView(el, proish.getChildCount());

            LinearLayout poln = findViewById(R.id.poln);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText(polnota);
            el.setTextColor(color);
            poln.addView(el, poln.getChildCount());

            //TODO p g zap
            /*LinearLayout p_otnos = findViewById(R.id.p_otnos);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText("");
            el.setTextColor(color);
            p_otnos.addView(el, p_otnos.getChildCount());


            LinearLayout G = findViewById(R.id.G);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText("");
            el.setTextColor(color);
            G.addView(el, G.getChildCount());


            LinearLayout zap = findViewById(R.id.zap);
            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_template, null);
            el.setText("");
            el.setTextColor(color);
            zap.addView(el, zap.getChildCount());*/
        }
    }
    //endregion

    void init() {
        //region начальная инициализация
        //region Запись
        lesvo = findViewById(R.id.lesvo);
        uch_lesvo = findViewById(R.id.uch_lesvo);
        kvartal = findViewById(R.id.kvartal);
        videl = findViewById(R.id.videl);
        kz = findViewById(R.id.kz);
        //endregion

        //region Категория
        nomer_videla = findViewById(R.id.nomer_videla);
        ploshad_videla = findViewById(R.id.ploshad_videla);
        kat_zemel = findViewById(R.id.kat_zemel);
        ozu = findViewById(R.id.ozu);
        num = findViewById(R.id.num);
        expozicia = findViewById(R.id.expozicia);
        krutizna = findViewById(R.id.krutizna);
        erosia_vid = findViewById(R.id.erosia_vid);
        erosia_stepen = findViewById(R.id.erosia_stepen);
        //endregion

        //region Мероприятия
        meropr1 = findViewById(R.id.meropr1);
        meropr1_proc = findViewById(R.id.meropr1_proc);
        meropr1_rtk = findViewById(R.id.meropr1_rtk);
        meropr2 = findViewById(R.id.meropr2);
        meropr2_rtk = findViewById(R.id.meropr2_rtk);
        meropr3 = findViewById(R.id.meropr3);
        meropr3_rtk = findViewById(R.id.meropr3_rtk);
        cel_por = findViewById(R.id.cel_por);
        //endregion

        //region Вырубка
        god_vir = findViewById(R.id.god_vir);
        pne_vse = findViewById(R.id.pne_vse);
        pne_sos = findViewById(R.id.pne_sos);
        diam = findViewById(R.id.diam);
        tip_vir = findViewById(R.id.tip_vir);
        //endregion

        //region Ярус
        pre_por = findViewById(R.id.pre_por);
        kl_bon = findViewById(R.id.kl_bon);
        tip_les = findViewById(R.id.tip_les);
        tlu_obs = findViewById(R.id.tlu_obs);

        zahl = findViewById(R.id.zahl);
        likvid = findViewById(R.id.likvid);
        suhost = findViewById(R.id.suhost);
        //endregion

        //region Подрост подлесок
        podr_kolvo = findViewById(R.id.podr_kolvo);
        podr_visota = findViewById(R.id.podr_visota);
        podr_vozr = findViewById(R.id.podr_vozr);
        coef1 = findViewById(R.id.coef1);
        por1 = findViewById(R.id.por1);
        coef2 = findViewById(R.id.coef2);
        por2 = findViewById(R.id.por2);
        coef3 = findViewById(R.id.coef3);
        por3 = findViewById(R.id.por3);
        podl_gust = findViewById(R.id.podl_gust);
        podl_por1 = findViewById(R.id.podl_por1);
        podl_por2 = findViewById(R.id.podl_por2);
        podl_por3 = findViewById(R.id.podl_por3);
        //endregion

        //region GPS
        gps = findViewById(R.id.gps);
        //endregion

        //region Примечания
        primech = findViewById(R.id.note);
        //endregion
        //endregion

        //region Установка значений
        int color = getColor(R.color.colorPrimary);
        setDisabledForms();

        //region Запись
        LinearLayout l_zapis = findViewById(R.id.l_zapis);
        setBackgroundResourceRecursive(l_zapis, R.drawable.draw_cell);

        lesvo.setText(State.selectedLesvo.lesnichestvoName);
        lesvo.setTextColor(color);
        uch_lesvo.setText(State.selectedUchLesvo.uchLesvoName);
        uch_lesvo.setTextColor(color);
        kvartal.setText(State.selectedKvartal.kvartalNomer);
        kvartal.setTextColor(color);
        videl.setText(selectedVidel.nomerVidela);
        videl.setTextColor(color);
        kz.setText(DatabaseProcedures.GetDataFromPK("категория_земель_ТЗ", "code", selectedVidel.katZemel));
        kz.setTextColor(color);
        //endregion

        for (String form : State.activeForms.split(", ")) {
            int int_form = Integer.parseInt(form.trim());
            switch (int_form) {
                case 1:
                    //region Категория
                    try {
                        LinearLayout l_category = findViewById(R.id.l_category);
                        //setBackgroundResourceRecursive(l_category, R.drawable.draw_cell);
                        setTextColorRecursive(l_category, Color.BLACK);

                        nomer_videla.setText(selectedVidel.nomerVidela);
                        nomer_videla.setTextColor(color);
                        ploshad_videla.setText(selectedVidel.ploshadVidela);
                        ploshad_videla.setTextColor(color);
                        kat_zemel.setText(DatabaseProcedures.GetDataFromPK("категория_земель_ТЗ", "code", selectedVidel.katZemel));
                        kat_zemel.setTextColor(color);
                        ozu.setText(DatabaseProcedures.GetDataFromPK("ОЗУ_ТЗ", "code", selectedVidel.OZU));
                        ozu.setTextColor(color);
                        num.setText(selectedVidel.visotaNadMorem);
                        num.setTextColor(color);
                        expozicia.setText(DatabaseProcedures.GetDataFromPK("экспозиция_склона", "name", selectedVidel.exposSklona));
                        expozicia.setTextColor(color);
                        krutizna.setText(selectedVidel.krutSklona);
                        krutizna.setTextColor(color);
                        erosia_vid.setText(DatabaseProcedures.GetDataFromPK("вид_эрозии", "code", selectedVidel.erosiaVid));
                        erosia_vid.setTextColor(color);
                        erosia_stepen.setText(DatabaseProcedures.GetDataFromPK("степень_эрозии", "code", selectedVidel.erosiaStepen));
                        erosia_stepen.setTextColor(color);
                    }
                    catch (Exception e){
                        continue;
                    }
                    //endregion
                    break;
                case 2:
                    //region Ярус
                    try {
                        LinearLayout l_yarus = findViewById(R.id.l_yarus);
                        //setBackgroundResourceRecursive(l_yarus, R.drawable.draw_cell);
                        setTextColorRecursive(l_yarus, Color.BLACK);

                        pre_por.setText(DatabaseProcedures.GetDataFromPK("порода_ярус", "code", selectedVidel.preoblPoroda));
                        pre_por.setTextColor(color);
                        kl_bon.setText(selectedVidel.bonitet);
                        kl_bon.setTextColor(color);
                        tip_les.setText(DatabaseProcedures.GetDataFromPK("тип_леса", "short_title", selectedVidel.tipLesa));
                        tip_les.setTextColor(color);
                        tlu_obs.setText(DatabaseProcedures.GetDataFromPK("ТЛУ", "short_title", selectedVidel.TLU));
                        tlu_obs.setTextColor(color);

                        zahl.setText(selectedVidel.zahlam);
                        zahl.setTextColor(color);
                        likvid.setText(selectedVidel.likvidnost);
                        likvid.setTextColor(color);
                        suhost.setText(selectedVidel.syhostoj);
                        suhost.setTextColor(color);

                        ArrayList<YarusPoroda> yar1 = new ArrayList<>(selectedVidel.yarus1.yarusPorodaList);
                        ArrayList<YarusPoroda> yar2 = new ArrayList<>(selectedVidel.yarus2.yarusPorodaList);
                        ArrayList<YarusPoroda> yar3 = new ArrayList<>(selectedVidel.yarus3.yarusPorodaList);
                        initYarus(yar1, color, "1", selectedVidel.yarus1.polnota);
                        initYarus(yar2, color, "2", selectedVidel.yarus2.polnota);
                        initYarus(yar3, color, "3", selectedVidel.yarus3.polnota);
                    }
                    catch (Exception e){
                        continue;
                    }
                    //endregion
                    break;
                case 3:
                    //region Вырубка
                    try {
                        LinearLayout l_virubka = findViewById(R.id.l_virubka);
                        //setBackgroundResourceRecursive(l_virubka, R.drawable.draw_cell);
                        setTextColorRecursive(l_virubka, Color.BLACK);

                        god_vir.setText(selectedVidel.godVirubki);
                        god_vir.setTextColor(color);
                        pne_vse.setText(selectedVidel.pneyVsego);
                        pne_vse.setTextColor(color);
                        pne_sos.setText(selectedVidel.pneySosny);
                        pne_sos.setTextColor(color);
                        diam.setText(selectedVidel.diameter);
                        diam.setTextColor(color);
                        tip_vir.setText(DatabaseProcedures.GetDataFromPK("тип_вырубки", "short_title", selectedVidel.tipVirubki));
                        tip_vir.setTextColor(color);
                    }
                    catch (Exception e){
                        continue;
                    }
                    //endregion
                    break;
                case 4:
                    //region Мероприятия
                    try {
                        LinearLayout l_event = findViewById(R.id.l_event);
                        //setBackgroundResourceRecursive(l_event, R.drawable.draw_cell);
                        setTextColorRecursive(l_event, Color.BLACK);

                        meropr1.setText(DatabaseProcedures.GetDataFromPK("мероприятия_ТЗ", "code", selectedVidel.meropr1Vid));
                        meropr1.setTextColor(color);
                        meropr1_proc.setText(selectedVidel.meropr1Proc);
                        meropr1_proc.setTextColor(color);
                        meropr1_rtk.setText(selectedVidel.meropr1RTK);
                        meropr1_rtk.setTextColor(color);
                        meropr2.setText(DatabaseProcedures.GetDataFromPK("мероприятия_ТЗ", "code", selectedVidel.meropr2Vid));
                        meropr2.setTextColor(color);
                        meropr2_rtk.setText(selectedVidel.meropr2RTK);
                        meropr3.setText(DatabaseProcedures.GetDataFromPK("мероприятия_ТЗ", "code", selectedVidel.meropr3Vid));
                        meropr3.setTextColor(color);
                        meropr3_rtk.setText(selectedVidel.meropr3RTK);
                        meropr3_rtk.setTextColor(color);
                        cel_por.setText(DatabaseProcedures.GetDataFromPK("порода_ярус", "code", selectedVidel.celPoroda));
                        cel_por.setTextColor(color);
                    }
                    catch (Exception e){
                        continue;
                    }
                    //endregion
                    break;
                case 5:
                    //region Подрост подлесок
                    try {
                        LinearLayout l_pordrost = findViewById(R.id.l_pordrost);
                        //setBackgroundResourceRecursive(l_pordrost, R.drawable.draw_cell);
                        setTextColorRecursive(l_pordrost, Color.BLACK);

                        podr_kolvo.setText(selectedVidel.podrostGustota);
                        podr_kolvo.setTextColor(color);
                        podr_visota.setText(selectedVidel.podrostVisota);
                        podr_visota.setTextColor(color);
                        podr_vozr.setText(selectedVidel.podrostVozrast);
                        podr_vozr.setTextColor(color);
                        coef1.setText(selectedVidel.podrostCoef1);
                        coef1.setTextColor(color);
                        por1.setText(DatabaseProcedures.GetDataFromPK("порода_подрост", "code", selectedVidel.podrostPoroda1));
                        por1.setTextColor(color);
                        coef2.setText(selectedVidel.podrostCoef2);
                        coef2.setTextColor(color);
                        por2.setText(DatabaseProcedures.GetDataFromPK("порода_подрост", "code", selectedVidel.podrostPoroda2));
                        por2.setTextColor(color);
                        coef3.setText(selectedVidel.podrostCoef3);
                        coef3.setTextColor(color);
                        por3.setText(DatabaseProcedures.GetDataFromPK("порода_подрост", "code", selectedVidel.podrostPoroda3));
                        por3.setTextColor(color);

                        podl_gust.setText(DatabaseProcedures.GetDataFromPK("густота_подлеска", "code", selectedVidel.podlesokGustota));
                        podl_gust.setTextColor(color);
                        podl_por1.setText(DatabaseProcedures.GetDataFromPK("порода_подлесок", "code", selectedVidel.podlesokPoroda1));
                        podl_por1.setTextColor(color);
                        podl_por2.setText(DatabaseProcedures.GetDataFromPK("порода_подлесок", "code", selectedVidel.podlesokPoroda2));
                        podl_por2.setTextColor(color);
                        podl_por3.setText(DatabaseProcedures.GetDataFromPK("порода_подлесок", "code", selectedVidel.podlesokPoroda3));
                        podl_por3.setTextColor(color);
                    }
                    catch (Exception e){
                        continue;
                    }
                    //endregion
                    break;
                case 6:
                    //region МДС
                    LinearLayout l_mds = findViewById(R.id.l_mds);
                    //setBackgroundResourceRecursive(l_mds, R.drawable.draw_cell);
                    setTextColorRecursive(l_mds, Color.BLACK);

                    break;
                //endregion

                case 11:
                    //region 11
                    try {
                        if (selectedVidel.selectedMDS.contains("11")) {
                            LinearLayout mds1 = findViewById(R.id.mds1);
                            TextView el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket11.godSozd);
                            el.setTextColor(color);
                            mds1.addView(el, mds1.getChildCount());

                            LinearLayout mds2 = findViewById(R.id.mds2);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("способ_обр_почвы", "code", selectedVidel.maket11.sposobObrabotki));
                            el.setTextColor(color);
                            mds2.addView(el, mds2.getChildCount());

                            LinearLayout mds3 = findViewById(R.id.mds3);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("способ_созд_лк", "code", selectedVidel.maket11.sposobSozd));
                            el.setTextColor(color);
                            mds3.addView(el, mds3.getChildCount());

                            LinearLayout mds4 = findViewById(R.id.mds4);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket11.mejduRyad);
                            el.setTextColor(color);
                            mds4.addView(el, mds4.getChildCount());

                            LinearLayout mds5 = findViewById(R.id.mds5);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket11.vRyad);
                            el.setTextColor(color);
                            mds5.addView(el, mds5.getChildCount());

                            LinearLayout mds6 = findViewById(R.id.mds6);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket11.kolvo);
                            el.setTextColor(color);
                            mds6.addView(el, mds6.getChildCount());

                            LinearLayout mds7 = findViewById(R.id.mds7);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("состояние_лес_культ", "code", selectedVidel.maket11.sostoyanie));
                            el.setTextColor(color);
                            mds7.addView(el, mds7.getChildCount());

                            LinearLayout mds8 = findViewById(R.id.mds8);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("причина_неуд_сост", "code", selectedVidel.maket11.prichinaNeudSost));
                            el.setTextColor(color);
                            mds8.addView(el, mds8.getChildCount());

                            LinearLayout mds = findViewById(R.id.mds);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("11");
                            el.setTextColor(color);
                            mds.addView(el, mds.getChildCount());
                        }
                    }
                    catch (Exception e){
                        continue;
                    }
                    break;
                //endregion
                case 12:
                    //region 12
                    try {
                        if (selectedVidel.selectedMDS.contains("12")) {
                            LinearLayout mds1 = findViewById(R.id.mds1);
                            TextView el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("причина_неуд_сост", "code", selectedVidel.maket12.tipPovrejd));
                            el.setTextColor(color);
                            mds1.addView(el, mds1.getChildCount());

                            LinearLayout mds2 = findViewById(R.id.mds2);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket12.godPovrejd);
                            el.setTextColor(color);
                            mds2.addView(el, mds2.getChildCount());

                            LinearLayout mds3 = findViewById(R.id.mds3);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("порода_ярус", "code", selectedVidel.maket12.povrejdPoroda));
                            el.setTextColor(color);
                            mds3.addView(el, mds3.getChildCount());

                            LinearLayout mds4 = findViewById(R.id.mds4);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("болезнь", "code", selectedVidel.maket12.bolezn1));
                            el.setTextColor(color);
                            mds4.addView(el, mds4.getChildCount());

                            LinearLayout mds5 = findViewById(R.id.mds5);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("степень_поврежд", "code", selectedVidel.maket12.stepen1));
                            el.setTextColor(color);
                            mds5.addView(el, mds5.getChildCount());

                            LinearLayout mds6 = findViewById(R.id.mds6);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("болезнь", "code", selectedVidel.maket12.bolezn2));
                            el.setTextColor(color);
                            mds6.addView(el, mds6.getChildCount());

                            LinearLayout mds7 = findViewById(R.id.mds7);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("степень_поврежд", "code", selectedVidel.maket12.stepen2));
                            el.setTextColor(color);
                            mds7.addView(el, mds7.getChildCount());

                            LinearLayout mds8 = findViewById(R.id.mds8);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket12.istochnik);
                            el.setTextColor(color);
                            mds8.addView(el, mds8.getChildCount());

                            LinearLayout mds = findViewById(R.id.mds);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("12");
                            el.setTextColor(color);
                            mds.addView(el, mds.getChildCount());
                        }
                    }
                    catch (Exception e){
                        continue;
                    }
                    break;
                //endregion
                case 13:
                    //region 13
                    try {
                        if (selectedVidel.selectedMDS.contains("13")) {
                            LinearLayout mds1 = findViewById(R.id.mds1);
                            TextView el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket13.shirina);
                            el.setTextColor(color);
                            mds1.addView(el, mds1.getChildCount());

                            LinearLayout mds2 = findViewById(R.id.mds2);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket13.protyaj);
                            el.setTextColor(color);
                            mds2.addView(el, mds2.getChildCount());

                            LinearLayout mds3 = findViewById(R.id.mds3);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("состояние_линейн_объект", "code", selectedVidel.maket13.sostojan));
                            el.setTextColor(color);
                            mds3.addView(el, mds3.getChildCount());

                            LinearLayout mds4 = findViewById(R.id.mds4);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("назначение_дороги", "code", selectedVidel.maket13.naznachDorogi));
                            el.setTextColor(color);
                            mds4.addView(el, mds4.getChildCount());

                            LinearLayout mds5 = findViewById(R.id.mds5);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("тип_покрытия", "code", selectedVidel.maket13.tipPokrutija));
                            el.setTextColor(color);
                            mds5.addView(el, mds5.getChildCount());

                            LinearLayout mds6 = findViewById(R.id.mds6);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket13.shirinaDorogi);
                            el.setTextColor(color);
                            mds6.addView(el, mds6.getChildCount());

                            LinearLayout mds7 = findViewById(R.id.mds7);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("сезонность_действия", "code", selectedVidel.maket13.sezonnost));
                            el.setTextColor(color);
                            mds7.addView(el, mds7.getChildCount());

                            LinearLayout mds8 = findViewById(R.id.mds8);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket13.dlinaMeropr);
                            el.setTextColor(color);
                            mds8.addView(el, mds8.getChildCount());

                            LinearLayout mds = findViewById(R.id.mds);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("13");
                            el.setTextColor(color);
                            mds.addView(el, mds.getChildCount());
                        }
                    }
                    catch (Exception e) {
                        continue;
                    }
                    break;
                //endregion
                case 17:
                    //region 17
                    try {
                        if (selectedVidel.selectedMDS.contains("17")) {
                            LinearLayout mds1 = findViewById(R.id.mds1);
                            TextView el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("тип_пользователя", "code", selectedVidel.maket17.polzovatel));
                            el.setTextColor(color);
                            mds1.addView(el, mds1.getChildCount());

                            LinearLayout mds2 = findViewById(R.id.mds2);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("качество_угодья", "code", selectedVidel.maket17.kachestvo));
                            el.setTextColor(color);
                            mds2.addView(el, mds2.getChildCount());

                            LinearLayout mds3 = findViewById(R.id.mds3);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("тип_сенокоса", "code", selectedVidel.maket17.tipSenokosa));
                            el.setTextColor(color);
                            mds3.addView(el, mds3.getChildCount());

                            LinearLayout mds4 = findViewById(R.id.mds4);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("состояние_сх", "code", selectedVidel.maket17.sostoyanie));
                            el.setTextColor(color);
                            mds4.addView(el, mds4.getChildCount());

                            LinearLayout mds5 = findViewById(R.id.mds5);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("порода_ярус", "code", selectedVidel.maket17.porodaZarast));
                            el.setTextColor(color);
                            mds5.addView(el, mds5.getChildCount());

                            LinearLayout mds6 = findViewById(R.id.mds6);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket17.procentZarast);
                            el.setTextColor(color);
                            mds6.addView(el, mds6.getChildCount());

                            LinearLayout mds7 = findViewById(R.id.mds7);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket17.urogainost);
                            el.setTextColor(color);
                            mds7.addView(el, mds7.getChildCount());

                            LinearLayout mds8 = findViewById(R.id.mds8);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("");
                            el.setTextColor(color);
                            mds8.addView(el, mds8.getChildCount());

                            LinearLayout mds = findViewById(R.id.mds);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("17");
                            el.setTextColor(color);
                            mds.addView(el, mds.getChildCount());
                        }
                    }
                    catch (Exception e){
                        continue;
                    }
                    break;
                //endregion
                case 19:
                    //region 19
                    try {
                        if (selectedVidel.selectedMDS.contains("19")) {
                            LinearLayout mds1 = findViewById(R.id.mds1);
                            TextView el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("тип_болота", "code", selectedVidel.maket19.tipBolota));
                            el.setTextColor(color);
                            mds1.addView(el, mds1.getChildCount());

                            LinearLayout mds2 = findViewById(R.id.mds2);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("тип_растительности", "code", selectedVidel.maket19.tipRast));
                            el.setTextColor(color);
                            mds2.addView(el, mds2.getChildCount());

                            LinearLayout mds3 = findViewById(R.id.mds3);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket19.moshTorfSloj);
                            el.setTextColor(color);
                            mds3.addView(el, mds3.getChildCount());

                            LinearLayout mds4 = findViewById(R.id.mds4);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("порода_ярус", "code", selectedVidel.maket19.porodaZarast));
                            el.setTextColor(color);
                            mds4.addView(el, mds4.getChildCount());

                            LinearLayout mds5 = findViewById(R.id.mds5);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(selectedVidel.maket19.stepenZarast);
                            el.setTextColor(color);
                            mds5.addView(el, mds5.getChildCount());

                            LinearLayout mds6 = findViewById(R.id.mds6);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("");
                            el.setTextColor(color);
                            mds6.addView(el, mds6.getChildCount());

                            LinearLayout mds7 = findViewById(R.id.mds7);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("");
                            el.setTextColor(color);
                            mds7.addView(el, mds7.getChildCount());

                            LinearLayout mds8 = findViewById(R.id.mds8);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("");
                            el.setTextColor(color);
                            mds8.addView(el, mds8.getChildCount());

                            LinearLayout mds = findViewById(R.id.mds);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("19");
                            el.setTextColor(color);
                            mds.addView(el, mds.getChildCount());
                        }
                    }
                    catch (Exception e) {
                        continue;
                    }
                    break;
                //endregion
                case 21:
                    //region 21
                    try {
                        if (selectedVidel.selectedMDS.contains("21")) {
                            LinearLayout mds1 = findViewById(R.id.mds1);
                            TextView el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("тип_ландшафта", "code", selectedVidel.maket21.tipLand));
                            el.setTextColor(color);
                            mds1.addView(el, mds1.getChildCount());

                            LinearLayout mds2 = findViewById(R.id.mds2);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("эстет_оценка", "code", selectedVidel.maket21.estetPoint));
                            el.setTextColor(color);
                            mds2.addView(el, mds2.getChildCount());

                            LinearLayout mds3 = findViewById(R.id.mds3);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("сан_оценка", "code", selectedVidel.maket21.sanitarPoint));
                            el.setTextColor(color);
                            mds3.addView(el, mds3.getChildCount());

                            LinearLayout mds4 = findViewById(R.id.mds4);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("устойчивость", "code", selectedVidel.maket21.ustojchuvost));
                            el.setTextColor(color);
                            mds4.addView(el, mds4.getChildCount());

                            LinearLayout mds5 = findViewById(R.id.mds5);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("проходимость", "code", selectedVidel.maket21.prohodimost));
                            el.setTextColor(color);
                            mds5.addView(el, mds5.getChildCount());

                            LinearLayout mds6 = findViewById(R.id.mds6);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("просматриваемость", "code", selectedVidel.maket21.prosmatrivaemost));
                            el.setTextColor(color);
                            mds6.addView(el, mds6.getChildCount());

                            LinearLayout mds7 = findViewById(R.id.mds7);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("стадия_дегрессии", "code", selectedVidel.maket21.stadiaDigresija));
                            el.setTextColor(color);
                            mds7.addView(el, mds7.getChildCount());

                            LinearLayout mds8 = findViewById(R.id.mds8);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("МАФ", "code", selectedVidel.maket21.MAF));
                            el.setTextColor(color);
                            mds8.addView(el, mds8.getChildCount());

                            LinearLayout mds = findViewById(R.id.mds);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("21");
                            el.setTextColor(color);
                            mds.addView(el, mds.getChildCount());
                        }
                    }
                    catch (Exception e){
                        continue;
                    }
                    break;
                //endregion
                case 23:
                    //region 23
                    try {
                        if (selectedVidel.selectedMDS.contains("23")) {
                            LinearLayout mds1 = findViewById(R.id.mds1);
                            TextView el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", selectedVidel.maket23.osobennost1));
                            el.setTextColor(color);
                            mds1.addView(el, mds1.getChildCount());

                            LinearLayout mds2 = findViewById(R.id.mds2);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", selectedVidel.maket23.osobennost2));
                            el.setTextColor(color);
                            mds2.addView(el, mds2.getChildCount());

                            LinearLayout mds3 = findViewById(R.id.mds3);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", selectedVidel.maket23.osobennost3));
                            el.setTextColor(color);
                            mds3.addView(el, mds3.getChildCount());

                            LinearLayout mds4 = findViewById(R.id.mds4);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", selectedVidel.maket23.osobennost4));
                            el.setTextColor(color);
                            mds4.addView(el, mds4.getChildCount());

                            LinearLayout mds5 = findViewById(R.id.mds5);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", selectedVidel.maket23.osobennost5));
                            el.setTextColor(color);
                            mds5.addView(el, mds5.getChildCount());

                            LinearLayout mds6 = findViewById(R.id.mds6);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", selectedVidel.maket23.osobennost6));
                            el.setTextColor(color);
                            mds6.addView(el, mds6.getChildCount());

                            LinearLayout mds7 = findViewById(R.id.mds7);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", selectedVidel.maket23.osobennost7));
                            el.setTextColor(color);
                            mds7.addView(el, mds7.getChildCount());

                            LinearLayout mds8 = findViewById(R.id.mds8);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText(DatabaseProcedures.GetDataFromPK("особенности_ТЗ", "code", selectedVidel.maket23.osobennost8));
                            el.setTextColor(color);
                            mds8.addView(el, mds8.getChildCount());

                            LinearLayout mds = findViewById(R.id.mds);
                            el = (TextView) getLayoutInflater().inflate(R.layout.textview_observe_mds, null);
                            el.setText("23");
                            el.setTextColor(color);
                            mds.addView(el, mds.getChildCount());
                        }
                    }
                    catch (Exception e){
                        continue;
                    }
                    break;
                //endregion
                case 7:
                    //GPS
                    try {
                        LinearLayout l_gps = findViewById(R.id.l_gps);
                        //setBackgroundResourceRecursive(l_gps, R.drawable.draw_cell);
                        setTextColorRecursive(l_gps, Color.BLACK);

                        gps.setText(selectedVidel.GPS);
                        gps.setTextColor(color);
                    }
                    catch (Exception e){
                        continue;
                    }
                    break;
                case 8:
                    //Примечания
                    try {
                        LinearLayout l_note = findViewById(R.id.l_note);
                        //setBackgroundResourceRecursive(l_note, R.drawable.draw_cell);
                        setTextColorRecursive(l_note, Color.BLACK);

                        primech.setText(selectedVidel.note);
                        primech.setTextColor(color);
                    }
                    catch (Exception e){
                        continue;
                    }
                    break;
            }
        }

        //endregion
    }
}