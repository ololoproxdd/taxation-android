package com.example.ololoproxdd.sqlliteforest.HelpClasses.Services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.EditItem;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;

import java.io.IOException;
import java.util.ArrayList;

public class DatabaseProcedures {

    //region поля
    public static Context _context;
    //endregion

    //region методы

    public static SQLiteDatabase initDB(){
        DatabaseHelper HelperDB = new DatabaseHelper(_context, "forest.db");

        try {
            HelperDB.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            return HelperDB.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
    }

    static Cursor run(String script, String[] params){
        SQLiteDatabase db = initDB();
        Cursor query = db.rawQuery(script,  params);
        //db.close();
        return query;
    }

    public static String GetPK(String table, String field, String value){
        String script = "Select primarykey from " + table + " where " + field + "= ?";
        Cursor query = run(script,  new String[]{value});
        String result = "";
        if(query.moveToFirst()){
            result = query.getString(0);
        }
        query.close();
        return result;
    }

    public static String GetPKCode(String table, String value){
        String code = value.split("-", 2)[0];
        String script = "Select primarykey from " + table + " where code = ?";
        Cursor query = run(script, new String[]{code});
        String result = "";
        if(query.moveToFirst()){
            result = query.getString(0);
        }
        query.close();
        return result;
    }

    public static String GetDataFromPK(String table, String field, String primarykey){
        String script = "Select " + field + " from " + table + " where primarykey = ?";
        Cursor query = run(script, new String[]{primarykey});
        String result = "-";
        if(query.moveToFirst()){
            result = query.getString(0);
        }
        query.close();
        return result;
    }

    public static String GetDataFromPK(String table, String field1, String field2, String primarykey){
        String script = "Select " + field1 + ", " + field2 + " from " + table + " where primarykey = ?";
        Cursor query = run(script, new String[]{primarykey});
        String result = "";
        if(query.moveToFirst()){
            if (query.getString(1).equals("-"))
                result = query.getString(0);
            else
                result = query.getString(0) + "-" + query.getString(1);
        }
        query.close();
        return result;
    }

    public static ArrayList<String> runScriptCodePair(String script){
        Cursor query = run(script, null);
        ArrayList<String> result = null;
        if(query.moveToFirst()){
            result = new ArrayList<>();
            do {
                if (query.getString(1).equals("-"))
                    result.add(query.getString(0));
                else
                    result.add(query.getString(0) + "-" + query.getString(1));
            }
            while(query.moveToNext());
        }
        query.close();
        return result;
    }

    public static ArrayList<EditItem> runScriptCodeEditorPair(String script){
        Cursor query = run(script, null);
        ArrayList<EditItem> result = null;
        if(query.moveToFirst()){
            result = new ArrayList<>();
            do {
                EditItem item = new EditItem();
                item.name = query.getString(0);
                item.checked = query.getInt(1) > 0;
                result.add(item);
            }
            while(query.moveToNext());
        }
        query.close();
        return result;
    }

    public static ArrayList<String> runScript(String script){
        Cursor query = run(script, null);
        ArrayList<String> result = null;
        if(query.moveToFirst()){
            result = new ArrayList<>();
            do {
                result.add(query.getString(0));
            }
            while(query.moveToNext());
        }
        query.close();
        return result;
    }

    public static ArrayList<String> runScriptParams(String script, String[] params){
        Cursor query = run(script, params);
        ArrayList<String> result = null;
        if(query.moveToFirst()){
            result = new ArrayList<>();
            do {
                result.add(query.getString(0));
            }
            while(query.moveToNext());
        }
        query.close();
        return result;
    }

    public static ArrayList<String> getDataOrderDesc(String table, String field){
        String script = "Select " + field + " from " + table + " order by primarykey desc";
        return runScript(script);
    }

    public static ArrayList<String> getDataActual(String table, String field){
        String script = "Select " + field + " from " + table + " where actual = 1";
        return runScript(script);
    }

    public static ArrayList<String> getData(String table, String field1, String field2){
        String script = "Select " + field1 + ", " + field2 + " from " + table;
        return runScriptCodePair(script);
    }

    public static ArrayList<String> getDataActual(String table, String field1, String field2){
        String script = "Select " + field1 + ", " + field2 + " from " + table + " where actual = 1";
        return runScriptCodePair(script);
    }

    public static ArrayList<EditItem> getDataEditor(String table, String field1, String field2){
        String script = "Select " + field1 + ", " + field2 + " from " + table;
        return runScriptCodeEditorPair(script);
    }

    public static ArrayList<String> getData(String table){
        String script = "Select name from " + table;
        return runScript(script);
    }

    public static ArrayList<String> getDataActual(String table){
        String script = "Select name from " + table + " where actual = 1";
        return runScript(script);
    }

    public static ArrayList<String> getData(String table, String field){
        String script = "Select " + field + " from " + table;
        return runScript(script);
    }

    //region Бонитет
    public static String getBonitet(Integer visota, Integer vozrast, Integer poroda_type){
        String script = "SELECT case when " + visota + ">=Vysota98 then '1Б' " +
                "             when " + visota + ">=Vysota99 then '1А' " +
                "             when " + visota + ">= Vysota100 then '1' " +
                "             when " + visota + ">=Vysota200 then '2' " +
                "             when " + visota + ">=Vysota300 then '3' " +
                "             when " + visota + ">=Vysota400 then '4' " +
                "             when " + visota + ">=Vysota500 then '5' " +
                "             when " + visota + ">= Vysota501 then '5A' " +
                "             when " + visota + ">=Vysota502  then '5Б' " +
                "           end " +
                "  FROM LogBonitet where BonitetNomTab=" + poroda_type + " and Vozrast>= " + vozrast +
                "  order by Vozrast";
        Cursor query = run(script, null);
        String result = "";
        if(query.moveToFirst())
            result = query.getString(0);
        query.close();
        return result;
    }
    //endregion

    public static boolean setActual(String table, Integer actual, String field, String value){
        ContentValues cv = new ContentValues();
        cv.put("actual", actual);
        SQLiteDatabase db = initDB();
        long result = db.update(table, cv, field + "=" + value, null);
        return result > 0;
    }

    //endregion
}
