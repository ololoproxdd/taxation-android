package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.ModelPoroda;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Yarus;
import com.example.ololoproxdd.sqlliteforest.XmlModels.YarusPoroda;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;

public class AddPoroduActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_type_tree = null;
    Spinner spinner_yarus_poroda_coeff = null;
    Spinner spinnerTovarnost = null;
    Spinner spinnerProishogd = null;
    EditText vozrast = null;
    EditText visota = null;
    EditText diametr = null;

    ArrayList<String> poroda_yarus = null;
    ArrayList<String> rate = null;
    ArrayList<String> tovarnost = null;
    ArrayList<String> proishojdenie = null;

    Yarus selectedYarus = State.selectedYarus;
    YarusPoroda selectedPoroda = State.selectedPoroda;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_porodu);
        getSupportActionBar().setTitle("Добавление породы");
        getDataFromDB();
        init();
        initHeader();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    //установка пустых полей нулевыми значениями
    void presave(){
        if (vozrast.getText().toString().trim().isEmpty())
            vozrast.setText("0");

        if (visota.getText().toString().trim().isEmpty())
            visota.setText("0");

        if (diametr.getText().toString().trim().isEmpty())
            diametr.setText("0");
    }

    public boolean save(boolean validation){
        presave();
        if (validation){
            String errorMessage = "";

            //обязатаельные поля для ввода
            if (spinner_type_tree.getSelectedItem().toString().equals("-") ||
                    spinner_yarus_poroda_coeff.getSelectedItem().toString().isEmpty() ||
                    vozrast.getText().toString().isEmpty() ||
                    visota.getText().toString().isEmpty() ||
                    diametr.getText().toString().isEmpty())
                errorMessage += "Заполните обязательное поле ввода!\n";

            if (errorMessage.isEmpty()) {
                //Высота больше 41 м или меньше 1 м
                Integer height = Integer.parseInt(visota.getText().toString().trim());
                if (height > 41 || height < 1)
                    errorMessage += "Проверь высоту!\n";
                //Диаметр больше 100  см или меньше 2 см
                //при высоте породы 0,5 или 1 м, заполнен диаметр
                //При высоте породы 1,5 м и более, не заполнен диаметр
                Integer diam = Integer.parseInt(diametr.getText().toString().trim());
                if (diam > 100 || (height <= 1 && diam != 0) || (height > 1 && diam == 0))
                    errorMessage += "Проверь диаметр!\n";
                //Возраст меньше 1 года или больше 200 лет
                Integer age = Integer.parseInt(vozrast.getText().toString().trim());
                if (age > 200 || age < 1)
                    errorMessage += "Проверь возраст!\n";
                //Соотношение высота/диаметр больше 3
                if (diam != 0 && height * 1.0 / diam > 3)
                    errorMessage += "Проверь высоту/диаметр!\n";
            }

            if (!errorMessage.isEmpty()){
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Порода яруса");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save(){
        presave();
        //int
        selectedPoroda.coeff = spinner_yarus_poroda_coeff.getSelectedItem().toString();
        selectedPoroda.vozrast = vozrast.getText().toString();
        selectedPoroda.visota = visota.getText().toString();
        selectedPoroda.diameter = diametr.getText().toString();
        selectedPoroda.tovarnost = spinnerTovarnost.getSelectedItem().toString();

        //guid
        selectedPoroda.poroda= DatabaseProcedures.GetPK("порода_ярус", "name", spinner_type_tree.getSelectedItem().toString());
        selectedPoroda.proishogden = DatabaseProcedures.GetPK("происхождение_породы", "name", spinnerProishogd.getSelectedItem().toString());

        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    public void init(){
        //region начальная инициализация
        spinner_type_tree = findViewById(R.id.spinner_type_tree);
        ArrayAdapter<String> adapter_spinner_type_tree = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poroda_yarus);
        adapter_spinner_type_tree.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type_tree.setAdapter(adapter_spinner_type_tree);

        spinner_yarus_poroda_coeff = findViewById(R.id.spinner_yarus_poroda_coeff);
        ArrayAdapter<String> adapter_spinner_yarus_poroda_coeff = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, rate);
        adapter_spinner_yarus_poroda_coeff.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_yarus_poroda_coeff.setAdapter(adapter_spinner_yarus_poroda_coeff);

        spinnerTovarnost = findViewById(R.id.spinnerTovarnost);
        ArrayAdapter<String> adapter_spinnerTovarnost = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tovarnost);
        adapter_spinnerTovarnost.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTovarnost.setAdapter(adapter_spinnerTovarnost);

        spinnerProishogd = findViewById(R.id.spinnerProishogd);
        ArrayAdapter<String> adapter_spinnerProishogd = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, proishojdenie);
        adapter_spinnerProishogd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProishogd.setAdapter(adapter_spinnerProishogd);

        vozrast = findViewById(R.id.edit_age);

        visota = findViewById(R.id.edit_height);

        diametr = findViewById(R.id.edit_diameter);
        //endregion

        //region события
        spinner_type_tree.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_yarus_poroda_coeff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinnerTovarnost.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinnerProishogd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        vozrast.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        vozrast.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });
        visota.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        visota.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        diametr.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        diametr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });


        //endregion

        // region загружаем информацию из xml
        if (selectedYarus != null && selectedPoroda != null) {
            //порода
            if (selectedPoroda.poroda != null) {
                String poroda = DatabaseProcedures.GetDataFromPK("порода_ярус", "name", selectedPoroda.poroda);
                spinner_type_tree.setSelection(adapter_spinner_type_tree.getPosition(poroda));
            }

            //коэфф
            if (selectedPoroda.coeff != null)
                spinner_yarus_poroda_coeff.setSelection(adapter_spinner_yarus_poroda_coeff.getPosition(selectedPoroda.coeff));

            //возраст
            if (selectedPoroda.vozrast != null)
                vozrast.setText(selectedPoroda.vozrast);

            //высота
            if (selectedPoroda.visota != null)
                visota.setText(selectedPoroda.visota);

            //диаметр
            if (selectedPoroda.diameter != null)
                diametr.setText(selectedPoroda.diameter);

            //товарность
            if (selectedPoroda.tovarnost != null)
                spinnerTovarnost.setSelection(adapter_spinnerTovarnost.getPosition(selectedPoroda.tovarnost));

            //происхождение
            if (selectedPoroda.proishogden != null) {
                String proish = DatabaseProcedures.GetDataFromPK("происхождение_породы", "name", selectedPoroda.proishogden);
                spinnerProishogd.setSelection(adapter_spinnerProishogd.getPosition(proish));
            }
        }
        //endregion

        //region устанавливаем заголовок
        TextView title = findViewById(R.id.add_porodu_title);
        String titleText = State.selectedYarusRadioButton.getText().toString() + " | Порода " + State.selectedYarus.yarusPorodaList.size();
        title.setText(titleText);
        //endregion

        //region устанавливаем составbreak;
        if (State.selectedSostavPorod != null) {
            TextView tv_sostav_poroda = findViewById(R.id.text_coeff);
            String sostav_porod = "";
            String coef0 = "";
            for (ModelPoroda poroda : State.selectedSostavPorod) {
                if (poroda.Coef > 0)
                    sostav_porod += poroda.Coef.toString() + poroda.Code + " ";
                else {
                    if (coef0.isEmpty())
                        coef0 += "+";
                    else
                        coef0 += ", ";
                    coef0 += poroda.Code;

                }
            }
            tv_sostav_poroda.setText(sostav_porod + coef0);
        }
        //endregion
    }

    void initHeader(){
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region БД
    public void getDataFromDB(){
        poroda_yarus = DatabaseProcedures.getDataActual("порода_ярус");
        rate = DatabaseProcedures.getData("коэф_подроста");
        tovarnost = DatabaseProcedures.getData("товарность");
        proishojdenie = DatabaseProcedures.getDataActual("происхождение_породы");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
