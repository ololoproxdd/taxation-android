package com.example.ololoproxdd.sqlliteforest.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.AddPoroduActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.YarusActivity;
import com.example.ololoproxdd.sqlliteforest.Adapters.NoteAdapter;
import com.example.ololoproxdd.sqlliteforest.Adapters.YarusPorodaAdapter;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Kvartal;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Yarus;
import com.example.ololoproxdd.sqlliteforest.XmlModels.YarusPoroda;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.io.IOException;
import java.util.ArrayList;


public class YarusFragment extends Fragment {

    //region поля
    ArrayList<String> type_yarus = null;
    ArrayList<String> polnota = null;

    View _this = null;
    Context _context = null;
    Button add_porodu = null;
    Spinner spinner_type_yarus = null;
    Spinner spinner_polnota = null;
    EditText obiem = null;
    ListView list_porodi = null;

    Videl selectedVidel = State.selectedVidel;
    Yarus selectedYarus = State.selectedYarus;

    public int summaCoef = 0;
    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_yarus, container, false);
        _this = view;

        getDataFromDB();
        init();

        //ViewGroup.LayoutParams contentViewLayout = new ViewGroup.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT );
        //view.setLayoutParams( contentViewLayout );

        return view;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        try {
            _context = context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " должен реализовывать интерфейс OnFragmentInteractionListener");
        }
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues(){
        if (obiem.getText().toString().isEmpty())
            obiem.setText("0");
    }

    public void save(){
        setDefaultValues();

        //int
        selectedYarus.polnota = spinner_polnota.getSelectedItem().toString();
        selectedYarus.SZN = obiem.getText().toString();

        //guid
        selectedYarus.typeYarus = DatabaseProcedures.GetPK("тип_яруса", "name", spinner_type_yarus.getSelectedItem().toString());

        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    public String AddProbeli(int n){
        String result = "";
        for (int i = 0; i < n; i++){
            result += " ";
        }
        return result;
    }

    public String AddProbeli(String str1, String str2){
        String result = "";
        int n = str1.length() - str2.length() * 2;
        for (int i = 0; i < n; i++){
            result += " ";
        }
        return result;
    }

    /*public ArrayList<String> getPorodaList(ArrayList<YarusPoroda> yarusPorodaList) {
        if (yarusPorodaList != null) {
            ArrayList<String> result = new ArrayList<>();
            summaCoef = 0;
            for (int i = 0; i < yarusPorodaList.size(); i++) {
                String row = "";
                //коэффициент
                String coef = yarusPorodaList.get(i).coeff != null ? yarusPorodaList.get(i).coeff : "-";
                row += coef + AddProbeli("           ", coef);
                summaCoef += Integer.parseInt(coef.trim());
                //порода
                String por = DatabaseProcedures.GetDataFromPK("порода_ярус", "code",yarusPorodaList.get(i).poroda);
                row += por + AddProbeli("              ", por);
                //возраст
                String voz = yarusPorodaList.get(i).vozrast != null ? yarusPorodaList.get(i).vozrast : "-";
                row += voz + AddProbeli("              ", voz);
                //высота
                String vis = yarusPorodaList.get(i).visota != null ? yarusPorodaList.get(i).visota : "-";
                row += vis + AddProbeli("             ", vis);
                //диаметр
                String diam = yarusPorodaList.get(i).diameter != null ? yarusPorodaList.get(i).diameter : "-";
                row += diam + AddProbeli("                 ", diam);
                //товарность
                row += yarusPorodaList.get(i).tovarnost != null ? yarusPorodaList.get(i).tovarnost : "-";
                result.add(row);
            }
            return result;
        }
        return null;
    }*/

    public void initListViewPoroda(){
        list_porodi = _this.findViewById(R.id.list_poroda);
        YarusPorodaAdapter adapter_list_porodi = new YarusPorodaAdapter(_context, R.layout.yarus_poroda_item, State.selectedYarus.yarusPorodaList);
        list_porodi.setAdapter(adapter_list_porodi);
    }

    public void init() {
        selectedVidel = State.selectedVidel;
        selectedYarus = State.selectedYarus;

        //region начальная инициаизация
        spinner_type_yarus = _this.findViewById(R.id.spinner_type_yarus);
        ArrayAdapter<String> adapter_spinner_type_yarus = new ArrayAdapter<String>(_context, android.R.layout.simple_spinner_item, type_yarus);
        adapter_spinner_type_yarus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type_yarus.setAdapter(adapter_spinner_type_yarus);

        spinner_polnota = _this.findViewById(R.id.spinner_polnota);
        ArrayAdapter<String> adapter_spinner_polnota = new ArrayAdapter<String>(_context, android.R.layout.simple_spinner_item, polnota);
        adapter_spinner_polnota.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_polnota.setAdapter(adapter_spinner_polnota);

        obiem = _this.findViewById(R.id.edit_obiem);

        add_porodu = _this.findViewById(R.id.button_add_porodu1);

        initListViewPoroda();
        //endregion

        //region события
        add_porodu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(_context, AddPoroduActivity.class);
                State.selectedPoroda = new YarusPoroda();
                selectedYarus.yarusPorodaList.add(State.selectedPoroda);
                startActivity(intent);
            }
        });

        spinner_type_yarus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_polnota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                save();
                obiem.setText(calcSrednZapasNasajd(selectedYarus));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        obiem.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });

        list_porodi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(_context, AddPoroduActivity.class);
                State.selectedPoroda = selectedYarus.yarusPorodaList.get(position);
                startActivity(intent);
            }
        });
        list_porodi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                DialogInterface.OnClickListener confirmDelete = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        YarusPoroda deletedPoroda = selectedYarus.yarusPorodaList.get(position);
                        deletedPoroda = null;
                        selectedYarus.yarusPorodaList.remove(position);
                        initListViewPoroda();
                        XmlHelper.writeXmlFile(State.selectedNote);
                        YarusActivity.calcPreobladPoroda();
                    }
                };

                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Подтверждение удаления породы из яруса");
                args.putString("message", "Удалить выбранную породу?");
                args.putInt("icon", android.R.drawable.ic_menu_delete);
                args.putString("positiveMessage", "Да");
                args.putString("negativeMessage", "Нет");
                dialog.setArguments(args);
                dialog.positive = confirmDelete;
                dialog.show(getFragmentManager(), "custom");

                return true;
            }
        });
        //endregion

        // region загружаем информацию из xml
        selectedVidel = State.selectedVidel;
        if (selectedVidel != null && selectedYarus != null) {
            //объём
            if (selectedYarus.SZN != null)
                obiem.setText(selectedYarus.SZN);

            //полнота
            if (selectedYarus.polnota != null)
                spinner_polnota.setSelection(adapter_spinner_polnota.getPosition(selectedYarus.polnota));

            //тип яруса
            if (selectedYarus.typeYarus != null){
                String tip_yarus = DatabaseProcedures.GetDataFromPK("тип_яруса", "name", selectedYarus.typeYarus);
                spinner_type_yarus.setSelection(adapter_spinner_type_yarus.getPosition(tip_yarus));
            }
        }
        //endregion
    }

    boolean IsGornoeUchLesvo(){
        // если в участковом лесничестве хоть у одного выдела экспозиция и крутизна склона != 0, то горные, иначе равнинные
        ArrayList<Kvartal> kvartals = State.selectedUchLesvo.kvartals;
        for (int i = 0; i < kvartals.size(); i++){
            ArrayList<Videl> videls = kvartals.get(i).videls;
            for (int j = 0; j < videls.size(); j++){
                if (Integer.parseInt(videls.get(j).krutSklona) > 0 && !videls.get(j).exposSklona.equals("0"))
                    return true;
            }
        }
        return false;
    }

    String calcSrednZapasNasajd(Yarus yarus){
        try {
            if (State.preobladPoroda != null && yarus.yarusPorodaList != null && yarus.yarusPorodaList.size() > 0) {
                // равнинные или горные леса
                boolean isGornoe = IsGornoeUchLesvo();
                // stock_num
                String field;
                if (isGornoe)
                    field = "stock_nom_mount";
                else
                    field = "stock_nom_flat";
                String script = "select " + field + " from порода_ярус where code = " + "'" + State.preobladPoroda.Code + "'";
                String stock_nom = DatabaseProcedures.runScript(script).get(0);

                if (Integer.parseInt(stock_nom) != -1) {
                    //полнота и средняя высота яруса преобладающей породы
                    //TODO я беру просто высоту преобл породы и полноту яруса, где находится преобл порода
                    Integer visota = State.preobladPoroda.Visota;
                    //Yarus yarus = YarusPorodi(State.preobladPoroda.Code);
                    Double polnota = Double.parseDouble(yarus.polnota);

                    //берём ZapasGa
                    String table;
                    if (isGornoe)
                        table = "ZapasGa_mount";
                    else
                        table = "ZapasGa_flat";

                    script = "select ZapasGa from " + table + " where ZapasNomTab =? and Vysota = ?";
                    String params[] = {stock_nom, visota.toString()};
                    String ZapasGa = DatabaseProcedures.runScriptParams(script, params).get(0);

                    Double result = Integer.parseInt(ZapasGa) * polnota * 10;
                    Integer result_int = result.intValue();
                    return result_int.toString();
                } else
                    return "0";
            }
            return "0";
        }
        catch (Exception e){
            return "0";
        }
    }

    //region БД
    public void getDataFromDB(){
        type_yarus = DatabaseProcedures.getData("тип_яруса");
        polnota = DatabaseProcedures.getData("полнота", "tx");
    }
    //endregion
}