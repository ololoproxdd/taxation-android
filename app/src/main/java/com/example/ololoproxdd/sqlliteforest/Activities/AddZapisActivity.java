package com.example.ololoproxdd.sqlliteforest.Activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Adapters.NoteAdapter;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.NoteTaxation;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;

import java.util.ArrayList;

public class AddZapisActivity extends AppCompatActivity {

    Activity _this = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_zapis);
        getSupportActionBar().setTitle("Список записей");
    }

    @Override
    protected void onStart(){
        super.onStart();
        XmlHelper.readXmlFiles();
        init();
    }

    @Override
    public void onBackPressed()
    {
        if (State.exportFlag) {
            State.exportFlag = false;
            Toast toast = Toast.makeText(this, "Вы вышли из режима экспорта!",Toast.LENGTH_LONG);
            toast.show();
        }
        super.onBackPressed();
    }

    public void init(){
        ListView notesList = findViewById(R.id.main_listview);
        final NoteAdapter na = new NoteAdapter(this, R.layout.view_note_item, XmlHelper.noteTaxationList);
        notesList.setAdapter(na);

        notesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NoteTaxation selectedNote = XmlHelper.noteTaxationList.get(position);
                //region Export
                if (State.exportFlag){
                    State.exportFlag = false;

                    DialogInterface.OnClickListener confirm = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            _this.finish();
                            Intent intent = new Intent(_this, ImportExportActivity.class);
                            startActivity(intent);
                        }
                    };

                    if (XmlHelper.exportNote(selectedNote) != null){
                        MyDialog dialog = new MyDialog();
                        Bundle args = new Bundle();
                        args.putString("title", "Экспорт");
                        args.putString("message", "Экспорт выполнен успешно\nЭкспортируемый файл находится в папке export");
                        args.putInt("icon", android.R.drawable.ic_dialog_info);
                        args.putString("positiveMessage", "Ок");
                        //args.putString("negativeMessage", "Нет");
                        dialog.setArguments(args);
                        dialog.positive = confirm;
                        dialog.show(getSupportFragmentManager(), "custom");
                    }
                    else
                    {
                        MyDialog dialog = new MyDialog();
                        Bundle args = new Bundle();
                        args.putString("title", "Экспорт");
                        args.putString("message", "Ошибка экспорта");
                        args.putInt("icon", android.R.drawable.ic_dialog_alert);
                        args.putString("positiveMessage", "Ок");
                        //args.putString("negativeMessage", "Нет");
                        dialog.setArguments(args);
                        dialog.positive = confirm;
                        dialog.show(getSupportFragmentManager(), "custom");
                    }

                }
                //endregion
                // при обычном режиме выбора записи для дальнейшей работы с выделами
                else {
                    Intent intent = new Intent(getApplicationContext(), LesnichestvoActivity.class);
                    State.selectedNote = selectedNote;
                    startActivity(intent);
                }
            }
        });

        notesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                DialogInterface.OnClickListener confirmDelete = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NoteTaxation selectedNote = XmlHelper.noteTaxationList.get(position);
                        XmlHelper.deleteXmlFile(selectedNote);
                        init();
                    }
                };

                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Подтверждение удаления записи");
                args.putString("message", "Удалить выбранную запись?");
                args.putInt("icon", android.R.drawable.ic_menu_delete);
                args.putString("positiveMessage", "Да");
                args.putString("negativeMessage", "Нет");
                dialog.setArguments(args);
                dialog.positive = confirmDelete;
                dialog.show(getSupportFragmentManager(), "custom");

                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (State.exportFlag)
            return false;

        switch (item.getItemId()) {
            case R.id.action_create:

                NoteTaxation note = new NoteTaxation();
                note.lesnichestvos = new ArrayList<>();
                note.setDate();
                note.setId();

                XmlHelper.writeXmlFile(note);
                Intent intent = new Intent(this, LesnichestvoActivity.class);
                State.selectedNote = note;
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

}
