package com.example.ololoproxdd.sqlliteforest.XmlModels;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;

@Root(name="Лесничество")
public class Lesnichestvo  implements Serializable {
    //region методы
    public Lesnichestvo(){
        uchastkovoeLesvos = new ArrayList<>();
    }

    public void setDefault() {
        lesnichestvoName = "";
        if (uchastkovoeLesvos != null) {
            for (int i = 0; i < uchastkovoeLesvos.size(); i++)
                uchastkovoeLesvos.get(i).setDefault();
        }
    }

    public ArrayList<String> uchastkovoeLesvosNames() {
        ArrayList<String> uchastkovoeLesvosNames = new ArrayList<>();
        if (uchastkovoeLesvos != null) {
            for (int i = 0; i < uchastkovoeLesvos.size(); i++) {
                String name = uchastkovoeLesvos.get(i).uchLesvoName;
                if (name != null)
                    uchastkovoeLesvosNames.add(name);
            }
        }
        return uchastkovoeLesvosNames;
    }
    //endregion

    //region поля
    @Element(required = false, name = "Лесничество_название")
    public String lesnichestvoName;

    @ElementList(inline=true, required = false, name = "Участ_Лесничество")
    public ArrayList<UchastkovoeLesvo> uchastkovoeLesvos;
    //endregion
}
