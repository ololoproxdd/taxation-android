package com.example.ololoproxdd.sqlliteforest.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.ololoproxdd.sqlliteforest.R;

public class EditDirActivity extends AppCompatActivity {

    //region UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_dir);
        getSupportActionBar().setTitle("Редактирование справочников");
    }

    // Категории земель
    public void editLandCategories(View view) {
        Intent intent = new Intent(this, EditorListActivity.class);
        intent.putExtra("editor_type", "kat_zemel");
        intent.putExtra("title", "Категории земель");
        startActivity(intent);
    }

    // Породы
    public void editBreeds(View view) {
        Intent intent = new Intent(this, EditorListActivity.class);
        intent.putExtra("editor_type", "poroda_yarus");
        intent.putExtra("title", "Породы");
        startActivity(intent);
    }

    // Типы леса
    public void editForestTypes(View view) {
        Intent intent = new Intent(this, EditorListActivity.class);
        intent.putExtra("editor_type", "tip_lesa");
        intent.putExtra("title", "Типы леса");
        startActivity(intent);
    }

    // Особо защитные участки
    public void editSpecProtectAreas(View view) {
        Intent intent = new Intent(this, EditorListActivity.class);
        intent.putExtra("editor_type", "ozu");
        intent.putExtra("title", "ОЗУ");
        startActivity(intent);
    }

    // особенности выдела
    public void editFeaturesDepartment(View view) {
        Intent intent = new Intent(this, EditorListActivity.class);
        intent.putExtra("editor_type", "osoben");
        intent.putExtra("title", "Особенности выдела");
        startActivity(intent);
    }
    //endregion

    public void editEvent(View view) {
        Intent intent = new Intent(this, EditorListActivity.class);
        intent.putExtra("editor_type", "events");
        intent.putExtra("title", "Мероприятия");
        startActivity(intent);
    }
}
