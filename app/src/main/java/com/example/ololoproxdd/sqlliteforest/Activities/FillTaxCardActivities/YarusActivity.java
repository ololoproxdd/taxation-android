package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.Fragments.YarusFragment;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseHelper;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.DatabaseProcedures;
import com.example.ololoproxdd.sqlliteforest.Fragments.MyDialog;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.ModelPoroda;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Kvartal;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Videl;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Yarus;
import com.example.ololoproxdd.sqlliteforest.XmlModels.YarusPoroda;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;
import com.google.common.collect.ComparisonChain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class YarusActivity extends AppCompatActivity {

    //region поля
    Spinner spinner_tip_lesa = null;
    Spinner spinner_tlu = null;
    static Spinner spinner_preobl_poroda = null;
    static ArrayAdapter<String> adapter_spinner_preobl_poroda = null;
    static ArrayAdapter<String> adapter_spinner_bonitet = null;
    static Spinner spinner_bonitet = null;
    Spinner spinner_vozrast = null;
    EditText zahlam = null;
    EditText liquid = null;
    EditText suhost = null;
    RadioButton yarus1 = null;
    RadioButton yarus2 = null;
    RadioButton yarus3 = null;

    ArrayList<String> tip_lesa = null;
    ArrayList<String> tlu = null;
    ArrayList<String> preobl_poroda = null;
    ArrayList<String> bonitet = null;
    ArrayList<String> group_age = null;

    Bundle _savedInstanceState = null;
    YarusFragment _yarus1 = null;
    YarusFragment _yarus2 = null;
    YarusFragment _yarus3 = null;
    Activity _this = this;

    static Videl selectedVidel = State.selectedVidel;

    boolean close = false;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        _savedInstanceState = savedInstanceState;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yarus);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Ярус");
        getDataFromDB();
        init();
        initHeader();
        calcPreobladPoroda();
    }

    @Override
    public void onBackPressed() {
        if (close == true || save(true)) {
            close = false;
            super.onBackPressed();  // optional depending on your needs
        }
    }

    public boolean save(boolean validation) {
        setDefaultValues();
        if (validation) {
            String errorMessage = "";
            //Не проставлена полнота у категорий, относящихся к покрытым лесом
            DatabaseProcedures DatabaseProcedures = new DatabaseProcedures();
            if (DatabaseProcedures.GetDataFromPK("категория_земель_ТЗ", "pokryt_lesom", selectedVidel.katZemel).equals("1") &&
                    selectedVidel.yarus1.yarusPorodaList.size() > 0 && (selectedVidel.yarus1.polnota.equals("0") ||
                    selectedVidel.yarus2.yarusPorodaList.size() > 0 && selectedVidel.yarus2.polnota.equals("0") ||
                    selectedVidel.yarus3.yarusPorodaList.size() > 0 && selectedVidel.yarus3.polnota.equals("0")))
                errorMessage += "Проверь полноту!\n";

            //сумма коэф должна быть = 10
            if (selectedVidel.yarus1.yarusPorodaList.size() > 0 &&  selectedVidel.yarus1.getSummaCoef() != 10 ||
                    selectedVidel.yarus2.yarusPorodaList.size() > 0 &&  selectedVidel.yarus2.getSummaCoef() != 10 ||
                    selectedVidel.yarus3.yarusPorodaList.size() > 0 &&  selectedVidel.yarus3.getSummaCoef() != 10)
                errorMessage += "Сумма коэффициентов каждого яруса должна быть равна 10!\n";

            //Запас на выделе более 600 куб.м/га
            Double zap1 = Double.parseDouble(selectedVidel.yarus1.SZN);
            Double zap2 = Double.parseDouble(selectedVidel.yarus2.SZN);
            Double zap3 = Double.parseDouble(selectedVidel.yarus3.SZN);
            Integer zahl = Integer.parseInt(zahlam.getText().toString());
            Integer suh = Integer.parseInt(suhost.getText().toString());
            Integer liq = Integer.parseInt(liquid.getText().toString());
            if (zap1 > 600 || zap2 > 600 || zap3 > 600 || zahl > 300 || liq > 300 || suh > 300)
                errorMessage += "Проверь запас!\n";


            if (!errorMessage.isEmpty()) {
                MyDialog dialog = new MyDialog();
                Bundle args = new Bundle();
                args.putString("title", "Ярус");
                args.putString("message", errorMessage);
                args.putInt("icon", android.R.drawable.ic_dialog_alert);
                args.putString("positiveMessage", "Исправить");
                args.putString("negativeMessage", "Выйти");
                dialog.setArguments(args);
                dialog.negative = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        close = true;
                        onBackPressed();
                    }
                };
                dialog.show(getSupportFragmentManager(), "custom");
                return false;
            }
        }
        save();
        Toast toast = Toast.makeText(this, "Сохранение выполнено успешно",Toast.LENGTH_LONG);
        toast.show();
        return true;
    }

    public void save() {
        setDefaultValues();
        //int
        selectedVidel.zahlam = zahlam.getText().toString();
        selectedVidel.likvidnost = liquid.getText().toString();
        selectedVidel.syhostoj = suhost.getText().toString();
        selectedVidel.bonitet = spinner_bonitet.getSelectedItem().toString();

        //guid
        DatabaseProcedures DatabaseProcedures = new DatabaseProcedures();

        selectedVidel.tipLesa = DatabaseProcedures.GetPK("тип_леса", "short_title", spinner_tip_lesa.getSelectedItem().toString());
        selectedVidel.TLU = DatabaseProcedures.GetPK("ТЛУ", "short_title", spinner_tlu.getSelectedItem().toString());
        selectedVidel.preoblPoroda = DatabaseProcedures.GetPK("порода_ярус", "code", spinner_preobl_poroda.getSelectedItem().toString());
        selectedVidel.groupVozrasta = DatabaseProcedures.GetPKCode("группа_возраста", spinner_vozrast.getSelectedItem().toString());

        if (State.selectedNote != null)
            XmlHelper.writeXmlFile(State.selectedNote);
    }

    //установка значений по умоланию в случае когда поле пустое
    void setDefaultValues() {
        if (zahlam.getText().toString().isEmpty())
            zahlam.setText("0");

        if (liquid.getText().toString().isEmpty())
            liquid.setText("0");

        if (suhost.getText().toString().isEmpty())
            suhost.setText("0");
    }

    public void init() {
        //region начальная инициализация
        Videl selectedVidel = State.selectedVidel;

        _yarus1 = new YarusFragment();
        _yarus2 = new YarusFragment();
        _yarus3 = new YarusFragment();

        spinner_tip_lesa = findViewById(R.id.spinner_tip_lesa);
        final ArrayAdapter<String> adapter_spinner_tip_lesa = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tip_lesa);
        adapter_spinner_tip_lesa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tip_lesa.setAdapter(adapter_spinner_tip_lesa);

        spinner_tlu = findViewById(R.id.spinner_tlu);
        ArrayAdapter<String> adapter_spinner_tlu = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tlu);
        adapter_spinner_tlu.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tlu.setAdapter(adapter_spinner_tlu);

        spinner_preobl_poroda = findViewById(R.id.spinner_preobl_poroda);
        adapter_spinner_preobl_poroda = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, preobl_poroda);
        adapter_spinner_preobl_poroda.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_preobl_poroda.setAdapter(adapter_spinner_preobl_poroda);
        spinner_preobl_poroda.setEnabled(false);

        spinner_bonitet = findViewById(R.id.spinner_bonitet);
        adapter_spinner_bonitet = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bonitet);
        adapter_spinner_bonitet.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_bonitet.setAdapter(adapter_spinner_bonitet);
        spinner_bonitet.setEnabled(false);

        spinner_vozrast = findViewById(R.id.spinner_vozrast);
        ArrayAdapter<String> adapter_spinner_vozrast = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, group_age);
        adapter_spinner_vozrast.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_vozrast.setAdapter(adapter_spinner_vozrast);
        //spinner_vozrast.setEnabled(false);

        //значения по умолчанию
        zahlam = findViewById(R.id.edit_zahlamlenie);
        zahlam.setText("0");

        liquid = findViewById(R.id.edit_liquid);
        liquid.setText("0");

        suhost = findViewById(R.id.edit_suhost);
        suhost.setText("0");

        yarus1 = findViewById(R.id.radio_yarus1);

        yarus2 = findViewById(R.id.radio_yarus2);
        yarus3 = findViewById(R.id.radio_yarus3);

        if (State.selectedYarusRadioButton == null)
            State.selectedYarusRadioButton = yarus1;
        else {
            if (State.selectedYarusRadioButton.getId() == yarus1.getId())
                State.selectedYarusRadioButton = yarus1;
            else if (State.selectedYarusRadioButton.getId() == yarus2.getId())
                State.selectedYarusRadioButton = yarus2;
            else if (State.selectedYarusRadioButton.getId() == yarus3.getId())
                State.selectedYarusRadioButton = yarus3;
        }

        State.selectedYarusRadioButton.setChecked(true);
        onRadioButtonClicked(State.selectedYarusRadioButton);
        //endregion

        //region события
        spinner_tip_lesa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String value = adapter_spinner_tip_lesa.getItem(position);
                if (value.equals("-"))
                    spinner_tlu.setSelection(0);
                    else
                {
                    value = value.split("-")[0];
                    DatabaseProcedures DatabaseProcedures = new DatabaseProcedures();
                    ArrayList<String> tlu = DatabaseProcedures.runScript(
                            "select ТЛУ_short_title from тип_леса_ТЛУ_бонитет where тип_леса_short_title = '" + value + "'");

                    ArrayAdapter<String> adapter_spinner_tlu = new ArrayAdapter<String>(_this, android.R.layout.simple_spinner_item, tlu);
                    adapter_spinner_tlu.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_tlu.setAdapter(adapter_spinner_tlu);

                    save();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_tlu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_preobl_poroda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_bonitet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinner_vozrast.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        zahlam.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        zahlam.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        liquid.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        liquid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });


        suhost.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    save();
            }
        });
        suhost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1 && s.toString().startsWith("0")) {
                    s.delete(0,1);
                }
            }
        });

        //endregion

        // region загружаем информацию из xml
        selectedVidel = State.selectedVidel;
        if (selectedVidel != null) {
            //захлам
            if (selectedVidel.zahlam != null)
                zahlam.setText(selectedVidel.zahlam);

            //ликвидность
            if (selectedVidel.likvidnost != null)
                liquid.setText(selectedVidel.likvidnost);

            //сухостой
            if (selectedVidel.syhostoj != null)
                suhost.setText(selectedVidel.syhostoj);

            DatabaseProcedures DatabaseProcedures = new DatabaseProcedures();

            //тип леса
            if (selectedVidel.tipLesa != null) {
                String tip_lesa = DatabaseProcedures.GetDataFromPK("тип_леса", "short_title", selectedVidel.tipLesa);
                spinner_tip_lesa.setSelection(adapter_spinner_tip_lesa.getPosition(tip_lesa));
            }

            //ТЛУ
            if (selectedVidel.TLU != null) {
                String tlu = DatabaseProcedures.GetDataFromPK("ТЛУ", "short_title", selectedVidel.TLU);
                spinner_tlu.setSelection(adapter_spinner_tlu.getPosition(tlu));
            }

            //преобл порода
            if (selectedVidel.preoblPoroda != null) {
                String poroda = DatabaseProcedures.GetDataFromPK("порода_ярус", "code", selectedVidel.preoblPoroda);
                spinner_preobl_poroda.setSelection(adapter_spinner_preobl_poroda.getPosition(poroda));
            }

            //бонитет
            if (selectedVidel.bonitet != null)
                spinner_bonitet.setSelection(adapter_spinner_bonitet.getPosition(selectedVidel.bonitet));

            //группа возраста
            if (selectedVidel.groupVozrasta != null) {
                String age = DatabaseProcedures.GetDataFromPK("группа_возраста", "code", "name", selectedVidel.groupVozrasta);
                spinner_vozrast.setSelection(adapter_spinner_vozrast.getPosition(age));
            }

        }
        //endregion
    }


    void initHeader() {
        View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        // Получаем нажатый переключатель
        switch (view.getId()) {
            case R.id.radio_yarus1:
                if (checked) {
                    State.selectedYarusRadioButton = (RadioButton) view;
                    if (_savedInstanceState == null) {
                        State.SelectedYarusNum = 1;
                        State.selectedSostavPorod = State.sostavPorod1;
                        State.selectedYarus = State.selectedVidel.yarus1;
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_yarus, _yarus1)
                                .commit();
                    }
                }
                break;
            case R.id.radio_yarus2:
                if (checked) {
                    State.selectedYarusRadioButton = (RadioButton) view;
                    if (_savedInstanceState == null) {
                        State.SelectedYarusNum = 2;
                        State.selectedSostavPorod = State.sostavPorod2;

                        State.selectedYarus = State.selectedVidel.yarus2;
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_yarus, _yarus2)
                                .commit();
                    }
                }
                break;

            case R.id.radio_yarus3:
                if (checked) {
                    State.selectedYarusRadioButton = (RadioButton) view;
                    if (_savedInstanceState == null) {
                        State.SelectedYarusNum = 3;
                        State.selectedSostavPorod = State.sostavPorod3;

                        State.selectedYarus = State.selectedVidel.yarus3;
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_yarus, _yarus3)
                                .commit();
                    }
                    break;
                }
        }
    }

    //region БД
    public void getDataFromDB() {
        tip_lesa = DatabaseProcedures.getDataActual("тип_леса", "short_title");
        tlu = DatabaseProcedures.getData("ТЛУ", "short_title");
        preobl_poroda = DatabaseProcedures.getDataActual("порода_ярус", "code");
        bonitet = DatabaseProcedures.getData("бонитет", "tx");
        group_age = DatabaseProcedures.getData("группа_возраста", "code", "name");
    }
    //endregion

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save(true);
                break;
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

    Yarus YarusPorodi(String code) {
        selectedVidel = State.selectedVidel;
        ArrayList<YarusPoroda> yarus1 = new ArrayList<>(selectedVidel.yarus1.yarusPorodaList);
        ArrayList<YarusPoroda> yarus2 = new ArrayList<>(selectedVidel.yarus2.yarusPorodaList);
        ArrayList<YarusPoroda> yarus3 = new ArrayList<>(selectedVidel.yarus3.yarusPorodaList);

        for (int i = 0; i < yarus1.size(); i++) {
            if (DatabaseProcedures.GetDataFromPK("порода_ярус", "code", yarus1.get(i).poroda).equals(code)) {
                return selectedVidel.yarus1;
            }
        }

        for (int i = 0; i < yarus2.size(); i++) {
            if (DatabaseProcedures.GetDataFromPK("порода_ярус", "code", yarus2.get(i).poroda).equals(code)) {
                return selectedVidel.yarus2;
            }
        }

        for (int i = 0; i < yarus3.size(); i++) {
            if (DatabaseProcedures.GetDataFromPK("порода_ярус", "code", yarus3.get(i).poroda).equals(code)) {
                return selectedVidel.yarus3;
            }
        }

        return null;
    }

    public static int yarusNum(YarusPoroda poroda){
        selectedVidel = State.selectedVidel;
        if (selectedVidel.yarus1.yarusPorodaList.contains(poroda))
            return 1;

        if (selectedVidel.yarus2.yarusPorodaList.contains(poroda))
            return 2;

        if (selectedVidel.yarus3.yarusPorodaList.contains(poroda))
            return 3;

        return 0;
    }

    //region Преобладающая порода
    public static void calcPreobladPoroda() {
        selectedVidel = State.selectedVidel;
        ArrayList yarus1 = new ArrayList<>(selectedVidel.yarus1.yarusPorodaList);
        ArrayList yarus2 = new ArrayList<>(selectedVidel.yarus2.yarusPorodaList);
        ArrayList yarus3 = new ArrayList<>(selectedVidel.yarus3.yarusPorodaList);
        ArrayList<YarusPoroda> yarusAll = new ArrayList<>();
        yarusAll.addAll(yarus1);
        yarusAll.addAll(yarus2);
        yarusAll.addAll(yarus3);

        ArrayList<ModelPoroda> result = new ArrayList<>();
        HashMap<String, ArrayList<Integer>> hvoinie = new HashMap<>();
        HashMap<String, ArrayList<Integer>> listvennie = new HashMap<>();

        if (yarusAll.size() > 0) {
            //накапливаем хвойные и лиственные породы
            for (int i = 0; i < yarusAll.size(); i++) {
                YarusPoroda poroda = yarusAll.get(i);
                String key = DatabaseProcedures.GetDataFromPK("порода_ярус", "code", poroda.poroda);
                Integer hoz_znach = Integer.parseInt(DatabaseProcedures.GetDataFromPK("порода_ярус", "hoz_zn", poroda.poroda));
                Integer priority = Integer.parseInt(DatabaseProcedures.GetDataFromPK("порода_ярус", "priority", poroda.poroda));
                Integer curValue = 0;

                HashMap<String, ArrayList<Integer>> curmap;
                //хвойные
                if (hoz_znach == 0)
                    curmap = hvoinie;
                else
                    curmap = listvennie;

                if (curmap.containsKey(key))
                    curValue = curmap.get(key).get(0);
                ArrayList<Integer> value = new ArrayList<>();
                value.add(0, Integer.parseInt(poroda.coeff) + curValue);
                value.add(1, priority);
                value.add(2, Integer.parseInt(poroda.visota));
                value.add(3, Integer.parseInt(poroda.vozrast));
                value.add(4, Integer.parseInt(DatabaseProcedures.GetDataFromPK("происхождение_породы", "bonitet_nom_tab", poroda.proishogden)));
                value.add(5, yarusNum(poroda));

                curmap.put(key, value);
            }

            Comparator<ModelPoroda> comparator = new Comparator<ModelPoroda>() {
                @Override
                public int compare(ModelPoroda d1, ModelPoroda d2) {
                    return ComparisonChain.start()
                            .compare(100 - d1.Coef, 100 - d2.Coef)
                            .compare(d1.Priority, d2.Priority)
                            .result();
                }
            };

            ArrayList<ModelPoroda> h = new ArrayList<>();
            ArrayList<ModelPoroda> l = new ArrayList<>();
            if (hvoinie.size() >= listvennie.size()) {
                for (Map.Entry<String, ArrayList<Integer>> item : hvoinie.entrySet()) {
                    h.add(new ModelPoroda(item.getKey(), item.getValue().get(0), item.getValue().get(1), item.getValue().get(2), item.getValue().get(3), item.getValue().get(4), item.getValue().get(5)));
                }
                Collections.sort(h, comparator);
                result.addAll(h);
                for (Map.Entry<String, ArrayList<Integer>> item : listvennie.entrySet()) {
                    l.add(new ModelPoroda(item.getKey(), item.getValue().get(0), item.getValue().get(1), item.getValue().get(2), item.getValue().get(3), item.getValue().get(4), item.getValue().get(5)));
                }
                Collections.sort(l, comparator);
                result.addAll(l);
            } else {
                for (Map.Entry<String, ArrayList<Integer>> item : listvennie.entrySet()) {
                    l.add(new ModelPoroda(item.getKey(), item.getValue().get(0), item.getValue().get(1), item.getValue().get(2), item.getValue().get(3), item.getValue().get(4), item.getValue().get(5)));
                }
                Collections.sort(l, comparator);
                result.addAll(l);
                for (Map.Entry<String, ArrayList<Integer>> item : hvoinie.entrySet()) {
                    h.add(new ModelPoroda(item.getKey(), item.getValue().get(0), item.getValue().get(1), item.getValue().get(2), item.getValue().get(3), item.getValue().get(4), item.getValue().get(5)));
                }
                Collections.sort(h, comparator);
                result.addAll(h);
            }

            State.sostavPorod1.clear();
            State.sostavPorod2.clear();
            State.sostavPorod3.clear();
            for (ModelPoroda poroda : result){
                switch (poroda.YarusNum){
                    case 1:
                        State.sostavPorod1.add(poroda);
                        break;
                    case 2:
                        State.sostavPorod2.add(poroda);
                        break;
                    case 3:
                        State.sostavPorod3.add(poroda);
                        break;
                }

            }
            State.preobladPoroda = result.get(0);
            spinner_preobl_poroda.setSelection(adapter_spinner_preobl_poroda.getPosition(result.get(0).Code));

            String bonitet = DatabaseProcedures.getBonitet(State.preobladPoroda.Visota, State.preobladPoroda.Vozrast, State.preobladPoroda.Proish);
            spinner_bonitet.setSelection(adapter_spinner_bonitet.getPosition(bonitet));

            selectedVidel.yarus1.SZN = calcSrednZapasNasajd(selectedVidel.yarus1);
            selectedVidel.yarus2.SZN = calcSrednZapasNasajd(selectedVidel.yarus2);
            selectedVidel.yarus3.SZN = calcSrednZapasNasajd(selectedVidel.yarus3);

        }
    }
    //endregion

    //region СЗН
    static boolean IsGornoeUchLesvo(){
        // если в участковом лесничестве хоть у одного выдела экспозиция и крутизна склона != 0, то горные, иначе равнинные
        ArrayList<Kvartal> kvartals = State.selectedUchLesvo.kvartals;
        for (int i = 0; i < kvartals.size(); i++){
            ArrayList<Videl> videls = kvartals.get(i).videls;
            for (int j = 0; j < videls.size(); j++){
                if (Integer.parseInt(videls.get(j).krutSklona) > 0 && !videls.get(j).exposSklona.equals("0"))
                    return true;
            }
        }
        return false;
    }

    static String calcSrednZapasNasajd(Yarus yarus) {
        try {
            if (yarus.yarusPorodaList != null && yarus.yarusPorodaList.size() > 0){
                // равнинные или горные леса
                boolean isGornoe = IsGornoeUchLesvo();
                // stock_num
                String field;
                if (isGornoe)
                    field = "stock_nom_mount";
                else
                    field = "stock_nom_flat";
                String script = "select " + field + " from порода_ярус where code = " + "'" + State.preobladPoroda.Code + "'";
                String stock_nom = DatabaseProcedures.runScript(script).get(0);

                if (Integer.parseInt(stock_nom) != -1) {
                    //полнота и средняя высота яруса преобладающей породы
                    //TODO я беру просто высоту преобл породы и полноту яруса, где находится преобл порода
                    Integer visota = State.preobladPoroda.Visota;
                    //Yarus yarus = YarusPorodi(State.preobladPoroda.Code);
                    Double polnota = Double.parseDouble(yarus.polnota);

                    //берём ZapasGa
                    String table;
                    if (isGornoe)
                        table = "ZapasGa_mount";
                    else
                        table = "ZapasGa_flat";

                    script = "select ZapasGa from " + table + " where ZapasNomTab =? and Vysota = ?";
                    String params[] = {stock_nom, visota.toString()};
                    String ZapasGa = DatabaseProcedures.runScriptParams(script, params).get(0);

                    Double result = Integer.parseInt(ZapasGa) * polnota * 10;
                    Integer result_int = result.intValue();
                    return result_int.toString();
                } else
                    return "0";
            }
            return "0";
        } catch (Exception e) {
            return "0";
        }
    }
    //endregion

    //region Группа возраста

    Integer calcKlassVozrasta(){
        String script = "select klass_vozrasta from порода_ярус where code = " + State.preobladPoroda.Code;
        Integer klass_vozrasta = Integer.parseInt(DatabaseProcedures.runScript(script).get(0));

        Integer result = State.preobladPoroda.Vozrast / klass_vozrasta;
        return  result;
    }

    Integer calcVozrastRubki(){
        return -1;
    }

    //endregion
}
