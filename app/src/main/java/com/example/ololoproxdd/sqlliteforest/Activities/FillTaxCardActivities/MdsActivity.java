package com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities.BolotoActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities.LesnieKulturiActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities.LineinieObjectActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities.OsobenVidelaActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities.PovrejdNasajdActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities.RekreacHarakterActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.FillTaxCardActivities.MdsActivities.ShUgodiaActivity;
import com.example.ololoproxdd.sqlliteforest.Activities.ObserveActivity;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Models.State;
import com.example.ololoproxdd.sqlliteforest.HelpClasses.Services.XmlHelper;
import com.example.ololoproxdd.sqlliteforest.R;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket11;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket12;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket13;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket17;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket19;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket21;
import com.example.ololoproxdd.sqlliteforest.XmlModels.Mds.Maket23;

import java.util.ArrayList;

public class MdsActivity extends AppCompatActivity {

    int btnColor = Color.WHITE;
    int requiredColor = Color.RED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("МДС");

        LinearLayout linearLayout = init(this);
        setContentView(linearLayout);
    }

    LinearLayout init(final MdsActivity mdsActivity){
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        View fragment = View.inflate(this, R.layout.fragment_header_info, null);
        initHeader(fragment);
        linearLayout.addView(fragment);

        TextView viborMaketa = new TextView(this);
        viborMaketa.setTextColor(Color.BLACK);
        //viborMaketa.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
        viborMaketa.setText("Выбор макета");
        viborMaketa.setTypeface(Typeface.create("casual", Typeface.NORMAL));
        viborMaketa.setTextSize(26);
        viborMaketa.setGravity(Gravity.CENTER);
        viborMaketa.setPadding(10,10,10,10);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(2,2,2,2);
        viborMaketa.setLayoutParams(layoutParams);

        linearLayout.addView(viborMaketa);

        if (State.mdsEnabled.get(11)) {
//            LinearLayout mdsLayout = new LinearLayout(this);
//            mdsLayout.setOrientation(LinearLayout.HORIZONTAL);
//            final Button button11 = new Button(this);
//            button11.setText("11 - Лесные культуры");
//            button11.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
//            ));
//
//            CheckBox cb = new CheckBox(this);
//            cb.setLayoutParams(new ViewGroup.LayoutParams
//                    (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//            button11.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                    Intent intent = new Intent(mdsActivity, LesnieKulturiActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    State.mdsShow.put(11,isChecked);
//                    button11.setEnabled(isChecked);
//                }
//            });
//
//            cb.setChecked(State.selectedVidel.maket11 != null);
//
//            if (State.activeRequiredForms != null && Integer.parseInt(State.activeRequiredForms.trim()) == 11){
//                button11.setBackgroundColor(requiredColor);
//                cb.setBackgroundColor(requiredColor);
//                cb.setChecked(true);
//            }
//            else{
//                button11.setBackgroundColor(btnColor);
//                cb.setBackgroundColor(btnColor);
//            }
//
//            mdsLayout.addView(cb);
//            mdsLayout.addView(button11);
//            linearLayout.addView(mdsLayout);
            initMds(11, "11 - Лесные культуры", linearLayout, LesnieKulturiActivity.class, State.selectedVidel.maket11, Maket11.class, mdsActivity);

        }

        if (State.mdsEnabled.get(12)) {
//            LinearLayout mdsLayout = new LinearLayout(this);
//            mdsLayout.setOrientation(LinearLayout.HORIZONTAL);
//            final Button button12 = new Button(this);
//            button12.setText("12 - Повреждение насаждений");
//            button12.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
//            ));
//
//            CheckBox cb = new CheckBox(this);
//            cb.setLayoutParams(new ViewGroup.LayoutParams
//                    (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//            button12.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                    Intent intent = new Intent(mdsActivity, PovrejdNasajdActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    State.mdsShow.put(12,isChecked);
//                    button12.setEnabled(isChecked);
//                }
//            });
//
//            cb.setChecked(State.selectedVidel.maket12 != null);
//
//            if (State.activeRequiredForms != null && Integer.parseInt(State.activeRequiredForms.trim()) == 12){
//                button12.setBackgroundColor(requiredColor);
//                cb.setBackgroundColor(requiredColor);
//                cb.setChecked(true);
//            }
//            else{
//                button12.setBackgroundColor(btnColor);
//                cb.setBackgroundColor(btnColor);
//            }
//
//            mdsLayout.addView(cb);
//            mdsLayout.addView(button12);
//            linearLayout.addView(mdsLayout);
            initMds(12, "12 - Повреждение насаждений", linearLayout, PovrejdNasajdActivity.class, State.selectedVidel.maket12, Maket12.class, mdsActivity);

        }

        if (State.mdsEnabled.get(13)) {
//            LinearLayout mdsLayout = new LinearLayout(this);
//            mdsLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            final Button button13 = new Button(this);
//            button13.setText("13 - Линейный объект");
//            button13.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
//            ));
//
//            CheckBox cb = new CheckBox(this);
//            cb.setLayoutParams(new ViewGroup.LayoutParams
//                    (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//            button13.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                    Intent intent = new Intent(mdsActivity, LineinieObjectActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    State.mdsShow.put(13,isChecked);
//                    button13.setEnabled(isChecked);
//                }
//            });
//
//            cb.setChecked(State.selectedVidel.maket13 != null);
//
//
//            if (State.activeRequiredForms != null && Integer.parseInt(State.activeRequiredForms.trim()) == 13){
//                button13.setBackgroundColor(requiredColor);
//                cb.setBackgroundColor(requiredColor);
//                cb.setChecked(true);
//            }
//            else{
//                button13.setBackgroundColor(btnColor);
//                cb.setBackgroundColor(btnColor);
//            }
//
//            mdsLayout.addView(cb);
//            mdsLayout.addView(button13);
//            linearLayout.addView(mdsLayout);
            initMds(13, "13 - Линейный объект", linearLayout, LineinieObjectActivity.class, State.selectedVidel.maket13, Maket13.class, mdsActivity);
        }

        if (State.mdsEnabled.get(17)) {
//            LinearLayout mdsLayout = new LinearLayout(this);
//            mdsLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            final Button button17 = new Button(this);
//            button17.setText("17 - С/Х угодья");
//            button17.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
//            ));
//            CheckBox cb = new CheckBox(this);
//            cb.setLayoutParams(new ViewGroup.LayoutParams
//                    (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//            button17.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                    Intent intent = new Intent(mdsActivity, ShUgodiaActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    State.mdsShow.put(17,isChecked);
//                    button17.setEnabled(isChecked);
//                }
//            });
//
//            cb.setChecked(State.selectedVidel.maket17 != null);
//
//            if (State.activeRequiredForms != null && Integer.parseInt(State.activeRequiredForms.trim()) == 17){
//                button17.setBackgroundColor(requiredColor);
//                cb.setBackgroundColor(requiredColor);
//                cb.setChecked(true);
//            }
//            else{
//                button17.setBackgroundColor(btnColor);
//                cb.setBackgroundColor(btnColor);
//            }
//
//            mdsLayout.addView(cb);
//            mdsLayout.addView(button17);
//            linearLayout.addView(mdsLayout);
//        }
            initMds(17, "17 - С/Х угодья", linearLayout, ShUgodiaActivity.class, State.selectedVidel.maket17, Maket17.class, mdsActivity);
        }
        if (State.mdsEnabled.get(19)) {
//            LinearLayout mdsLayout = new LinearLayout(this);
//            mdsLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            final Button button19 = new Button(this);
//            button19.setText("19 - Болото");
//            button19.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
//            ));
//            CheckBox cb = new CheckBox(this);
//            cb.setLayoutParams(new ViewGroup.LayoutParams
//                    (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//            button19.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                    Intent intent = new Intent(mdsActivity, BolotoActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    State.mdsShow.put(19,isChecked);
//                    button19.setEnabled(isChecked);
//                }
//            });
//
//            cb.setChecked(State.selectedVidel.maket19 != null);
//
//            if (State.activeRequiredForms != null && Integer.parseInt(State.activeRequiredForms.trim()) == 19){
//                button19.setBackgroundColor(requiredColor);
//                cb.setBackgroundColor(requiredColor);
//                cb.setChecked(true);
//            }
//            else{
//                button19.setBackgroundColor(btnColor);
//                cb.setBackgroundColor(btnColor);
//            }
//
//            mdsLayout.addView(cb);
//            mdsLayout.addView(button19);
//            linearLayout.addView(mdsLayout);
            initMds(19, "19 - Болото", linearLayout, BolotoActivity.class, State.selectedVidel.maket19, Maket19.class, mdsActivity);

        }

        if (State.mdsEnabled.get(21)) {
//            LinearLayout mdsLayout = new LinearLayout(this);
//            mdsLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            final Button button21 = new Button(this);
//            button21.setText("21 - Рекреационная характеристика");
//            button21.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
//            ));
//            CheckBox cb = new CheckBox(this);
//            cb.setChecked(State.mdsShow.get(21));
//            cb.setLayoutParams(new ViewGroup.LayoutParams
//                    (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//            button21.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                    Intent intent = new Intent(mdsActivity, RekreacHarakterActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    State.mdsShow.put(21,isChecked);
//                    button21.setEnabled(isChecked);
//                }
//            });
//
//            cb.setChecked(State.selectedVidel.maket21 != null);
//
//            if (State.activeRequiredForms != null && Integer.parseInt(State.activeRequiredForms.trim()) == 21){
//                button21.setBackgroundColor(requiredColor);
//                cb.setBackgroundColor(requiredColor);
//                cb.setChecked(true);
//            }
//            else{
//                button21.setBackgroundColor(btnColor);
//                cb.setBackgroundColor(btnColor);
//            }
//
//            mdsLayout.addView(cb);
//            mdsLayout.addView(button21);
//            linearLayout.addView(mdsLayout);
//        }
            initMds(21, "21 - Рекреационная характеристика", linearLayout, RekreacHarakterActivity.class, State.selectedVidel.maket21, Maket21.class, mdsActivity);
        }
            if (State.mdsEnabled.get(23)) {
//            LinearLayout mdsLayout = new LinearLayout(this);
//            mdsLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//            final Button button23 = new Button(this);
//            button23.setText("23 - Особенность выдела");
//            button23.setLayoutParams(new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
//            ));
//            CheckBox cb = new CheckBox(this);
//            cb.setChecked(State.mdsShow.get(23));
//            cb.setLayoutParams(new ViewGroup.LayoutParams
//                    (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//            button23.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                    Intent intent = new Intent(mdsActivity, OsobenVidelaActivity.class);
//                    startActivity(intent);
//                }
//            });
//
//            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    State.mdsShow.put(23,isChecked);
//                    button23.setEnabled(isChecked);
//                }
//            });
//
//            cb.setChecked(State.selectedVidel.maket23 != null);
//
//            if (State.activeRequiredForms != null && Integer.parseInt(State.activeRequiredForms.trim()) == 23){
//                button23.setBackgroundColor(requiredColor);
//                cb.setBackgroundColor(requiredColor);
//                cb.setChecked(true);
//            }
//            else{
//                button23.setBackgroundColor(btnColor);
//                cb.setBackgroundColor(btnColor);
//            }
//
//
//            mdsLayout.addView(cb);
//            mdsLayout.addView(button23);
//            linearLayout.addView(mdsLayout);
            initMds(23, "23 - Особенность выдела", linearLayout, OsobenVidelaActivity.class, State.selectedVidel.maket23, Maket23.class,  mdsActivity);
        }
        return linearLayout;
    }

    void initMds(final Integer mdsNum, String text, LinearLayout linearLayout, final Class mdsClass, final Object maket, final Class maketClass, final MdsActivity mdsActivity){
        LinearLayout mdsLayout = new LinearLayout(this);
        mdsLayout.setOrientation(LinearLayout.HORIZONTAL);

        final Button button = new Button(this);
        button.setText(text);
        button.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        CheckBox cb = new CheckBox(this);
        cb.setChecked(State.selectedVidel.selectedMDS.contains(mdsNum.toString()));
        cb.setLayoutParams(new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(mdsActivity, mdsClass);
                startActivity(intent);
            }
        });

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    if (!State.selectedVidel.selectedMDS.contains(mdsNum.toString()))
                        if (State.selectedVidel.selectedMDS.isEmpty())
                            State.selectedVidel.selectedMDS += mdsNum.toString();
                        else
                            State.selectedVidel.selectedMDS += " " + mdsNum.toString();
                }
                else {
                    int index = State.selectedVidel.selectedMDS.indexOf(mdsNum.toString());
                    if (index > -1) {
                        State.selectedVidel.selectedMDS = State.selectedVidel.selectedMDS.replace(mdsNum.toString(), "");
                        State.selectedVidel.selectedMDS.trim();
                    }
                }
                XmlHelper.writeXmlFile(State.selectedNote);
                button.setEnabled(isChecked);
            }
        });

        if (State.activeRequiredForms != null && Integer.parseInt(State.activeRequiredForms.trim()) == mdsNum){
            button.setBackgroundColor(requiredColor);
            cb.setBackgroundColor(requiredColor);
            cb.setChecked(true);
            cb.setEnabled(false);
        }
        else{
            button.setBackgroundColor(btnColor);
            cb.setBackgroundColor(btnColor);
            cb.setEnabled(true);
        }

        button.setEnabled(cb.isChecked());
        mdsLayout.addView(cb);
        mdsLayout.addView(button);
        linearLayout.addView(mdsLayout);
    }

    Object initMaket(Object maket, final Class maketClass){
        if (maket == null){
            try {
                maket = maketClass.newInstance();
                return maket;
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return maket;
    }

    void initHeader(View view){
        //View view = findViewById(R.id.header);
        TextView date = view.findViewById(R.id.date);
        date.setText(State.selectedNote.DateTime);

        TextView lesvo = view.findViewById(R.id.les_vo);
        lesvo.setText(State.selectedLesvo.lesnichestvoName);

        TextView uchLesvo = view.findViewById(R.id.uch_les_vo);
        uchLesvo.setText(State.selectedUchLesvo.uchLesvoName);

        TextView kvartal = view.findViewById(R.id.h_kvartal);
        kvartal.setText(State.selectedKvartal.kvartalNomer);

        TextView videl = view.findViewById(R.id.h_videl);
        videl.setText(State.selectedVidel.nomerVidela);
    }

    //region работа с кнопкой сохранить
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_observe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_observe:
                Intent intent = new Intent(this, ObserveActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
